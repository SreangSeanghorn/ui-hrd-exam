FROM node
 #RUN mkdir build
 RUN ls
 WORKDIR /app
 COPY package*.json /app/
 RUN npm install 
 COPY ./ /app/
 ADD ./ /public/
 #CMD ["npm", "start"]
 RUN npm run build
 #RUN npm start

#FROM nginx:alpine
#COPY nginx/hrdexam.conf /ect/nginx/conf.d/hrdexam.conf
#RUN rm -fr /usr.share/nginx/html/*
#RUN ls
#COPY public/ /usr/share/nginx/html
# EXPOSE 3000 1608
#ENTRYPOINT ["nginx","-g","daemon off;"]
