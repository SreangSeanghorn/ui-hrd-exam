//Icon
import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import AutorenewOutlinedIcon from '@material-ui/icons/AutorenewOutlined';
import PersonAddOutlinedIcon from '@material-ui/icons/PersonAddOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import ArchiveOutlinedIcon from '@material-ui/icons/ArchiveOutlined';
import WorkOutlineOutlinedIcon from '@material-ui/icons/WorkOutlineOutlined';

// core components/views for Admin layout
import Dashboard from "./views/Dashboard/Dashboard";
import ClassWork from "./views/ClassWork/ClassWork";
import Student from "./views/Student/Student";
import Response from "./views/Response/Response";
import ListTeacher from "./views/CreateTeacher/ListTeacher";
import Archived from "./views/Archived/Archived";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: DashboardOutlinedIcon,
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/classwork",
    name: "Class Work",
    icon: WorkOutlineOutlinedIcon,
    component: ClassWork,
    layout: "/admin",
  },
  {
    path: "/student",
    name: "Student",
    icon: PeopleAltOutlinedIcon,
    component: Student,
    layout: "/admin",
  },
  {
    path: "/response",
    name: "Response",
    icon: AutorenewOutlinedIcon,
    component: Response,
    layout: "/admin",
  },
  {
    path: "/create-teacher",
    name: "Create Teacher",
    icon: PersonAddOutlinedIcon,
    component: ListTeacher,
    layout: "/admin",
  },
  {
    path: "/archived-generation",
    name: "Generation",
    icon: ArchiveOutlinedIcon,
    component: Archived,
    layout: "/admin",
  },
];

export default dashboardRoutes;
