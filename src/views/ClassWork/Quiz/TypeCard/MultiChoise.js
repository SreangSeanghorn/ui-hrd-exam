import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
// import "../../../../assets/css/TabSection.css";
export default class MultiChoise extends Component {
  render() {
    const {index,question,point,isCorrect1,isCorrect2,isCorrect3,isCorrect4,anw1,anw2,anw3,anw4} =this.props
    return (
      <div>
        <Card style={{ width: "100%", marginTop: "14px"}} className="cardTop">
        <Form.Control
            disabled
            className='boxCard'
            type='text'
            value={point}
          />
          <Card.Body className="bodyAnswer textAnswer">
            {(index +1) + ". "+question}
          </Card.Body>

          <Card.Body>
            <Form.Check
            // disabled
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer "
              checked={isCorrect1 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-1`}
              label={anw1}
              custom
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              checked={isCorrect2 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-2`}
              label={anw2}
              custom
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              checked={isCorrect3 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-3`}
              label={anw3}
              custom
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              checked={isCorrect4 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-4`}
              label={anw4}
              custom
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
