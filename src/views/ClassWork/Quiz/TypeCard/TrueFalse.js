import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class TrueFalse extends Component {
  render() {
    const {index,question,point} =this.props
    return (
      <div>
        <Card style={{ width: "100%" }} className="cardTop">
          <Form.Control
            disabled
            className="boxCard"
            type="text"
            value={point}
          />
          <Card.Body className="textAnswer">
            <TextField
              disabled
              className="MuiInputBase-input-text"
              value={this.props.anwser}
            />
            {index + 1 + ". "}
            {question}
          </Card.Body>
        </Card>
      </div>
    );
  }
}
