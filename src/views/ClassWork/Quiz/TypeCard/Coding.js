import React, { useState } from "react";
import { Card, Form } from "react-bootstrap";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-java";
import "prismjs/components/prism-javascript";
import parse from 'html-react-parser';
//--- prism-themes
import "prism-themes/themes/prism-atom-dark.css";
export default function Coding({question, point, index}) {
  const [javascriptCode, setJavaScriptCode] = useState("");
  const content = question;
  var dataSplit = content.split(/\n/);
  return (
    <div>
      <form>

        <Card style={{ width: "100%" }}  className="cardTop">
        <Form.Control
            disabled
            className='boxCard'
            type='text'
            value={point}
          />
          <Card.Body className="textAnswer">
            {index + 1}
            <span>. </span>
            {dataSplit.map((data) => parse(data, { trim: true }))}
            </Card.Body>
        </Card>
        <br />
        <Card>
          <Editor
          disabled
            value={javascriptCode}
            onValueChange={(javascriptCode) =>
              setJavaScriptCode(javascriptCode)
            }
            highlight={(javascriptCode) =>
              highlight(javascriptCode, languages.javascript)
            }
            padding={10}
            tabSize={4}
            preClassName="#fff"
            style={{
              width: "100%",
              minHeight: "250px",
              fontFamily: '"Consolas", monospace',
              fontSize: 18,
            }}
          />
        </Card>
      </form>
    </div>
  );
}
