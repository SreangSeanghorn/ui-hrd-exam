import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class QuestionAnswer extends Component {
  render() {
    const {index,question,point} =this.props
    return (
      <div>
        <Card style={{ width: "100%" }} className="cardTop">
        <Form.Control
            disabled
            className='boxCard'
            type='text'
            value={point}
          />
          <Card.Body className="textAnswer">{(index +1) + ". " + question}</Card.Body>
          <Card.Body>
            <TextField
              disabled
              id="standard-full-width"
              multiline
              fullWidth
              placeholder="Answer"
              name="answer1"
              className="px-2 textAnswer"
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
