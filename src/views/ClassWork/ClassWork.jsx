import React, { Component } from "react";

import ClassWorkTable from './ClassWorkTable'
import queryString from 'query-string'
import "../ClassWork/css.css";


 class ClassWork extends Component {
  render() {
    const values = queryString.parse(this.props.location.search)
    return (
      <div style={{marginTop: "-25px"}}>
        <ClassWorkTable generationId = {values.generationId} activeGen = {values.activeGen}/>
      </div>
    );
  }
}

export default ClassWork

