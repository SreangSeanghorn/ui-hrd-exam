/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 12-08-2020
 * Development Group: HRD_EXAM
 * Description: This class create for control quiz.
 **********************************************************************/

import React, { useEffect, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import { bindActionCreators } from 'redux';
import { Button, Container, Pagination } from 'react-bootstrap';
import { connect } from 'react-redux';
import {
  deleteClasswork,
  fetchClassworkOne,
  updateClassWork,
  updateClassWorkStatus,
  getClassworks,
} from '../../redux/actions/classworkAction';
//Icon
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import CheckboxToggle from 'react-rainbow-components';
import { Table, Col, Form, Row } from 'react-bootstrap';
//alert
import Swal from 'sweetalert2';
//laoding
import { CustomLoading } from '../../components/utils/Utils';
import { Link } from 'react-router-dom';
/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 21-07-2020
 * Description: post students to database
 * Modification History : all day
 *     Date:
 *  Developer:Nheng Vanchhay
 *  TODO: - get all quizzes
 *        - Edit quiz and detail quiz
 *        - view quiz and print pdf
 *        - delete delete quiz (update status to false)
 *        - enable active quiz (update isActive is true)
 **********************************************************************/

/**********************************************************************
 * This method it used to control quiz.
 **********************************************************************/

const ClassWorkTable = (props) => {
  //get all record quiz
  const { getClassworks } = props;
  // create number of page
  const [page, setPage] = useState(1);
  const [isOpen, setIsOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [subject, setSubject] = useState('');
  const [duration, setDuration] = useState('');
  const [clsId, setClsId] = useState('');
  const [genId, setgenId] = useState(0);
  const { generationId } = props;

  const [titleError, setTitleError] = useState('');
  const [subjectError, setSubjectError] = useState('');
  const [durationError, setDurationError] = useState('');

  useEffect(() => {
    setgenId(generationId?generationId:0)
    getClassworks(page, (generationId?generationId : 0));
  }, [generationId, getClassworks, page]);

  //action delete
  const deleteClass = (Id) => {
    Swal.fire({
      title: 'Are you sure to delete this quiz?',
      // text: 'You Sure to delete this classwork.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#d33',
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Quiz has been delete successfully.',
          'success'
        ).then((res) => {
          getClassworks(page, genId);
        });
        props.deleteClasswork(Id);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Deleting quiz has been cancelled.', 'error');
      }
    });
  };

  //Modal
  const showModal = (clsID) => {
    setIsOpen(true);
    if (clsID !== undefined) {
      fetchClassworkOne(clsID, (data) => {
        setClsId(clsID);
        setTitle(data.title);
        setSubject(data.subject);
        setDuration(data.duration);
      });
    }
  };

  const validation = () => {
    let titleError = null;
    let subjectError = null;
    let durationError = null;
    setTitle(title.trim());
    setSubject(subject.trim());
    if (!title) {
      titleError = 'Title cannot be empty!';
      setTitleError(titleError);
    } else {
      setTitleError('');
    }

    if (title.trim() === '') {
      titleError = 'Title cannot be empty!';
      setTitleError(titleError);
    }
    if (!subject) {
      subjectError = 'Subject cannot be empty!';
      setSubjectError(subjectError);
    } else {
      setSubjectError('');
    }

    if (subject.trim() === '') {
      subjectError = 'Subject cannot be empty!';
      setSubjectError(subjectError);
    }

    if (!duration) {
      durationError = 'Duration cannot be empty!';
      setDurationError(durationError);
    } else {
      setDurationError('');
    }

    if (titleError || subjectError || durationError) {
      return false;
    } else {
      return true;
    }
  };

  const update = (e) => {
    const validate = validation();
    let classwork = {
      title,
      subject,
      duration,
    };

    if (validate) {
      console.log(validate);
      updateClassWork(classwork, clsId, (message) => {
        if(message !=='Quiz is failed to update'){
          Swal.fire('Update', `${message}`, 'success').then((res) => {
            getClassworks(page, genId);
            setIsOpen(false);
          });
        }else{
          Swal.fire('Fail', `${message}`, 'error').then((res) => {
            getClassworks(page, genId);
            setIsOpen(false);
          });
        }
    
      });
    }
  };
  const onActive = (id, status) => {
    // localStorage.setItem('quizId', id);
    let isActive = status ? false : true;
    updateClassWorkStatus(isActive, id, (message) => {
      if(message!=="Cannot Active the Empty Quiz"){
        Swal.fire('Update', `${message}`, 'success').then((res) => {
          getClassworks(page,genId);
        });
      }
      else{
        Swal.fire('Unavailable', `${message}`, 'worming')
      }
    });
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  // create action pagination
  const onNextPage = () => {
    if (page === props.paging.totalPages) {
      setPage(props.paging.totalPages);
    } else {
      setPage(page + 1);
      getClassworks(page + 1, genId);
    }
  };

  const onPrivePage = () => {
    if (page === 1) {
      setPage(1);
    } else {
      setPage(page - 1);
      getClassworks(page - 1, genId);
    }
  };

  const First = (p) => {
    setPage(p);
    getClassworks(p, genId);
  };
  const Last = (p) => {
    setPage(p);
    getClassworks(p, genId);
  };

  if (props.error === 'Network Error') {
    return (
      <div
        style={{
          textAlign: 'center',
        }}>
        <h3>Error connecting</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
  }
  if (props.loading) {
    return (
      <div>
        <CustomLoading />
      </div>
    );
  }
  if(props.classworkData ===undefined || props.classworkData.lenght<0 ){
    return (
      <div
        style={{
          textAlign: 'center',
        }}>
        <h3>No data</h3>
      </div>
    );
  }
  return (
    <div>
      <Table hover className='size_table'>
        <thead className='t_head'>
          <tr>
            <th
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: '14px',
                width: '70px',
              }}>
              No
            </th>
            <th className='font_color'>Title</th>
            <th className='font_color'>Subject</th>
            <th className='font_color' style={{ width: '70px' }}>
              Create Date
            </th>
            <th
              style={{ textAlign: 'center', width: '80px' }}
              className='font_color'>
              Duration (Min)
            </th>
            <th
              style={{
                textAlign: 'center',
                color: 'white',
                width: '170px',
                fontSize: '14px',
              }}>
              Action
            </th>
          </tr>
        </thead>
        {props.classworkData.map((data, index) => {
          return (
            <tbody key={index} className='t_bg'>
              <tr>
                <td style={{ borderTop: '-1px', textAlign: 'center' }}>
                  {index + 1}
                </td>
                <td>{data.title}</td>
                <td>{data.subject}</td>
                <td>{data.date}</td>
                <td style={{ textAlign: 'center' }}>{data.duration}</td>
                <td style={{ textAlign: 'center' }}>
                  <button
                    className=' btn btn-fill btn-info tooltip-icon1 style_btn'
                    disabled={
                      props.activeGen === 'false'
                        ? true
                        : data.status
                        ? true
                        : false
                    }
                    onClick={() => showModal(data.quizId)}>
                    <EditOutlinedIcon className='size_svg' />
                  </button>
                  <Link
                    target='_blank'
                    to={`/admin/classwork/viewquiz?id=${data.quizId}`}>
                    <button className='btn btn-fill btn-success style_btn'>
                      <VisibilityOutlinedIcon className='size_svg' />
                    </button>
                  </Link>
                  <button
                    disabled={
                      props.activeGen === 'false'
                        ? true
                        : data.status
                        ? true
                        : false
                    }
                    className='btn btn-fill btn-danger style_btn'
                    onClick={() => {
                      deleteClass(data.quizId);
                    }}>
                    <DeleteOutlineOutlinedIcon className='size_svg' />
                  </button>
                  <CheckboxToggle
                    disabled={props.activeGen === 'false' ? true : false}
                    value={data.status}
                    onClick={() => onActive(data.quizId, data.status)}
                    style={toggle_btn}
                  />
                </td>
              </tr>
            </tbody>
          );
        })}
      </Table>
      <Pagination style={{ marginLeft: '2%' }}>
        <Pagination.First onClick={() => onPrivePage()} />
        <Pagination.Item onClick={() => First(1)}>{1}</Pagination.Item>
        <Pagination.Ellipsis onClick={() => onPrivePage()} />
        <Pagination.Item active>{page}</Pagination.Item>
        <Pagination.Ellipsis onClick={() => onNextPage()} />
        <Pagination.Item
          onClick={() => Last(props.paging ? props.paging.totalPages : null)}>
          {props.paging ? props.paging.totalPages : null}
        </Pagination.Item>
        <Pagination.Last onClick={() => onNextPage()} />
      </Pagination>
      <Container style={{ fontFamily: 'Roboto ' }}>
        <Modal
          show={isOpen}
          onHide={hideModal}
          style={{ textAlign: 'center' }}
          backdrop='static'
          keyboard={false}
          size='lg'>
          <Modal.Header closeButton>
            <Modal.Title className='mt-3'>
              <div style={{ fontFamily: 'Roboto ', marginLeft: '17px' }}>
                Update Classwork
              </div>
            </Modal.Title>
          </Modal.Header>

          <Modal.Body style={{ borderRadius: '20px' }}>
            <Form.Group>
              <Container-fluid>
                <Row>
                  <Col md={6} className='mb-4 mt-1'>
                    <Col m={12} className='left_label'>
                      <label className='label_title'>Title</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type='text'
                        placeholder='Enter Title'
                        className='mb-2 border-text-none'
                        value={title}
                        onChange={(event) => setTitle(event.target.value)}
                      />
                      {titleError ? (
                        <h6 className='text-left text-danger'>{titleError}</h6>
                      ) : null}
                    </Col>
                  </Col>
                  <Col md={6} className='mb-4 mt-1'>
                    <Col m={12} className='left_label'>
                      <label className='left_label'>Subject</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type='text'
                        placeholder='Enter Subject'
                        className='mb-2'
                        value={subject}
                        onChange={(event) => setSubject(event.target.value)}
                      />
                      {subjectError ? (
                        <h6 className='text-left text-danger'>
                          {subjectError}
                        </h6>
                      ) : null}
                    </Col>
                  </Col>
                  <Col md={6} className='mb-4'>
                    <Col m={12} className='left_label'>
                      <label className='label_title'>Duration</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type='number'
                        placeholder='Enter Duration'
                        className='mb-2'
                        value={duration}
                        onChange={(event) => setDuration(event.target.value)}
                      />
                      {durationError ? (
                        <h6 className='text-left text-danger'>
                          {durationError}
                        </h6>
                      ) : null}
                    </Col>
                  </Col>
                </Row>
              </Container-fluid>
              <Button
                variant='info'
                style={{ fontFamily: 'Roboto', width: '140px' }}
                className='mt-2 mr-1'
                onClick={() => {
                  update();
                }}>
                Save
              </Button>
              {/* <Button
                as={Link}
                to={`/update-quiz/${clsId}`}
                target='_blank'
                onClick={hideModal}
                style={{ fontFamily: 'Roboto', width: '140px' }}
                className='mt-2 ml-1 btn-success'>
                Edit Classwork
              </Button> */}
            </Form.Group>
          </Modal.Body>
        </Modal>
      </Container>
    </div>
  );
};
const toggle_btn = {
  marginBottom: '-8px',
  marginLeft: '2px',
};
const mapStateToProp = (state) => {
  // console.log("state.classworkData.data:", state.classworkData.data);
  return {
    viewQuizData: state.viewQuizData.data,
    classworkData: state.classworkData.data,
    loading: state.classworkData.loading,
    error: state.classworkData.error,
    paging: state.classworkData.paging,
  };
};
const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getClassworks,
      deleteClasswork,
    },
    dp
  );
};
export default connect(mapStateToProp, mapDispatchToProps)(ClassWorkTable);
