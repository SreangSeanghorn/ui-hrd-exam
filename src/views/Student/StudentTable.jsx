/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 15-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for controll student.
 **********************************************************************/

import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';

import getStudent, {
  deleteStudent,
  fetchStudentOne,
  updateStudent,
  getClasswork,
} from "../../redux/actions/studentDataAction";
import { bindActionCreators } from "redux";
import { Table, Col, Row, Dropdown} from "react-bootstrap";
import { Form, Button, Container } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
// import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import '../../assets/css/studentRes.css';

//laoding
import { CustomLoading } from '../../components/utils/Utils';

/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 15-08-2020
 * Description: constroll student
 * Modification History : all day
 *     Date:
 *  Developer:Nheng Vanchhay
 *
 *  TODO: - get all students by class
 *        - update and delete students
 *        - delete student (update status to false)
 *
 **********************************************************************/

/**********************************************************************
 * This method it used to control students.
 **********************************************************************/

function StudentTable(props) {
  const { getStudent, getClasswork,generationId,activeGen } = props;

  const [labelDropdown, setLabelDropdown] = React.useState('KPS');
  const [isOpen, setIsOpen] = React.useState(false);
  const [stuID, setStuId] = React.useState('');
  const [cardID, setCardId] = React.useState('');
  const [className, setClassName] = React.useState('');
  const [name, setName] = React.useState('');
  const [gender, setGender] = React.useState('');

  const [cardIDError, setCardIdError] = React.useState('');
  const [classNameError, setClassNameError] = React.useState('');
  const [nameError, setNameError] = React.useState('');

  const arrClassName = props.classNameData.data;
  let classname = arrClassName.map((value) => ({ value }));
  
  useEffect(() => {
    getStudent(labelDropdown,generationId);
    getClasswork(generationId  ? generationId : 0 );
  }, [getStudent, getClasswork, labelDropdown, generationId]);

  //dropdown
  const handleChange = (cls) => {
    props.getStudent(cls,generationId);
    setLabelDropdown(cls);
  };
  // Modal
  const showModal = (stuID) => {
    setIsOpen(true);
    if (stuID !== undefined) {
      fetchStudentOne(stuID, (data) => {
        setStuId(stuID);
        setCardId(data.cardID);
        setClassName(data.className);
        setName(data.name);
        setGender(data.gender);
      });
    }
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  const validation = () =>{
    let cardIDError = '';
    let classNameError = '';
    let nameError = '';
    setCardId(cardID.trim());
    setClassName(className.trim());
    setName(name.trim());
    let regex = /[!@#$%^&*(),.?":{}|<>1-9]/g;
    if(!cardID){
      cardIDError = 'cardid cannot be empty!';
      setCardIdError(cardIDError);
    }else{
      setCardIdError('');
    }

    if(cardID.trim() === ''){
      cardIDError = 'cardid cannot be empty!';
      setCardIdError(cardIDError);
    }

    if(!className){
      classNameError = 'class name cannot be empty!';
      setClassNameError(classNameError);
    }else{
      setClassNameError('');
    }

    if(className.trim() === ''){
      classNameError = 'class name cannot be empty!';
      setClassNameError(classNameError);
    }

    if(!name){
      nameError = 'name cannot be empty!';
      setNameError(nameError);
    }else{
      setNameError('');
    }

    if(name.trim() === ''){
      nameError = 'name cannot be empty!';
      setNameError(nameError);
    }

    if (name.match(regex)){
      nameError = 'name cannot be contain spectail character or number!';
      setNameError(nameError);
    }

    if(cardIDError || classNameError || nameError){
      return false;
    }else{
      return true;
    }
  }

  const update = (e) => {
    const valid = validation();
    if(valid){
      if (stuID !== undefined) {
        let student = {
          name,
          gender,
          className,
          cardID,
        };
        updateStudent(student, stuID, (res) => {
          console.log(res.status);
            if(res.status==="OK"){
              Swal.fire('Update', `${res.message}`, 'success').then((res) => {
                getStudent(labelDropdown,generationId);
                setIsOpen(false);
              });
            }
              else{
                Swal.fire('Fail', `${res.message}`, 'error').then((res) => {
                  setIsOpen(false);
                });
            }
        });
      }
    }
  };
  const deleteStudent = (stuId) => {
    Swal.fire({
      title: 'Are you to delete this student?',
      // text: 'Are you to delete this student.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      confirmButtonColor: '#d33',
      cancelButtonText: 'Cancel',

    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Student has been deleted successfully.',
          'success'
        ).then((res) => {
          props.deleteStudent(stuId);
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Student has been cancelled delete.', 'error');
      }
    });
  };

  if(props.error ==="Network Error") return (
    <div
      style={{
        textAlign: "center",
      }}>
      <h3>Error connecting</h3>
      <Button onClick={() => window.location.reload()}>Reload</Button>
    </div>
  );
  
  if (props.loading) {
    return (
      <div>
        <CustomLoading />
      </div>
    );
  }
  if(props.studentData ===undefined) {
    return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>No Data</h3>
      </div>
    );
  }

  return (
    <div>
      <div>
        <Dropdown>
          <Dropdown.Toggle
            className='dropdown'
            variant='light'
            id='dropdown-basic'>
            {labelDropdown}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            {classname.map((value) => {
              return (
                <Dropdown.Item
                  key={value.value}
                  value={value.value}
                  onClick={() => handleChange(value.value)}>
                  {value.value}
                </Dropdown.Item>
              );
            })}
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <Table hover className='size_table'>
        <thead className='t_head'>
          <tr>
            <th
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: '14px',
                width: '70px',
              }}>
              No
            </th>
            <th style={{ color: 'white', fontSize: '14px', width: '200px' }}>
              Card ID
            </th>
            <th className='font_color' style={{ width: '300px' }}>
              Name
            </th>
            <th className='font_color'>Gender</th>
            <th className='font_color'>Class</th>
            <th
              style={{
                textAlign: 'center',
                color: 'white',
                width: '160px',
                fontSize: '14px',
              }}>
              Action
            </th>
          </tr>
        </thead>

        {props.studentData.map((data, index) => {
          return (
            <tbody className='t_bg' key={data.studentId}>
              <tr>
                <td style={{ textAlign: 'center' }}>{index + 1}</td>
                <td style={{ width: '200px' }}>{data.cardID}</td>
                <td>{data.name}</td>
                <td>{data.gender}</td>
                <td>{data.className}</td>
                <td style={{ textAlign: 'center' }}>
                  <button
                    disabled={activeGen ==="false" ? true : false}
                    className=' btn btn-fill btn-info tooltip-icon1 style_btn'
                    onClick={() => showModal(data.studentId)}>
                    <EditOutlinedIcon className='size_svg' />
                  </button>
                  <button
                    disabled={activeGen ==="false" ? true : false}
                    className='btn btn-fill btn-danger style_btn'
                    onClick={() => {
                      deleteStudent(data.studentId);
                    }}>
                    <DeleteOutlineOutlinedIcon className='size_svg' />
                  </button>
                </td>
              </tr>
            </tbody>
          );
        })}
      </Table>
      <Container style={{ fontFamily: 'Roboto ' }}>
        <Modal
          show={isOpen}
          onHide={hideModal}
          style={{ textAlign: 'center' }}
          backdrop='static'
          keyboard={false}
          size='lg'>
          <Modal.Header closeButton>
            <Modal.Title className='mt-3'>
              <div style={{ fontFamily: 'Roboto ', marginLeft: '16px' }}>
                Update Student
              </div>
            </Modal.Title>
          </Modal.Header>

          <Modal.Body style={{ borderRadius: '20px' }}>
            <Form.Group>
              <Container-fluid>
                <Row>
                  <Col md={6} className='mb-4 mt-1'>
                    <Col m={12} className='left_label'>
                      <label className='label_title'>ID Card</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type='text'
                        placeholder='Enter Card ID'
                        className='mb-2'
                        value={cardID.trim()}
                        onChange={(event) => setCardId(event.target.value)}
                      />
                      {cardIDError ? (
                        <h6 className='text-left text-danger'>{cardIDError}</h6>
                      ) : (
                        ''
                      )}
                    </Col>
                  </Col>
                  <Col md={6} className='mb-4 mt-1'>
                    <Col m={12} className='left_label'>
                      <label className='left_label'>Class</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type='text'
                        placeholder='Enter Class'
                        className='mb-2'
                        value={className.trim()}
                        onChange={(event) => setClassName(event.target.value)}
                      />
                      {classNameError ? (
                        <h6 className='text-left text-danger' style = {{textAlign:'left'}}>{classNameError}</h6>
                      ) : (
                        ''
                      )}
                    </Col>
                  </Col>
                  <Col md={6} className='mb-4'>
                    <Col m={12} className='left_label'>
                      <label className='label_title'>Name</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type='text'
                        placeholder='Enter Name'
                        className='mb-2'
                        value={name}
                        onChange={(event) => setName(event.target.value)}
                      />
                      {nameError ? (
                        <h6 className='text-left text-danger'>{nameError}</h6>
                      ) : (
                        ''
                      )}
                    </Col>
                  </Col>
                  <Col md={6} className='mb-4'>
                    <Col m={12} className='left_label'>
                      <label className='left_label'>Gender</label>
                    </Col>
                    <Col m={12}>
                      <select
                        className='custom-select'
                        id='class'
                        name='studentgender'
                        value={gender}
                        onChange={(event) => setGender(event.target.value)}>
                        <option value='Male'>Male</option>
                        <option value='Female'>Female</option>
                      </select>
                    </Col>
                  </Col>
                </Row>
              </Container-fluid>
              <Button
                onClick={hideModal}
                style={{ fontFamily: 'Roboto', width: '140px' }}
                className='mt-2 ml-1 btn-danger'>
                Close
              </Button>
              <Button
                style={{ fontFamily: 'Roboto', width: '140px' }}
                className='mt-2 ml-1 btn-info'
                onClick={() => {
                  update();
                }}>
                Update
              </Button>
            </Form.Group>
          </Modal.Body>
        </Modal>
      </Container>
    </div>
  );
}
const mapStateToProp = (state) => {
  return {
    studentData: state.studentDataAdmin.data,
    status: state.studentDataAdmin.status,
    loading: state.studentDataAdmin.loading,
    error: state.studentDataAdmin.error,
    classNameData: state.classNameData,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getStudent,
      deleteStudent,
      getClasswork,
    },
    dp
  );
};

export default connect(mapStateToProp, mapDispatchToProps)(StudentTable);
