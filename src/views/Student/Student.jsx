import React, { Component } from "react";
import StudentTable from './StudentTable'
import queryString from 'query-string'
export default class Student extends Component { 
  render() {
    const values = queryString.parse(this.props.location.search)
    return (
      <div style={{marginTop: "-4px"}}>
        <StudentTable generationId = {values.generationId} activeGen = {values.activeGen}/>
      </div>
    );
  }
}
