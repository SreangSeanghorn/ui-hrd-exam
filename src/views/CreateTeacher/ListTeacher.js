
/**********************************************************************
 * Original Author: Dien Sereyrith
 * Created Date: 11-08-2020
 * Development Group: HRD_EXAM
 * Description: This class create teacher username for loggin
 **********************************************************************/


import React, { useEffect } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import getCreateTeacher,{deleteCreateTeacher, addTeacher, changePassword} from "../../redux/actions/createTeacherAction";
import { Table, Form, Button, Col, Modal, Container, Row } from "react-bootstrap";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import VpnKeyOutlinedIcon from '@material-ui/icons/VpnKeyOutlined';
import Swal from "sweetalert2";
//laoding
import { CustomLoading } from '../../components/utils/Utils';


/**********************************************************************
 * Original Author: Dien Sereyrith
 * Created Date: 15-08-2020
 * Description: we use it create teacher username that enable teacher loggin to do anything else
 * Modification History : all day
 *     Date:
 *  Developer:Dien Sereyrith
 *  TODO: - get all teacher username
 *        - create teacher username
 *        - change password
 *        - delete teacher username (update status to false)
 **********************************************************************/

/**********************************************************************
 * This method it used to control teachers.
 **********************************************************************/

const ListTeacher = (props) =>{
  const [isAlert, setIsAlert] = React.useState(false);
  
  //Teacher state
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");

  // Teacher error state
  const [usernameError, setUsernameError] = React.useState("");
  const [passwordError, setPasswordError] = React.useState("");

  // change password
  const [oldPassword, setOldPassword] = React.useState("");
  const [newPassword, setNewPassword] = React.useState("");

  // Change password error
  const [oldPasswordError, setOldPasswordError] = React.useState("");
  const [newPasswordError, setNewPasswordError] = React.useState("");

  const { getCreateTeacher } = props;
  useEffect(() => {
    getCreateTeacher();
  }, [getCreateTeacher]);

  const user = localStorage.getItem('username')
  
  const validate = () => {
    
    let usernameError = '';
    let passwordError = '';
    setUsername(username.trim());

    if (!username) {
      usernameError = 'username cannot be empty!';
    }

    if (username.trim() === '') {
      usernameError = 'username cannot be empty!';
    }

    if (usernameError) {
      setUsernameError(usernameError);
    }else{
      setUsernameError('')
    }

    if (!password) {
      passwordError = 'password cannot be empty!';
    }

    if (passwordError) {
      setPasswordError(passwordError);
    }else{
      setPasswordError('')
    }

    if (
      usernameError ||
      passwordError 
    ) {
      return false;
    }

    return true;
  };

  const validateChangePassword = () => {
    let oldPasswordError = '';
    let newPasswordError = '';


    if (!oldPassword) {
      oldPasswordError = 'Old password cannot be empty!';
    }

    if (oldPasswordError) {
      setOldPasswordError(oldPasswordError);
    }else{
      setOldPasswordError('')
    }

    if (!newPassword) {
      newPasswordError = 'New password cannot be empty!';
    }

    if (newPasswordError) {
      setNewPasswordError(newPasswordError);
    }else{
      setNewPasswordError('')
    }

    if (
      oldPasswordError ||
      newPasswordError 
    ) {
      return false;
    }
    return true;
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const isValid = validate();

    const newTeacher = {
      name: username.trim(),
      password: password.trim(),
      roleDto: [
        {
          role: "ADMIN",
          roleId: 1
        }
      ]
    }

    // No internet access
    if( props.error) {
      return (
        <div
          style={{
            textAlign: 'center',
          }}>
          <h3>Error connecting</h3>
          <Button onClick={() => window.location.reload()}>Reload</Button>
        </div>
      );
    }
    
    if (isValid) {
      setUsernameError('');
      setPasswordError('');

        addTeacher(newTeacher, (msg) => {
          
          if(msg.status==="OK"){
            Swal.fire("Added", "Teachers has been added successfully", "success").then((result)=>{
            });
          setUsernameError('')  
          setUsername('')
          setPassword('')
          
          }
          getCreateTeacher()
        } );
      
    }
  };

  const deleteTeahcer = (Id) => {
    Swal.fire({
      title: "Are you sure to delete?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      cancelButtonText: "Cancel",
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          "Deleted!",
          "Teacher has been delete successfully.",
          "success"
        );
        props.deleteCreateTeacher(Id)
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("Cancelled", "Teacher has been cancelled delete :)", "error");
      }
    });
  
};

   if(props.error ==="Network Error") return (
    <div
      style={{
        textAlign: "center",
      }}>
      <h3>Error connecting</h3>
      <Button onClick={() => window.location.reload()}>Reload</Button>
    </div>
  );
    //Modal
    const showModal = () => {
      setIsAlert(true);
      setOldPassword("");
      setNewPassword("");
    };

    const hideModal = () => {
      setIsAlert(false);
    };

    const updatePassword = (e) => {
      validateChangePassword();
      let teacherPassword = {
        oldPassword,
        newPassword,
      };

        if(oldPassword!=='' && newPassword!==''){
          changePassword(teacherPassword, (message) => {
            if(message==='OK'){
              setOldPasswordError('');
              Swal.fire("Changed", `password has been changed successfully`, "success").then(res => {
                getCreateTeacher();
              })
              setIsAlert(false);
            }else{
              setOldPasswordError('Old password is incorrect!');
            }
          })

        }

    };

    // map data to render on the web page
    let data = props.createTeacher.map((d,index)=>{
      let isButton = true
      if(d.name===user){
        isButton = false
      }
      return(
        <tr key={d.id}>
                  <td style={{ textAlign: "center"}}>{index+1}</td>
                  <td >{d.name}</td>
                  <td style={{ textAlign: "center"}}>
                    <button className="btn btn-fill btn-success style_btn"
                    onClick={() => showModal()}
                    disabled = {isButton}
                    >
                      <VpnKeyOutlinedIcon className="size_svg" />
                    </button>
                    <button className="btn btn-fill btn-danger style_btn"
                    onClick={() => {
                      deleteTeahcer(d.id);
                    }}
                    disabled = {isButton}
                    >
                      <DeleteOutlineOutlinedIcon className="size_svg" />
                    </button>
                  </td>
                </tr>
      )
    })

  if (props.loading){
    return (
      <div>
        <CustomLoading />
      </div>
    ); 
  }
    return (
      <div style={{marginTop: "-7px"}}>
        <div>
          <Form >
            <Form.Group controlId="formBasicEmail">
              <Form.Label className="font-face-en">Username</Form.Label>
              <Form.Control
                className="font-face-en"
                type="text"
                name="username"
                placeholder="Enter Username"
                value={username.trim()}
                onChange={(event) => setUsername(event.target.value)}
              />
              {usernameError ? (
                <h6 className='text-danger'>{usernameError}</h6>
              ) : (
                ''
              )}
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label className="font-face-en">Password</Form.Label>
              <Form.Control
                className="font-face-en"
                type="password"
                name="password"
                placeholder="Enter Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
              />
              {passwordError ? (
                <h6 className='text-danger'>{passwordError.toLowerCase()}</h6>
              ) : (
                ''
              )}
            </Form.Group>
            <Button style={{marginTop: "8px"}} className="btn-page-next" onClick={handleSubmit}> Save
            </Button>
          </Form>
        </div>
  
        <div>
          
          <Table hover className="size_table">
            <thead className="t_head">
              <tr>
                <th
                  style={{
                    textAlign: "center",
                    color: "white",
                    fontSize: "14px",
                    width: "100px",
                  }}
                >
                  No
                </th>
                <th style={{ color: "white", fontSize: "14px", width: "300px" }}>
                  Username
                </th>
                
                <th
                  style={{
                    textAlign: "center",
                    color: "white",
                    width: "150px",
                    fontSize: "14px",
                  }}
                >
                  Action
                </th>
              </tr>
            </thead>
            <tbody className="t_bg">
             {data}
              
            </tbody>
          </Table>
        </div>
        <Container style={{ fontFamily: "Roboto " }}>
        <Modal
          show={isAlert}
          onHide={hideModal}
          style={{ textAlign: "center" }}
          backdrop="static"
          keyboard={false}
          size="lg">
          <Modal.Header closeButton>
            <Modal.Title className="mt-3">
              <div style={{ fontFamily: "Roboto ",marginLeft: "17px" }}>Change password</div>
            </Modal.Title>
          </Modal.Header>

          <Modal.Body style={{ borderRadius: "20px" }}>
            <Form.Group>
              <Container-fluid>
                <Row>
                  <Col md={6} className="mb-4 mt-1">
                    <Col m={12} className="left_label">
                      <label className="label_title">Old Password</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type="text"
                        placeholder="Enter old password"
                        className="mb-2 border-text-none"
                        value={oldPassword}
                        onChange={(event) => setOldPassword(event.target.value)}
                      />
                      {oldPasswordError ? (
                      <h6 className='text-danger' style={{textAlign:'left'}}>{oldPasswordError}</h6>
                      ) : (
                        ''
                      )}
                    </Col>
                  </Col>
                  <Col md={6} className="mb-4 mt-1">
                    <Col m={12} className="left_label">
                      <label className="left_label">New Password</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type="text"
                        placeholder="Enter new password"
                        className="mb-2"
                        value={newPassword}
                        onChange={(event) => setNewPassword(event.target.value)}
                      />
                      {newPasswordError ? (
                      <h6 className='text-danger' style={{textAlign:'left'}}>{newPasswordError}</h6>
                      ) : (
                        ''
                      )}
                    </Col>
                  </Col>
                </Row>
              </Container-fluid>

              <Button
                variant="info"
                style={{ fontFamily: "Roboto", width: "140px" }}
                className="mt-2 mr-1"
                onClick={() => {
                  updatePassword();
                }}>
                Save
              </Button>
              <Button
                onClick={hideModal}
                style={{ fontFamily: "Roboto", width: "140px" }}
                className="mt-2 ml-1 btn-danger">
                Cancel
              </Button>
            </Form.Group>
          </Modal.Body>
        </Modal>
      </Container>

      </div>
    );
  }

const mapStateToProps = (state) => {
  return {
    createTeacher: state.createTeacher.data,
    loading: state.createTeacher.loading,
    error: state.createTeacher.error,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getCreateTeacher,
      deleteCreateTeacher
    },
    dp
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListTeacher);