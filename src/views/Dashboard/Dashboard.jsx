/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 15-08-2020
 * Development Group: HRD_EXAM
 * Description: This class create get all numof Classworks , students, response, archived
 **********************************************************************/

import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "../../components/AdminDashboard/Grid/GridItem";
import GridContainer from "../../components/AdminDashboard/Grid/GridContainer";
import Card from "../../components/AdminDashboard/Card/Card";
import CardHeader from "../../components/AdminDashboard/Card/CardHeader";
import CardFooter from "../../components/AdminDashboard/Card/CardFooter";
import Grid from "@material-ui/core/Grid";

//Icon
import monthlyIcon from "../../assets/img/monthlyIcon.png";
import learning from "../../assets/img/learning.png";
//style
import styles from "../../assets/jss/material-dashboard-react/views/dashboardStyle";
import { connect } from "react-redux";
//Action
import getClasswork_GEN from "../../redux/actions/classworkAction";
import getGeneration from "../../redux/actions/generationAction";
import { getAllStudentGen } from "../../redux/actions/studentDataAction";
import {getQuiz} from "../../redux/actions/quizAction";
import { bindActionCreators } from "redux";
import { Button } from "@material-ui/core";
import queryString from "query-string";
//laoding
import { CustomLoading } from "../../components/utils/Utils";

const useStyles = makeStyles(styles);

const Dashboard = (props) => {
  const classes = useStyles();
  const { getClasswork_GEN, getGeneration, getAllStudentGen,getQuiz } = props;

  const [numClass, setnumClass] = useState(0);
  const [numGen, setnumGen] = useState(0);
  const [numStudent, setnumStudent] = useState(0)
  const [numQuizResponse, setNumQuizResponse] = useState(0)
  const values = queryString.parse(props.location.search);
    const { generationId } = values;

  useEffect(() => {
    getGeneration();

  }, [generationId, getGeneration, getQuiz]);

  useEffect(() => {

    getAllStudentGen(generationId ? generationId : 0);
    getClasswork_GEN(generationId ? generationId : 0);
    
  }, [generationId, getAllStudentGen, getClasswork_GEN]);

  useEffect(() => {
    if (props.numberOfClasswork !== undefined) {
      const numofCls = props.numberOfClasswork.filter((res) => res);
      setnumClass(numofCls.length);
    }else{
      setnumClass(0);
    }
  }, [props.numberOfClasswork]);

  useEffect(() => {
    console.log("==>",props.numberOfGeneration);
    if (props.numberOfGeneration !== undefined) {
      const numofGen =props.numberOfGeneration.filter((res) => res);
      setnumGen(numofGen.length);
    }else{
      setnumGen(0);
    }
    if (props.numberOfStudent !==undefined) {
      const numofStudent =props.numberOfStudent.filter((res) => res);
      setnumStudent(numofStudent.length);
    }else{
      setnumGen(0);
    }   
  }, [props.numberOfGeneration, props.numberOfStudent]);

  useEffect(() => {
   
    if (props.numberOfQuizResponse !== undefined) {
      const numofCls = props.numberOfQuizResponse.filter((res) => res);
      setNumQuizResponse(numofCls.length);
      console.log("numofCls.length:", numofCls.length);
    }else{
      setnumClass(0);
    }
  }, [generationId, props.numberOfQuizResponse]);

  useEffect(() => {
    
    getQuiz(1, (generationId ? generationId : 0))
    console.log("====> generationId:", generationId);
  }, [generationId, getQuiz])

  if (props.error === "") {
    return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>Error connecting</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
  }
  if (props.loading) {
    return (
      <div>
        <CustomLoading />
      </div>
    );
  }
  return (
    <div className="font" style={{ marginTop: "-30px" }}>
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          {/* <Link to="/admin/classwork"> */}
          <Card>
            <CardHeader stats icon>
              <Grid container spacing={3}>
                <Grid item xs={2} style={stylesCard.item}>
                  <img src={monthlyIcon} alt="monthlyIcon" />
                </Grid>
                <Grid item xs={10} style={stylesCard.gridTitle}>
                  <h4 className={classes.cardTitle} style={stylesCard.title}>
                    Classworks
                  </h4>
                  <h3 className={classes.cardTitle} style={stylesCard.number}>
                    {numClass}
                  </h3>
                </Grid>
              </Grid>
            </CardHeader>
            <CardFooter stats>
              <br />
            </CardFooter>
          </Card>
          {/* </Link> */}
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader stats icon>
              <Grid container spacing={3}>
                <Grid item xs={2} style={stylesCard.item}>
                  <img src={learning} alt="learning" />
                </Grid>
                <Grid item xs={10} style={stylesCard.gridTitle}>
                  <h4 className={classes.cardTitle} style={stylesCard.title}>
                    Students
                  </h4>
                  <h3 className={classes.cardTitle} style={stylesCard.number}>
                    {numStudent}
                  </h3>
                </Grid>
              </Grid>
            </CardHeader>
            <CardFooter stats>
              <br />
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader stats icon>
              <Grid container spacing={3}>
                <Grid item xs={2} style={stylesCard.item}>
                  <img src={monthlyIcon} alt="monthlyIcon" />
                </Grid>
                <Grid item xs={10} style={stylesCard.gridTitle}>
                  <h4 className={classes.cardTitle} style={stylesCard.title}>
                    Responses
                  </h4>
                  <h3 className={classes.cardTitle} style={stylesCard.number}>
                    {numQuizResponse}
                  </h3>
                </Grid>
              </Grid>
            </CardHeader>
            <CardFooter stats>
              <br />
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader stats icon>
              <Grid container spacing={3}>
                <Grid item xs={2} style={stylesCard.item}>
                  <img src={monthlyIcon} alt="monthlyIcon" />
                </Grid>
                <Grid item xs={10} style={stylesCard.gridTitle}>
                  <h4 className={classes.cardTitle} style={stylesCard.title}>
                    Generation
                  </h4>
                  <h3 className={classes.cardTitle} style={stylesCard.number}>
                    {numGen}
                  </h3>
                </Grid>
              </Grid>
            </CardHeader>
            <CardFooter stats>
              <br />
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
};
const stylesCard = {
  item: {
    textAlign: "left",
    marginTop: 15,
  },
  gridTitle: {
    marginTop: 12,
  },
  title: {
    fontSize: "large",
  },
  number: {
    fontWeight: 600,
  },
};
const mapStateToProp = (state) => {
  console.log(state.generationData.data);
  return {
    numberOfClasswork: state.classworkData.data,
    numberOfGeneration: state.generationData.data,
    numberOfStudent: state.studentDataAdmin.data,
    numberOfQuizResponse: state.quizzesData.data,
  };
};
const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getClasswork_GEN,
      getGeneration,
      getAllStudentGen,
      getQuiz
    },
    dp
  );
};
export default connect(mapStateToProp, mapDispatchToProps)(Dashboard);
