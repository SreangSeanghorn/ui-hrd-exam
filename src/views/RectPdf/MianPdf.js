import React, { Component } from "react";
import MyPdf from "./MyPdf";
import { PDFViewer } from "@react-pdf/renderer";
import { connect } from "react-redux";

import getQuestionByQuizId from "../../redux/actions/getQuestionByQuizIdAction";
import { bindActionCreators } from "redux";
// import { Button } from "react-bootstrap";
const queryString = require("query-string");

/// create by Developer lon dara

class MianPdf extends Component {
  constructor() {
    super();
    this.state = {
      QuestionData: [],
    };
  }

  componentWillMount() {
    const search = this.props.location.search;
    let result = queryString.parse(search);
    this.props.getQuestionByQuizId(result.id);

  }

  render() {
    // console.log(this.props.data);
    // console.log(this.props.data.quizId);
    // console.log(this.props.message);
    // console.log(this.props.message);
    // if(this.props.data.quizId===undefined){
    //   return (
    //     <div style={{textAlign: "center", marginTop: "25px", width: "100%"}}>
    //       <PDFViewer width="100%" height="1200vh" style={{ marginTop: "-2%" }}>
    //         <MyPdf data={this.props.data} messages={this.props.message} />
    //       </PDFViewer>
    //     </div>
    //   );
    //   return false;
      
    // }
    // // return true
    // console.log(this.props.data.title);
    return (
      <div>
        <PDFViewer width="100%" height="1200vh" style={{ marginTop: "-2%" }}>
          <MyPdf data={this.props.data}/>
        </PDFViewer>
      </div>
    );
  }
}

const mapStateToProp = (state) => {
  return {
    data: state.getQQId.data,
    message: state.getQQId.error,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getQuestionByQuizId,
    },
    dp
  );
};

export default connect(mapStateToProp, mapDispatchToProps)(MianPdf);
