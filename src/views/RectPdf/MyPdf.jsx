import React, { Component } from "react";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  Image,
  //   Font,
} from "@react-pdf/renderer";
import "bootstrap/dist/css/bootstrap.min.css";
import Capture from "./image/Capture.PNG";
import Khmer from "./image/Khmer.PNG";
import Symbol from "./image/Symbol.PNG";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for view React PDF.
 **********************************************************************/

export default class MyPdf extends Component {
  render() {
    console.log(this.props.data.responses);
    return (
      <Document>
        <Page size="A4" style={styles.header}>
          <View style={styles.page}>
            <Image
              style={{
                width: "125px",
                height: "100px",
                marginLeft: "16px",
                marginHorizontal: 0,
                marginVertical: 0,
              }}
              src={Capture}
            />

            <View>
              <View style={(styles.header, styles.centerhHeader)}>
                <Image src={Khmer} style={{ width: "200px", height: "18px" }} />
                <Text
                  style={{
                    marginTop: "25",
                    textAlign: "center",
                    position: "absolute",
                  }}
                >
                  KOREAN SOFTWARE HRD CENTER
                </Text>
                <Text
                  style={{
                    marginTop: "40",
                    textDecoration: "underline",
                    textAlign: "center",
                    position: "absolute",
                  }}
                >
                  {this.props.data.title}
                </Text>
                <Text
                  style={{
                    marginTop: "56",
                    textDecoration: "underline",
                    textAlign: "center",
                    position: "absolute",
                  }}
                >
                  Subject : {this.props.data.subject}
                </Text>
                <Text
                  style={{
                    marginTop: "74",
                    textDecoration: "underline",
                    textAlign: "center",
                    position: "absolute",
                  }}
                >
                  Duration : {this.props.data.duration}
                </Text>
              </View>
            </View>
            <View style={(styles.header, styles.rightHeader)}>
              <Text style={{ fontSize: "11" }}>KINGDOM OF CAMBODIA</Text>
              <Text style={{ fontSize: "11" }}>NATION RELIGION KING</Text>
              <Image
                src={Symbol}
                style={{ width: "100px", height: "15px", marginLeft: "20" }}
              />
              <View
                style={{
                  border: "1",
                  lineHeight: "2",
                  width: "150",
                  height: "90",
                }}
              >
                <Text style={{ marginLeft: "10", marginTop: "8" }}>
                  Name:..................................
                </Text>
                <Text style={{ marginLeft: "10" }}>
                  Gender.................................
                </Text>
                <Text style={{ marginLeft: "10" }}>
                  Class..................................
                </Text>
                <Text style={{ marginLeft: "10" }}>Score...........</Text>
              </View>
            </View>
          </View>

          <View style={(styles.header, styles.rules)}>
            <View style={styles.textfromborder}>
              <Text
                style={{
                  fontSize: "11.5",
                  fontFamily: "Times-Bold",
                  textDecoration: "underline",
                  marginTop: "5",
                  marginBottom: "5",
                }}
              >
                {" "}
                Rule for Online Monthly Test:
              </Text>
              <Text style={{ fontSize: "10" }}>
                {" "}
                While doing monthly test all the students are not allowed to:
              </Text>
              <Text style={{ fontSize: "10", marginLeft: "15" }}>
                1. Sharing
              </Text>
              <Text style={{ fontSize: "10", marginLeft: "15" }}>
                2. Cheating
              </Text>
              <Text style={{ fontSize: "10", marginLeft: "15" }}>
                3. Discussing
              </Text>
              <Text style={{ fontSize: "10", marginBottom: "2" }}>
                {" "}
                Please be honest during the test. If any student doesn’t respect
                the rule above, your score will be deducted.
              </Text>
            </View>
          </View>

          <View style={styles.header}>
            <View style={{ textAlign: "center", fontSize: "11" }}>
              <Text
                style={{
                  fontFamily: "Times-Bold",
                  fontSize: "13",
                  textDecoration: "underline",
                }}
              >
                Question Sheet
              </Text>
            </View>
          </View>

          <View style={(styles.header, styles.body)}>
            {this.props.data.responses
              ? this.props.data.responses.map((qr) => {
                  return (
                    <View key={qr.instructionId}>
                      <Text
                        style={{
                          fontStyle: "italic",
                          fontSize: "12",
                          fontFamily: "Times-Bold",
                          marginTop: "8",
                          marginLeft: "10",
                          marginBottom: "10",
                        }}
                      >
                        {" "}
                        {qr.instruction} {" : "}{" "}
                        <Text
                          style={{
                            fontSize: "11",
                            textDecoration: "underline",
                          }}
                        >
                          {qr.title}
                        </Text>
                      </Text>

                      {qr.response.map((r, index) => {
                        return (
                          <View key={index}>
                            {r.image !== null && r.image !== "" ? (
                              <Image
                                src={r.image}
                                style={{
                                  width: "90%",
                                  marginLeft: "50",
                                  marginTop: "2",
                                  marginBottom: "5",
                                }}
                              />
                            ) : null}
                            <Text style={{ marginLeft: "25" }}>
                              <Text
                                style={{
                                  fontSize: "11.5",
                                  fontFamily: "Times-Bold",
                                }}
                              >
                                {index + 1}
                              </Text>
                              {" . "}
                              <Text style={{ fontSize: "10.5" }}>
                                {r.question}
                              </Text>
                              <Text style={{ fontSize: "10.5" }}>
                                {r.question}
                              </Text>
                            </Text>
                          </View>
                        );
                      })}
                    </View>
                  );
                })
              : null}
          </View>
        </Page>
      </Document>
    );
  }
}
const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "white",
    marginLeft: 15,
  },
  section: {
    textAlign: "center",
    margin: 5,
    padding: 0,
    flexGrow: 1,
  },
  rules: {
    fontSize: 10,
    border: 1,
    margin: 17,
    lineHeight: 1.5,
  },
  body: {
    fontSize: 10,
    margin: 17,
    lineHeight: 1.5,
  },

  centerhHeader: {
    marginLeft: 30,
    marginTop: 30,
    fontSize: 11,
    lineHeight: 2,
    fontFamily: "Times-Bold",
    textAlign: "justify",
  },

  rightHeader: {
    marginLeft: 35,
    fontSize: 11,
    fontStyle: "Times New Roman",
    lineHeight: 2,
    fontFamily: "Times-Bold",
  },

  header: {
    flexDirection: "column",
    backgroundColor: "white",
    padding: 20,
    paddingBottom: 20,
    paddingHorizontal: 10,
  },

  textfromborder: {
    marginLeft: 17,
    marginTop: 1.5,
  },
});
