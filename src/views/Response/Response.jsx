import React, { Component } from 'react';

import ResponseWork from "./ResponseWork";
import StudentResponse from "./StudentResponse";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MianPdf from "../RectPdf/MianPdf";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for view quiz.
 **********************************************************************/

class Response extends Component {
  constructor() {
    super();
    this.state = {
      action: true,
      qiuzid: "",
    };
  }
  onClickReponse = (qid) => {
    this.setState({
      action: false,
      qiuzid: qid,
    });
  };
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route path='/admin/response' exact component={ResponseWork} />
            <Route path='/admin/response/student' component={StudentResponse} />
            <Route path='/admin/response/reactpdf' component={MianPdf} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Response;
