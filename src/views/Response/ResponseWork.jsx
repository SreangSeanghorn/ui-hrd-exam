import React, { useEffect, useState } from "react";
import { Table, Button } from "react-bootstrap";

import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import AutorenewOutlinedIcon from "@material-ui/icons/AutorenewOutlined";
import {
  returnScoreToStudent
} from '../../redux/actions/classworkAction';
import Swal from 'sweetalert2';

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for view quiz.
 **********************************************************************/
import { connect } from "react-redux";
import { getQuiz } from "../../redux/actions/quizAction";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { CustomLoading } from "../../components/utils/Utils";
import { Pagination } from "react-bootstrap";
import queryString from "query-string";
import CheckboxToggle from "react-rainbow-components";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 15-08-2020
 * Description: constroll student
 * Modification History : all day
 *     Date:
 *  Developer: Lon dara
 *
 *  TODO: - View quiz
 *
 **********************************************************************/

/**************************************************************************
 * This method it used to view students response answer.
 **************************************************************************/

const ResponseWork = (props) => {
  const [genId, setgenId] = useState(0);
  const { getQuiz } = props;
  const values = queryString.parse(props.location.search);
  const { generationId, activeGen } = values;
  useEffect(() => {
    setgenId(generationId ? generationId : 0);
    getQuiz(1, generationId ? generationId : 0);
  }, [generationId, getQuiz]);

  // create number of page
  const [page, setPage] = useState(1);
  // create action pagination
  const onNextPage = () => {
    if (page === props.paging.totalPages) {
      setPage(props.paging.totalPages);
    } else {
      setPage(page + 1);
      getQuiz(page + 1, genId);
    }
  };

  const onPrivePage = () => {
    if (page === 1) {
      setPage(1);
    } else {
      setPage(page - 1);
      getQuiz(page - 1, genId);
    }
  };

  const Frist = (p) => {
    setPage(p);
    getQuiz(p, genId);
  };
  const Last = (p) => {
    setPage(p);
    getQuiz(p, genId);
  };
  const onActive = (id, status) => {
    // localStorage.setItem('quizId', id);
    let isSend = status ? false : true;
    returnScoreToStudent(isSend, id, (message) => {
      if(message!=="Cannot Active the Empty Quiz"){
        Swal.fire('Update', `${message}`, 'success').then((res) => {
          getQuiz(1, generationId ? generationId : 0);
        });
      }
      else{
        Swal.fire('Unavailable', `${message}`, 'worming')
      }
    });
  };
  if (props.message === "Network Error")
    return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>Error connecting</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
  if (props.quizzesLoading) return <CustomLoading />;
  return (
    <div style={{ marginTop: "-26px" }}>
      <div>
        <Table hover className="size_table">
          <thead className="t_head">
            <tr>
              <th
                style={{
                  textAlign: "center",
                  color: "white",
                  fontSize: "14px",
                  width: "70px",
                }}>
                No
              </th>
              <th className="font_color" style={{ width: "350px" }}>
                Title
              </th>
              <th className="font_color">Subject</th>
              <th className="font_color">Create Date</th>
              <th className="font_color">Assign Date</th>
              <th
                style={{
                  textAlign: "center",
                  color: "white",
                  fontSize: "14px",
                  width: "150px",
                }}>
                Action
              </th>
            </tr>
          </thead>
          {props.quizzesData
            ? props.quizzesData.map((data, index) => {
                return (
                  <tbody className="t_bg" key={data.quizId}>
                    <tr>
                      <td style={{ borderTop: "0px", textAlign: "center" }}>
                        {" "}
                        {index + 1}
                      </td>
                      <td>{data.title}</td>
                      <td>{data.subject}</td>
                      <td>{data.date}</td>
                      <td>{data.date}</td>
                      <td style={{ textAlign: "center" }}>
                        <Link
                          to={`/admin/response/reactpdf?id=${data.quizId}&&status=${activeGen}`}>
                          <button className="btn btn-fill btn-success style_btn">
                            <VisibilityOutlinedIcon className="size_svg" />
                          </button>
                        </Link>
                        <Link
                          to={`/admin/response/student?quizid=${data.quizId}&&status=${activeGen}`}>
                          <button className="btn btn-fill btn-primary style_btn">
                            <AutorenewOutlinedIcon className="size_svg" />
                          </button>
                        </Link>
                        <CheckboxToggle
                          // disabled={props.activeGen === 'false' ? true : false}
                          value={data.is_sent}
                          onClick={() => onActive(data.quizId, data.is_sent)}
                          style={toggle_btn}
                        />
                      </td>
                    </tr>
                  </tbody>
                );
              })
            : null}
        </Table>
        <Pagination style={{ marginLeft: "2%" }}>
          <Pagination.First onClick={() => onPrivePage()} />
          <Pagination.Item onClick={() => Frist(1)}>{1}</Pagination.Item>
          <Pagination.Ellipsis onClick={() => onPrivePage()} />

          <Pagination.Item active>{page}</Pagination.Item>

          <Pagination.Ellipsis onClick={() => onNextPage()} />
          <Pagination.Item
            onClick={() => Last(props.paging ? props.paging.totalPages : null)}>
            {props.paging ? props.paging.totalPages : null}
          </Pagination.Item>
          <Pagination.Last onClick={() => onNextPage()} />
        </Pagination>
      </div>
    </div>
  );
};
const toggle_btn = {
  marginBottom: "-8px",
  marginLeft: "2px",
};
const mapStateToProp = (state) => {
  return {
    quizzesData: state.quizzesData.data,
    quizzesLoading: state.quizzesData.loading,
    paging: state.quizzesData.paging,
    message: state.quizzesData.error,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getQuiz,
    },
    dp
  );
};

export default connect(mapStateToProp, mapDispatchToProps)(ResponseWork);
