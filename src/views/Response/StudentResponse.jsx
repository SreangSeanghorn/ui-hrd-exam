import React, { Component } from "react";
import { Table, Dropdown,Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import getResultByClass from "../../redux/actions/getResultAction";
import { CustomLoading } from "../../components/utils/Utils";
import { Link } from "react-router-dom";
import "../../assets/css/studentRes.css";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import getClassNameResponse from '../../redux/actions/classNameAction'
// import {
//   getClasswork,
// } from "../../redux/actions/studentDataAction";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for controll student.
 **********************************************************************/

const queryString = require("query-string");

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 15-08-2020
 * Description: constroll student
 * Modification History : all day
 *     Date:
 *  Developer: Lon dara
 *  Modify by: Nheng Vanchhay
 *
 *  TODO: - View Result Answer
 *
 **********************************************************************/

/**************************************************************************
 * This method it used to view students response answer of quiz by class
 **************************************************************************/

class StudentResponse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      labeldropdowm: "KPS",
      action: true,
      getQuizId: null,
      getStudentId: null,
      status : ""
    };
  }

  onChangeDropdown = (e) => {
    e.preventDefault();
    this.setState(
      {
        labeldropdowm: e.target.name,
      },
      () => this.handlerChange()
    );
  };

  handlerChange = () => {
    let results = queryString.parse(this.props.location.search);
    this.props.getResultByClass(results.quizid, this.state.labeldropdowm);
  };
  componentWillMount() {
    const search = this.props.location.search;
    let result = queryString.parse(search);
    this.props.getClassNameResponse(result.quizid)
    

    this.props.getResultByClass(result.quizid, this.state.labeldropdowm);
    this.setState({
      getQuizId: result.quizid,
      status: result.status
    });
    // getClasswork(result.generationId  ? result.generationId : 0 );
  }
  render() {
    // const search = this.props.location.search;
    // let getId = queryString.parse(search);
    if(this.props.resultError !=="") return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>Error connecting</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
    if (this.props.resultLoading) return <CustomLoading />;
    if (this.props.resultError) return <h2>{this.props.resultError}</h2>;

    const arrClassName = this.props.className;
    let classname = arrClassName.map((value) => ({ value }));
    return (
      <div>
        <div>
          <Dropdown>
            <Dropdown.Toggle
              variant="light"
              id="dropdown-basic"
              style={{
                marginLeft: "2%",
                fontSize: "17px",
                width: "160px",
              }}>
              {this.state.labeldropdowm}
            </Dropdown.Toggle>

            <Dropdown.Menu style={{ width: "0%" }}>
              {classname
                ? classname.map((d, index) => (
                    <Dropdown.Item
                      key={index}
                      name={d.value}
                      onClick={this.onChangeDropdown}>
                      {d.value}
                    </Dropdown.Item>
                  ))
                : null}
            </Dropdown.Menu>
          </Dropdown>
          <Table hover className="size_table">
            <thead className="t_head">
              <tr>
                <th
                  style={{
                    textAlign: "center",
                    color: "white",
                    fontSize: "14px",
                    width: "70px",
                  }}>
                  No
                </th>
                <th className="font_color" style={{ width: "200px" }}>
                  Card ID
                </th>
                <th className="font_color" style={{ width: "230px" }}>
                  Name
                </th>
                <th className="font_color">Gander</th>
                <th className="font_color">Class</th>
                <th className="font_color">Date</th>

                <th className="font_color" style={{ textAlign: "center" }}>
                  Score
                </th>
                <th className="font_color">Status</th>
                <th
                  style={{
                    textAlign: "center",
                    color: "white",
                    fontSize: "14px",
                    width: "150px",
                  }}>
                  Action
                </th>
              </tr>
            </thead>
            {this.props.resultData ? (
              this.props.resultData.map((d) => {
                return (
                  <tbody className="t_bg" key={d.resultId}>
                    <tr>
                      <td style={{ textAlign: "center" }}>{d.resultId}</td>
                      <td>{d.student.cardID}</td>
                      <td>{d.student.name}</td>
                      <td>{d.student.gender}</td>
                      <td>{d.student.className}</td>
                      <td>{d.quiz.date}</td>

                      <td style={{ textAlign: "center" }}>{d.score}</td>
                      <td>
                        {d.status === 0
                          ? "uncheck"
                          : d.status === 1
                          ? "in progress"
                          : "checked"}
                      </td>
                      <td style={{ textAlign: "center" }}>
                        {
                          d.status ===0 ? 
                          (
                            <Link
                              target="_blank"
                              to={`/admin/response/student/checkresult?quizId=${this.state.getQuizId}&&stuId=${d.student.studentId}&&totalscore=${d.score}&&status=${this.state.status ==="" ? "true" : this.state.status}`}>
                              <button className="btn btn-fill btn-danger style_btn">
                                <ClearIcon className="size_svg" style={{fill: "none"}}/>
                              </button>
                            </Link>
                          ) 
                          : 
                          d.status ===1 ? 
                          (
                            <Link
                              target="_blank"
                              to={`/admin/response/student/checkresult?quizId=${this.state.getQuizId}&&stuId=${d.student.studentId}&&totalscore=${d.score}&&status=${this.state.status ==="" ? "true" : this.state.status}`}>
                              <button className="btn btn-fill btn-info style_btn">
                                <CheckIcon className="size_svg" />
                              </button>
                            </Link>
                          )
                          : 
                          (
                            <Link
                              target="_blank"
                              to={`/admin/response/student/checkresult?quizId=${this.state.getQuizId}&&stuId=${d.student.studentId}&&totalscore=${d.score}&&status=${this.state.status ==="" ? "true" : this.state.status}`}>
                              <button className="btn btn-fill btn-success style_btn">
                                <DoneAllIcon className="size_svg" />
                              </button>
                            </Link>
                          )
                        }
                      </td>
                    </tr>
                  </tbody>
                );
              })
            ) : (
              <tbody>
                <tr>
                  <th colSpan="8" className="text-center">
                    No Data
                  </th>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProp = (state) => {
  return {
    resultData: state.resultData.data,
    className: state.classNameResponseData.data,
    resultLoading: state.resultData.loading,
    resultError: state.resultData.error,
    resultMessage: state.resultData.message,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getResultByClass,
      getClassNameResponse
    },
    dp
  );
};

export default connect(mapStateToProp, mapDispatchToProps)(StudentResponse);
