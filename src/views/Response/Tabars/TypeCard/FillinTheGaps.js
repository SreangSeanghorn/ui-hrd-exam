import React from "react";
import { Card, Form, Col, Row } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
function FillinTheGaps(props) {
  // const styles = {
  //   correct: {
  //     borderColor: "#47A8AB",
  //   },
  //   inCorrect: {
  //     borderColor: "red",
  //   },
  // }
  const [oldScore, setOldSocre] = React.useState(0);

  const getTotalScore = (e) => {
    let newScore = 0;
    let event = e.type;
    if (event === "blur") {
      newScore = e.target.value;
      if (newScore === "") {
        newScore = 0;
      }
      newScore = parseFloat(newScore) - parseFloat(oldScore);
      props.onChangeTotalScore(newScore);
    }
  };
  const question = {
    questionIndex: props.index,
    sectionIndex: props.sectionIndex,
    questionTypeId: props.questionTypeId,
  };
  return (
    <div>
      <Card style={{ width: "100%" }} className="cardTop">
        <Row>
          <Col lg={6}>
            <Form.Control
              disabled={props.activeGen === "false" ? true : false}
              type={props.activeGen ? "text" : "number" }
              className="boxCard"
              // type="number"
              name="point"
              defaultValue={props.point}
              onFocus={(e) =>
                setOldSocre(e.target.value === "" ? 0 : e.target.value)
              }
              onBlur={(e) => getTotalScore(e)}
              onChange={(e) => props.onChangeQuestionScore(question, e)}
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint}
              onFocus={(e) =>
                setOldSocre(e.target.value === "" ? 0 : e.target.value)
              }
              onBlur={(e) => getTotalScore(e)}
              onChange={(e) => props.onChangeQuestionScore(question, e)}
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>
        <Card.Body className="textAnswer">
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>
        <Card.Body>
          <TextField
            id="standard-basic"
            multiline
            fullWidth
            placeholder="Answer "
            value={props.anwser}
            className="text-input-fill"
          />
        </Card.Body>
      </Card>
    </div>
  );
}
export default FillinTheGaps;
