import React from "react";
import { Card, Form, Row, Col } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
function TrueFalse(props) {
  const [oldScore, setOldSocre] = React.useState(0);
  const question = {
    questionIndex: props.index,
    sectionIndex: props.sectionIndex,
    questionTypeId: props.questionTypeId,
  };
  const getTotalScore = (e) => {
    let newScore = 0;
    let event = e.type;
    if (event === "blur") {
      newScore = e.target.value;
      if (newScore === "") {
        newScore = 0;
      }
      newScore = parseFloat(newScore) - parseFloat(oldScore);
      props.onChangeTotalScore(newScore);
    }
  };

  return (
    <div>
      <Card className="cardTop" name="truefalseCard">
        <Row>
          <Col lg={6}>
            <Form.Control
              className="boxCard"
              disabled={props.activeGen ==="false"? true : false}
              type={props.activeGen ? "text" : "number" }
              name="point"
              defaultValue={props.point}
              onFocus={(e) =>
                setOldSocre(e.target.value === "" ? 0 : e.target.value)
              }
              onBlur={(e) => getTotalScore(e)}
              onChange={(e) => props.onChangeQuestionScore(question, e)}
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="number"
              name="questioPoint"
              defaultValue={props.qPoint}
              onFocus={(e) =>
                setOldSocre(e.target.value === "" ? 0 : e.target.value)
              }
              onBlur={(e) => getTotalScore(e)}
              onChange={(e) => props.onChangeQuestionScore(question, e)}
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>

        <Card.Body className="textTrueFalse" >
          <TextField
            
            className=" MuiInputBase-input-text"
            value={"     "+props.anwser}
          />
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>
      </Card>
    </div>
  );
}

export default TrueFalse;
