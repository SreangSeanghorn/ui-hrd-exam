import React from "react";
import { Card, Form, Row, Col } from "react-bootstrap";
import { TextField } from "@material-ui/core";

function QuestionAnswer(props) {
  const [oldScore, setOldSocre] = React.useState(0);

  const getTotalScore = (e) => {
    let newScore = 0;
    let event = e.type;
    if (event === "blur") {
      newScore = e.target.value;
      if (newScore === "") {
        newScore = 0;
      }
      newScore = parseFloat(newScore) - parseFloat(oldScore);
      props.onChangeTotalScore(newScore);
    }
  };
  const getDataCard = (e) => {
    // console.log("point: ", point)
    // console.log("props.question:", props.question);
  };
  const question = {
    questionIndex: props.index,
    sectionIndex: props.sectionIndex,
    questionTypeId: props.questionTypeId,
  };
  return (
    <div>
      <Card
        onClick={(e)=> getDataCard(e)}
        style={{ width: "100%" }}
        className="cardTop"
        name="QACard"
        >
        <Row>
          <Col lg={6}>
            <Form.Control
              className="boxCard"
              disabled={props.activeGen ==="false"? true : false}
              type={props.activeGen ? "text" : "number" }
              name="point"
              defaultValue={props.point}
              onFocus={(e) =>
                setOldSocre(e.target.value === "" ? 0 : e.target.value)
              }
              onBlur={(e) => getTotalScore(e)}
              onChange={(e) => props.onChangeQuestionScore(question, e)}
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint}
              onFocus={(e) =>
                setOldSocre(e.target.value === "" ? 0 : e.target.value)
              }
              onBlur={(e) => getTotalScore(e)}
              onChange={(e) => props.onChangeQuestionScore(question, e)}
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>
        <Card.Body className="textAnswer">
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>
        <Card.Body style={{ marginTop: "-10px" }}>
          {/* <textarea
            style={{
              width: "100%",
              borderTop: 0,
              borderLeft: 0,
              borderRight: 0,
            }}
            value={props.anwser}
            placeholder="Answer"
            className="textAnswer mt-1"
          /> */}
          <TextField
              id="standard-full-width"
              multiline
              fullWidth
              placeholder="Answer"
              name="answer"
              // className="px-2 textAnswer"
              value={props.anwser}
            />
        </Card.Body>
      </Card>
    </div>
  );
}
export default QuestionAnswer;
