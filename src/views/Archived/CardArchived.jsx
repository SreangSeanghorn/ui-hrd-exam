import React, { useEffect } from "react";
import GridItem from "../../components/AdminDashboard/Grid/GridItem";
import Card from "../../components/AdminDashboard/Card/Card";
import CardHeader from "../../components/AdminDashboard/Card/CardHeader";
import CardFooter from "../../components/AdminDashboard/Card/CardFooter";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../assets/jss/material-dashboard-react/views/dashboardStyle";
import learning from "../../assets/img/learning.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { setGenId } from "../../redux/actions/generationAction";
import { bindActionCreators } from "redux";

/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 21-07-2020
 * Description: post students to database
 * Modification History : all day
 *     Date:
 *  Developer:Nheng Vanchhay
 *  TODO: - get all quizzes
 *        - Edit quiz and detail quiz
 *        - view quiz and print pdf
 *        - delete delete quiz (update status to false)
 *        - enable active quiz (update isActive is true)
 **********************************************************************/

/**********************************************************************
 * This method it used to control quiz.
 **********************************************************************/

const useStyles = makeStyles(styles);
function CardArchived({
  generation_value,
  generationId,
  setGenId,
  generation_id,
  activeGenertion,
  onHandleUpdateStatus,
  onHandleDeleteGen

}) {
  const classes = useStyles();
  useEffect(() => {}, [generation_id, generationId, activeGenertion]);
  return (
    <GridItem xs={12} sm={6} md={3}>
      <Card className="mb-1">
        <Link
          to={`/admin/dashboard?generationId=${generation_id}&&genActive=${activeGenertion}`}
          // target="_blank"
          onClick={() => setGenId(generation_id, activeGenertion)}>
          <CardHeader stats icon>
            <Grid container spacing={1}>
              <Grid item xs={2} style={stylesCard.item}>
                <img src={learning} alt="learning" />
              </Grid>
              <Grid item xs={10} style={stylesCard.gridTitle}>
                <h4 className={classes.cardTitle} style={stylesCard.title}>
                  Generation
                </h4>
                <h3 className={classes.cardTitle} style={stylesCard.number}>
                  {generation_value}
                </h3>
              </Grid>
            </Grid>
          </CardHeader>
        </Link>
        <CardFooter stats>
          <br />
          <p
            onClick={() => onHandleDeleteGen(generation_id, activeGenertion)}
            style={{
              marginBottom: "0",
              marginRight: "auto",
              color: "#ff757c",
              cursor: "pointer",
            }}>
            Delete
          </p>
          <p
            style={{ marginBottom: "0"}}>
            {activeGenertion ? (
              <span style={{ color: "#17a2b8", fontWeight: "bolder" }}>
                Active
              </span>
            ) : (
              <span style={{ color: "#3c4858" }}>Archived</span>
            )}{" "}
          </p>
        </CardFooter>
      </Card>
    </GridItem>
  );
}
const stylesCard = {
  item: {
    textAlign: "left",
    marginTop: 15,
  },
  gridTitle: {
    marginTop: 12,
  },
  title: {
    fontWeight: 400,
  },
  number: {
    fontWeight: 500,
  },
};

const mapStateToProp = (state) => {
  return {
    generationId: state.generationData.genId,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      setGenId,
    },
    dp
  );
};
export default connect(mapStateToProp, mapDispatchToProps)(CardArchived);
