/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 12-08-2020
 * Development Group: HRD_EXAM
 * Description: This class create for control quiz.
 **********************************************************************/

import React, { useEffect } from "react";
import CardArchived from "./CardArchived";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import getGeneration, {
  postGeneration,
  deleteGeneration,
  updateGenerationStatus,
} from "../../redux/actions/generationAction";
import GridContainer from "../../components/AdminDashboard/Grid/GridContainer";
import { Container } from "@material-ui/core";
import { Button, Form, Col, Row } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";
//laoding
import { CustomLoading } from "../../components/utils/Utils";
//laoding
// import { CustomLoading } from '../../components/utils/Utils';
// import { Button } from "react-bootstrap";

/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 22-07-2020
 * Description: show all archived generation
 * Modification History : all day
 * Final Date: 22-07-2020
 *  Developer:Nheng Vanchhay
 *  TODO: - get all archived generation
 *
 **********************************************************************/

/**********************************************************************
 * This method get all archived generation.
 **********************************************************************/

function Archived(props) {
  const { getGeneration } = props;
  useEffect(() => {
    getGeneration();
  }, [getGeneration]);

  const [isOpen, setIsOpen] = React.useState(false);
  const [generation, setGeneration] = React.useState(false);
  const [generationError, setGenerationError] = React.useState('')

  //Modal
  const showModal = () => {
    setIsOpen(true);
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  const validate = () => {
    let generationError = '';

    if(!generation){
      generationError = 'generation cannot be empty!';
    }

    if (generationError) {
      setGenerationError(generationError);
    }else{
      setGenerationError('')
    }
  }
  const handleSubmit = () => {
    validate();
    let gen = {
      generation,
    };
    if(generation!==false){
      props.postGeneration(gen, (message) => {
        Swal.fire("Insert Successfully", `${message}`, "success").then(function () {
          props.getGeneration();
        });
      });
      setGeneration(false);
      setIsOpen(false);
    }
    
  };
  const handleUpdateState = (id) => {
    props.updateGenerationStatus(true, id, (message) => {
      Swal.fire("Generation Active.", `${message}`, "success").then(
        function () {
          props.getGeneration();
        }
      );
    });
  };
  const handleDeleteGen = (id, status) => {
    // console.log("status:", status);
    if (status === false) {
      Swal.fire({
        title: "Are you sure to permanently delete this generation?",
        // text: "You sure to delete this Generation.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Delete",
        confirmButtonColor: '#d33',
        // cancelButtonColor: '#3085d6',
        cancelButtonText: "Cancel",
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            "Permanently Deleted!",
            "Generation has been delete successfully.",
            "success"
          );
          props.deleteGeneration(id);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            "Cancelled",
            "Generation has been cancelled delete.",
            "error"
          );
        }
      });
    } else {
      Swal.fire({
        title: "Are you sure to permanently delete this generation?",
        // text: "You sure to delete this Generation.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Delete",
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        cancelButtonText: "Create New Generation",
      }).then((result) => {
        if (result.value) {
          // Swal.fire(
          //   "Permanently Deleted!",
          //   "Generation has been delete successfully.",
          //   "success"
          // ).then((res) => {
          //   props.deleteGeneration(id);
          // });
          Swal.fire({
            title: 'Are you sure to deleted permanently active generation ?',
            // text: 'You Sure to delete this classwork.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            confirmButtonColor: '#d33',
            cancelButtonText: 'Cancel',
            // cancelButtonColor: '#3085d6',
          }).then((result) => {
            if (result.value) {
              Swal.fire(
                'Permanently Active Generation Deleted!',
                'Generation has been delete successfully.',
                'success'
              ).then((res) => {
                props.deleteGeneration(id);
              });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              Swal.fire('Cancelled', 'Generation has been cancelled delete', 'error');
            }
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          showModal();
        }
      });
    }
  };

  if (props.error !== "") {
    return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>Error connecting</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
  }

  if (props.loading) {
    return (
      <div>
        <CustomLoading />
      </div>
    );
  }
  return (
    <div style={{ marginTop: "-10px" }}>
      <Container>
        <Button
          onClick={showModal}
          className="mt-2 btn-info button-in-archived">
          Create Generation
        </Button>
      </Container>

      {/* modal */}
      <Container style={{ fontFamily: "Roboto " }}>
        <Modal
          show={isOpen}
          onHide={hideModal}
          style={{ textAlign: "center" }}
          backdrop="static"
          keyboard={false}
          size="lg">
          <Modal.Header closeButton>
            <Modal.Title className="mt-3">
              <div style={{ fontFamily: "Roboto ", marginLeft: "17px" }}>
                Create Generation
              </div>
            </Modal.Title>
          </Modal.Header>

          <Modal.Body style={{ borderRadius: "20px" }}>
            <Form.Group>
              <Container-fluid>
                <Row>
                  <Col md={12} className="mb-4 mt-1">
                    <Col m={12} className="left_label">
                      <label className="label_title">Generation</label>
                    </Col>
                    <Col m={12}>
                      <Form.Control
                        type="number"
                        placeholder="Enter Generation"
                        className="mb-2 border-text-none"
                        onChange={(event) => setGeneration(event.target.value)}
                      />
                      {generationError ? (
                        <h6 className='text-danger' style = {{textAlign:'left'}}>{generationError}</h6>
                      ) : (
                        ''
                      )}
                    </Col>
                  </Col>
                </Row>
              </Container-fluid>

              <Button
                variant="info"
                style={{ fontFamily: "Roboto", width: "140px" }}
                className="mt-2"
                onClick={() => {
                  handleSubmit();
                }}>
                Create
              </Button>
            </Form.Group>
          </Modal.Body>
        </Modal>
      </Container>

      <GridContainer>
        {props.generationData.map((res) => (
          <CardArchived
            key={res.id}
            generation_id={res.id}
            generation_value={res.generation}
            activeGenertion={res.status}
            onHandleDeleteGen={handleDeleteGen}
            onHandleUpdateStatus={handleUpdateState}
          />
        ))}
      </GridContainer>
    </div>
  );
}
const mapStateToProp = (state) => {
  return {
    generationData: state.generationData.data,
    loading: state.generationData.loading,
    error: state.generationData.error,
  };
};

const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      getGeneration,
      postGeneration,
      deleteGeneration,
      updateGenerationStatus,
    },
    dp
  );
};
export default connect(mapStateToProp, mapDispatchToProps)(Archived);
