import React, { useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import '../../assets/css/StudentRegisterPage.css';
import { addStudent } from '../../redux/actions/studentDataAction';
import Swal from 'sweetalert2';

function StudentRegisterPage({ history }) {
  //student state
  const [studentID, setStudentID] = useState('');
  const [studentName, setStudentName] = useState('');
  let [studentClass, setStudentClass] = useState('');
  let [studentGender, setStudentGender] = useState('');
  //student error state
  const [studentIDError, setStudentIDError] = useState('');
  const [studentNameError, setStudentNameError] = useState('');
  const [studentClassError, setStudentClassError] = useState('');
  const [studentGenderError, setStudentGenderError] = useState('');

  const validate = () => {
    let studentIDError = '';
    let studentNameError = '';
    let studentClassError = '';
    let studentGenderError = '';
    let regex = /[!@#$%^&*(),.?":{}|<>1-9]/g;
    setStudentID(studentID.trim());
    setStudentClass(studentClass);
    setStudentName(studentName.trim());

    if (!studentID) {
      studentIDError = 'ID cannot be empty!';
    }

    
    if (studentIDError) {
      setStudentIDError(studentIDError);
    }else{
      setStudentIDError('')
    }

    if (studentID.trim() === '') {
      studentIDError = 'ID cannot be empty!';
      setStudentIDError(studentIDError);
    }

    if (!studentName) {
      studentNameError = 'Name cannot be empty!';
    }

    if (studentName.trim() === '') {
      studentNameError = 'Name cannot be empty!';
    }

    if (studentName.match(regex)){
      studentNameError = 'Name cannot be contain spectail character or number!';
    }

    if (studentNameError) {
      setStudentNameError(studentNameError);
    }else {
      setStudentNameError('');
    }

    if (!studentClass) {
      studentClassError = 'Class cannot be empty!';
    }

    if (studentClassError) {
      setStudentClassError(studentClassError);
    }else{
      setStudentClassError('');
    }

    if (studentClass.trim() === '') {
      studentClassError = 'Class cannot be empty!';
      setStudentClassError(studentClassError);
    }

    if (
      studentIDError ||
      studentNameError ||
      studentClassError ||
      studentGenderError
    ) {
      return false;
    }

    return true;
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (studentGender === '') studentGender = 'Male';

    const student = {
      cardID: studentID,
      name: studentName,
      className: studentClass,
      gender: studentGender,
    };

    const isValid = validate();

    if (isValid) {
      console.log(student);

      setStudentIDError('');
      setStudentNameError('');
      setStudentClassError('');
      setStudentGenderError('');

      addStudent(student, (msg) => {
        console.log(msg);
        if(msg!=="Student has been registered already"){
          Swal.fire("Successfully.", `${msg}`, "success").then((result) => {
            history.push("/");
          });
        }else{
          Swal.fire("Faild.", `${msg}`, "error").then((result) => {
          });
        }
      
       
      });
    }
  };

  return (
    <div className='row m-0 auth-wrapper-register font-face-en'>
      <form className='auth-inner-register'>
        <h3 style={{ fontWeight: 'bold' }} className='mb-5'>
          Register
        </h3>
        <Row>
          <Col xs={6}>
            <div className='form-group mb-5'>
              <label>ID Card</label>
              <input
                type='text'
                id='id_card'
                name='id'
                value={studentID.trim()}
                onChange={(event) => setStudentID(event.target.value)}
                className='form-control'
                placeholder='Enter ID Card'
                style={style.border_text}
              />
              {studentIDError ? (
                <h6 className='text-danger'>{studentIDError}</h6>
              ) : (
                ''
              )}
            </div>
          </Col>
          <Col xs={6}>
            <div className='form-group mb-5'>
              <label>Class</label>
              <input
                type='text'
                id='class'
                name='classname'
                value={studentClass.trim()}
                style={style.border_text}
                onChange={(event) => setStudentClass(event.target.value)}
                className='form-control'
                placeholder='Enter Class'
              />
              {studentClassError ? (
                <h6 className='text-danger'>{studentClassError}</h6>
              ) : (
                ''
              )}
            </div>
          </Col>
          <Col xs={6}>
            <div className='form-group mb-5'>
              <label>Name</label>
              <input
                type='text'
                id='student_name'
                name='studentname'
                value={studentName}
                onChange={(event) => setStudentName(event.target.value)}
                className='form-control'
                placeholder='Enter Name'
                style={style.border_text}
              />
              {studentNameError ? (
                <h6 className='text-danger'>{studentNameError}</h6>
              ) : (
                ''
              )}
            </div>
          </Col>
          <Col xs={6}>
            <div className='form-group mb-5'>
              <label>Gender</label>
              <select
                className='custom-select'
                id='class'
                name='studentgender'
                value={studentGender}
                style={style.border_text}
                onChange={(event) => setStudentGender(event.target.value)}>
                <option value='Male'>Male</option>
                <option value='Female'>Female</option>
              </select>
            </div>
          </Col>
        </Row>
        <button
          type='button'
          className='btn btn-info btn-block mb-4'
          style={{ width: '150px', marginLeft: 'auto' }}
          onClick={handleSubmit}>
          Submit
        </button>
      </form>
    </div>
  );
}

const style = {
  border_text: {
    borderTop: '0',
    borderButtom: '0',
    borderLeft: '0',
    borderRight: '0',
    borderRadius: '0',
    padding: '0',
  },
};

export default connect(null, { addStudent })(StudentRegisterPage);
