import React, { useState } from "react";
import "../../assets/css/TeacherLoginPage.css";
import { connect } from "react-redux";
import { login } from "../../redux/actions/teacherActions";
import { Button } from "react-bootstrap";
import { CustomLoading } from "../utils/Utils";

export const TeacherLoginPage = ({ login, loading, error }) => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUserNameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [validateServerError, setValidateServerError] = useState("");

  const isValid = () => {
    let nameError = "";
    let passwordError = "";
    if (!username) {
      nameError = "Name cannot be empty";
    }
    if (!password) {
      passwordError = "Password cannot be empty";
    }
    if (nameError || passwordError) {
      setUserNameError(nameError);
      setPasswordError(passwordError);
      return false;
    }
    return true;
  };

  const handleLogin = async (event) => {
    localStorage.setItem("username", username);
    event.preventDefault();
    const valid = isValid();
    if (valid) {
      setUserNameError("");
      setPasswordError("");
      const teacher = {
        username: username,
        password: password,
      };
      await login(teacher, (response) => {
        let token = null;
        if (response.status === 200) {
          token = response.data.token;
          if (token) {
            window.location.href = "/";
          } else {
            setValidateServerError(response.data.message);
          }
        }
        // window.location.href = "/";
      });
    }
  };

  if (error) {
    return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>There was an error</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
  }

  if (loading) {
    return (
      <div style={style.content}>
        <CustomLoading />
      </div>
    );
  }

  return (
    <div className="row m-0 auth-wrapper font-face-en">
      <form className="auth-inner">
        <h3 style={{ fontWeight: "bold" }}>Login</h3>
        <div className="mb-5 ">
          <p>KSHRD Online Exam System</p>
        </div>
        <div className="form-group mb-5">
          {validateServerError ? (
            <h6 className="text-danger">{validateServerError}</h6>
          ) : null}
          <label>Username</label>
          <input
            type="text"
            name="username"
            value={username.trim()}
            onChange={(event) => setUserName(event.target.value)}
            className="form-control"
            placeholder="Enter username"
            style={style.border_text}
          />
          {usernameError ? (
            <h6 className="text-danger">{usernameError}</h6>
          ) : null}
        </div>
        <div className="form-group mb-5">
          <label>Password</label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            className="form-control"
            placeholder="Enter password"
            style={style.border_text}
          />
          {passwordError ? (
            <h6 className="text-danger">{passwordError}</h6>
          ) : null}
        </div>
        <button
          type="button"
          className="btn btn-info btn-block mb-5"
          style={{ width: "150px", marginLeft: "auto" }}
          onClick={handleLogin}>
          Login
        </button>
        <div style={{ textAlign: "center" }}>
          <p>Korea Software HRD Center - 2020</p>
        </div>
      </form>
    </div>
  );
};

const style = {
  border_text: {
    borderTop: "0",
    borderButtom: "0",
    borderLeft: "0",
    borderRight: "0",
    borderRadius: "0",
    padding: "0",
  },
  content: {
    position: "absolute",
    left: "50%",
    top: "50%",
    WebkitTransform: "translate(-50%, -50%)",
    transform: "translate(-50%, -50%)",
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.teacherData.loading,
    error: state.teacherData.error,
  };
};

export default connect(mapStateToProps, { login })(TeacherLoginPage);
