import React from "react";
import { Card, Form, Row, Col } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
function TrueFalse(props) {

  return (
    <div>
      <Card className="cardTop" name="truefalseCard" >
        <Row>
          <Col lg={6}>
            <Form.Control
              className="boxCard"
              disabled
              type="text"
              name="point"
              defaultValue={props.point+" pt"}
    
             
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint+" pt"}
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>

        <Card.Body className="textTrueFalse" >
          <TextField
            className=" MuiInputBase-input-text"
            value={"     "+props.anwser}
          />
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>
      </Card>
    </div>
  );
}

export default TrueFalse;
