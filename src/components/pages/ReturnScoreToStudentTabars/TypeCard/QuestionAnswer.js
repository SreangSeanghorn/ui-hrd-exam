import React from "react";
import { Card, Form, Row, Col } from "react-bootstrap";
import { TextField } from "@material-ui/core";

function QuestionAnswer(props) {
  
  const getDataCard = (e) => {
  };
 
  return (
    <div>
      <Card
        onClick={(e)=> getDataCard(e)}
        style={{ width: "100%" }}
        className="cardTop"
        name="QACard"
        
        >
        <Row>
          <Col lg={6}>
            <Form.Control
              className="boxCard"
              disabled
              type="text"
              name="point"
              defaultValue={props.point+" pt"}
         
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint+" pt"}
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>
        <Card.Body className="textAnswer">
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>
        <Card.Body style={{ marginTop: "-10px" }}>
          {/* <textarea
            style={{
              width: "100%",
              borderTop: 0,
              borderLeft: 0,
              borderRight: 0,
            }}
            value={props.anwser}
            placeholder="Answer"
            className="textAnswer mt-1"
          /> */}
          <TextField
              id="standard-full-width"
              multiline
              fullWidth
              placeholder="Answer"
              name="answer"
              // className="px-2 textAnswer"
              value={props.anwser}
            />
        </Card.Body>
      </Card>
    </div>
  );
}
export default QuestionAnswer;
