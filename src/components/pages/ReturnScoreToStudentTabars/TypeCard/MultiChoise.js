import React from "react";
import { Card, Form, Row, Col } from "react-bootstrap";
function MultiChoise(props) {

  return (
    <div>
      <Card style={{ width: "100%" }} className="cardTop" >
        <Row>
          <Col lg={6}>
            <Form.Control
              className="boxCard"
              disabled
              type="text"
              name="point"
              defaultValue={props.point+" pt"}
            
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint+" pt"}
           
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>
        <Card.Body className="bodyAnswer textAnswer">
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>

        <Card.Body>
          <Form.Check
            type="checkbox"
            className="my-1 mr-sm-2 pl-5 textAnswer "
            id={`custom-inline-checkbox-1`}
            custom
            label={props.anw1}
            checked={props.isCorrect1 === "true" ? true : false}
            readOnly
          />
          <Form.Check
            type="checkbox"
            className="my-1 mr-sm-2 pl-5 textAnswer"
            id={`custom-inline-checkbox-2`}
            custom
            label={props.anw2}
            checked={props.isCorrect2 === "true" ? true : false}
            readOnly
          />
          <Form.Check
            type="checkbox"
            className="my-1 mr-sm-2 pl-5 textAnswer"
            id={`custom-inline-checkbox-3`}
            custom
            label={props.anw3}
            checked={props.isCorrect3 === "true" ? true : false}
            readOnly
          />
          <Form.Check
            type="checkbox"
            className="my-1 mr-sm-2 pl-5 textAnswer"
            id={`custom-inline-checkbox-4`}
            custom
            label={props.anw4}
            checked={props.isCorrect4 === "true" ? true : false}
            readOnly
          />
        </Card.Body>
      </Card>
      {/* <br/> */}
    </div>
  );
}
export default MultiChoise;
