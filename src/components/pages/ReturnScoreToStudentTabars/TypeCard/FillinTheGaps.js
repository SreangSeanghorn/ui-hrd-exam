import React from "react";
import { Card, Form, Col, Row } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
function FillinTheGaps(props) {
  return (
    <div>
      <Card style={{ width: "100%" }} className="cardTop" >
        <Row>
          <Col lg={6}>
            <Form.Control
              disabled
              type="text"
              className="boxCard"
              // type="number"
              name="point"
              defaultValue={props.point+" pt"}
           
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint+" pt"}
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>
        <Card.Body className="textAnswer">
          {props.index + 1 + ". "}
          {props.question}
        </Card.Body>
        <Card.Body>
          <TextField
            id="standard-basic"
            multiline
            fullWidth
            placeholder="Answer "
            value={props.anwser}
            className="text-input-fill"
          />
        </Card.Body>
      </Card>
    </div>
  );
}
export default FillinTheGaps;
