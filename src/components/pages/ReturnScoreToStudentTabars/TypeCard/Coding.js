import React, { useState } from "react";
import { Card, Form, Row, Col } from "react-bootstrap";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-java";
import "prismjs/components/prism-javascript";
//--- prism-themes
import 'prism-themes/themes/prism-atom-dark.css';
import parse from 'html-react-parser';
export default function Section5(props) {
  const [setJavaScriptCode] = useState('');

  const content = props.question;
  var dataSplit = content.split(/\n/);
  return (
    <div>
      <form>
        <br />
        <Card style={{ width: "100%", marginTop: '2px' }} >
        <Row>
          <Col lg={6}>
            <Form.Control
              disabled
              type="text"
              className="boxCard"
              name="point"
              defaultValue={props.point+" pt"}
          
            />
          </Col>
          <Col lg={6}>
            <Form.Control
              disabled
              className="boxCard"
              type="text"
              name="questioPoint"
              defaultValue={props.qPoint+" pt"}
             
              style={{ marginLeft: "77%" }}
            />
          </Col>
        </Row>
          <Card.Body className="textAnswer">
            {/* {props.index + 1}
            <span>. </span> */}
            {dataSplit.map((data) => parse(data, { trim: true }))}
          </Card.Body>
        </Card>
        <br />
        <Card style={{backgroundColor: "#878686"}}>
          <Editor
          disabled
            // value={javascriptCode}
            value={props.anwser}
            // onChange={props.handeChangeAnswer}
            onValueChange={(javascriptCode) =>
              setJavaScriptCode(javascriptCode)
            }
            highlight={(javascriptCode) =>
              highlight(javascriptCode, languages.javascript)
            }
            padding={10}
            tabSize={4}
            preClassName='#fff'
            style={{
              width: '100%',
              minHeight: '250px',
              fontFamily: '"Consolas", monospace',
              fontSize: 18,
            }}
          />
        </Card>
      </form>
    </div>
  );
}
