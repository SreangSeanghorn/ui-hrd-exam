import React, { useState, useEffect } from 'react';
import { Tab, Form, Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { getResultStudent } from '../../../redux/actions/getResultAction';
import { bindActionCreators } from 'redux';
import logoImage from '../../../assets/img/hrd_exam_logo.png';
import {  Col, Nav, Row } from 'react-bootstrap';
import '../../../assets/css/TabSection.css';
import { withRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { CustomLoading } from '../../utils/Utils';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-javascript';
//--- prism-themes
// import { highlight, languages } from "prismjs/components/prism-core";
import 'prism-themes/themes/prism-atom-dark.css';
import {getResultOneStudent} from "../../../redux/actions/getResultAction";
/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 21-08-2020
 * Development Group: HRD_EXAM
 * Description: This Check result student and give score  submit.
 **********************************************************************/

//import type Card
import TrueFalse from './TypeCard/TrueFalse';
import MultiChoise from './TypeCard/MultiChoise';
import FillinTheGaps from './TypeCard/FillinTheGaps';
import QuestionAnswer from './TypeCard/QuestionAnswer';
import Coding from './TypeCard/Coding';

const queryString = require('query-string');
function CheckResultStudentTabars(props) {
  const [questions, setQuestion] = useState([]);
  const [score, setScore] = useState(0);

  const { getResultStudent, resultStudentData } = props;

  const param = queryString.parse(props.location.search);
  const stuId = parseInt(props.location.state.studentId)
  console.log("stuId:", stuId);
  const quizId=parseInt(localStorage.getItem('studentCheckQuizId'))
  useEffect(() => {
    getResultOneStudent(quizId,stuId,(score=> {
      setScore(score)
      console.log("score:", score);
    }));

  }, [quizId, stuId]);
  useEffect(() => {
   
    getResultStudent(quizId,stuId);
  }, [getResultStudent, quizId, stuId]);
  useEffect(() => {
    if (resultStudentData) {
      setQuestion(resultStudentData);
    }
  }, [resultStudentData]);

  function naviagationToHome() {
    window.location.href = '/admin/response';
  }

  const changeScore = (question, e) => {
    const { questionIndex, sectionIndex, questionTypeId } = question;
    const section = questions.filterResponses[sectionIndex];
    const currentQuestion = section.response[questionIndex];
    currentQuestion.point = parseFloat(e.target.value);

    if (questionTypeId === 2) {
      const updateQuestions = [...questions.filterResponses];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      const filterResponses = {};
      filterResponses.filterResponses = updateQuestions;
      setQuestion(filterResponses);
    }
    if (questionTypeId === 1) {
      const updateQuestions = [...questions.filterResponses];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });

      const filterResponses = {};
      filterResponses.filterResponses = updateQuestions;
      setQuestion(filterResponses);
      // console.log(qu);
    }
    if (questionTypeId === 3) {
      const updateQuestions = [...questions.filterResponses];
      updateQuestions.map((q) => {
        // console.log("q:", q.response)
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      const filterResponses = {};
      filterResponses.filterResponses = updateQuestions;
      setQuestion(filterResponses);
    }
    if (questionTypeId === 4) {
      const updateQuestions = [...questions.filterResponses];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      const filterResponses = {};
      filterResponses.filterResponses = updateQuestions;
      setQuestion(filterResponses);
    }
    if (questionTypeId === 5) {
      const updateQuestions = [...questions.filterResponses];
      updateQuestions.map((q) => {
        // console.log("q:", q.response)
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      const filterResponses = {};
      filterResponses.filterResponses = updateQuestions;
      setQuestion(filterResponses);
    }
  };


  if (questions.filterResponses && questions.filterResponses.length > 0) {
    return (
      <>
        <div>
          {/* nav top */}
          <Tab.Container
            defaultActiveKey={
              questions.filterResponses.find(
                (question) => question.instruction === 'Section I'
              ).instructionId
            }
            id='tab-container'
            className='m-0'>
            <Row className=' m-0 nav_content-fixed' id='item'>
              <Col lg={2} className='fixed_nav' id='exam-logo'>
                <a
                  href='#/'
                  onClick={naviagationToHome}
                  style={{ cursor: 'pointer' }}>
                  <img
                    src={logoImage}
                    className='d-inline-block py-3 '
                    style={{ width: '137px' }}
                    alt='hrd-exam-logo'
                  />
                </a>
              </Col>

              <Col lg={7} className='nav_content'>
                <Row style={{ marginLeft: 'auto' }}>
                  <Nav variant='pills'>
                    <Col lg={12} className='pt-3 pl-5 '>
                      <Form inline>
                        {questions.filterResponses
                          .sort((a, b) =>
                            a.instruction.localeCompare(b.instruction)
                          )
                          .map((question) => {
                            return (
                              <Nav.Item key={question.instructionId}>
                                <Nav.Link
                                  eventKey={question.instructionId}
                                  className='nav-link text-nav  ml-3'>
                                  {question.instruction}
                                </Nav.Link>
                              </Nav.Item>
                            );
                          })}
                      </Form>
                    </Col>
                  </Nav>
                </Row>
              </Col>

              {/* button save */}
              <Col lg={3} className='fixed_nav'>
                <Form inline>
                  <Row>
                    <Col lg={6}>
                      <Row>
                        <Col lg={6}>
                          <Form.Label className='mr-2'>
                            <p
                              style={{
                                color: 'white',
                                fontSize: '18px',
                                marginLeft: '317px',
                                marginTop: '24px',
                              }}>
                              Score:
                            </p>
                          </Form.Label>
                        </Col>
                        <Col lg={6} className='py-3'>
                          <Form.Control
                            disabled
                            style={{ marginLeft: '133px' }}
                            className='textScore'
                            value={score}
                          
                            type='text'
                            placeholder='Score'
                          />
                        </Col>
                      </Row>
                    </Col>
                  
                  </Row>
                </Form>
              </Col>
            </Row>
            <Row className=' m-0 body-content'>
              <Col lg={12}>
                <Row>
                  <Col lg={2}></Col>
                  <Col lg={8}>
                    <Tab.Content>
                      <br />
                      <br />
                      {questions.filterResponses
                        .sort((a, b) =>
                          a.instruction.localeCompare(b.instruction)
                        )
                        .map((question, sectionIndex) => {
                          return (
                            // nav bar
                            <Tab.Pane
                              eventKey={question.instructionId}
                              key={question.instructionId}>
                              <Card>
                                <Card.Body className='font-weight-bold textQuestionTitle'>
                                  {question.instruction}
                                </Card.Body>
                                {/* title */}
                                <Card.Body className='pl-5  textQuestion'>
                                  {question.title}
                                </Card.Body>
                              </Card>
                              {/* get question  */}
                              {question.response.map((value, index) => {
                                switch (value.questionType_id) {
                                  case 2:
                                    return (
                                      <TrueFalse
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                        point={value.point}
                                        qPoint={value.qPoint}
                                        onChangeQuestionScore={changeScore}
                                       
                                        activeGen={param.status}
                                      />
                                    );
                                  case 1:
                                    return (
                                      <MultiChoise
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        question={value.question}
                                        index={index}
                                        point={value.point}
                                        qPoint={value.qPoint}
                                        activeGen={param.status}
                                        anw1={value.answer.option1.answer}
                                        isCorrect1={
                                          value.answer.option1.isCorrect
                                        }
                                        anw2={value.answer.option2.answer}
                                        isCorrect2={
                                          value.answer.option2.isCorrect
                                        }
                                        anw3={value.answer.option3.answer}
                                        isCorrect3={
                                          value.answer.option3.isCorrect
                                        }
                                        anw4={value.answer.option4.answer}
                                        isCorrect4={
                                          value.answer.option4.isCorrect
                                        }
                                        onChangeQuestionScore={changeScore}
                                  
                                      />
                                      // null
                                    );
                                  case 3:
                                    return (
                                      <FillinTheGaps
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                        point={value.point}
                                        qPoint={value.qPoint}
                                        onChangeQuestionScore={changeScore}
                                      
                                        activeGen={param.status}
                                      />
                                    );
                                  case 4:
                                    return (
                                      <QuestionAnswer
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                        point={value.point}
                                        qPoint={value.qPoint}
                                        onChangeQuestionScore={changeScore}
                                      
                                        activeGen={param.status}
                                      />
                                    );
                                  case 5:
                                    return (
                                      <Coding
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                        qPoint={value.qPoint}
                                        point={value.point}
                                        onChangeQuestionScore={changeScore}
                                      
                                        activeGen={param.status}
                                      />
                                      // null
                                    );
                                  default:
                                    return null;
                                }
                              })}
                            </Tab.Pane>
                          );
                        })}
                    </Tab.Content>
                  </Col>
                  <Col lg={2}></Col>
                </Row>
              </Col>
            </Row>
          </Tab.Container>
        </div>
      </>
    );
  }

  return (
    <div style={style.content}>
      <CustomLoading />
    </div>
  );
}

const style = {
  border_text: {
    borderTop: '0',
    borderButtom: '0',
    borderLeft: '0',
    borderRight: '0',
    borderRadius: '0',
    padding: '0',
  },
  content: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    WebkitTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
};

const mapStateToProps = (state) => {
  return {
    resultStudentData: state.getResultStudentData.data,
    score: state.getResultStudentData.score,
    laoding: state.getResultStudentData.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      getResultStudent,
      getResultOneStudent
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CheckResultStudentTabars));
