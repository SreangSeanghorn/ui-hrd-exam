import React, { useState } from "react";
import { connect } from "react-redux";
import { Image, Card } from "react-bootstrap";
import HeaderLayout from "../layouts/HeaderLayout";
import { FooterLayout } from "../layouts/FooterLayout";
import { CustomLoading } from "../utils/Utils";
import StudentLoginByCardModal from "./modals/StudentLoginByCardModal";

import StudentStartExamModal from "./modals/StudentStartExamModal";
import TeacherCreateQuizModal from "./modals/TeacherCreateQuizModal";
import "../../assets/css/HomePage.css";
import hatImage from "../../assets/img/graduate_hat.png";
import computer from "../../assets/img/computer.png";
import koreanFlag from "../../assets/img/korean_flag.png";
import { useEffect } from "react";
// import { bindActionCreators } from 'redux';
// import { getStudentQuizz } from "../../redux/actions/studentquizzAction";

const HomePage = ({ history, teacherData, studentData, quizzData }) => {
  // define state
  const [showStudentModal, setShowStudentModal] = useState(false);

  const [showStudentStartExamModal, setShowStudentStartExamModal] = useState(
    false
  );
  const [showTeacherModal, setShowTeacherModal] = useState(false);
  const [loginError, setLoginError] = useState("");
  const [serverError, setServerError] = useState("");
  // // destructure props
  const { teacherError, validToken, teacherLoading } = teacherData;
  const { studentError, studentLoading } = studentData;
  const { quizzesDataLoading, quizzesDataError } = quizzData;

  useEffect(() => {
    quizzesDataError || studentError || teacherError
      ? setServerError(quizzesDataError || studentError || teacherError)
      : setServerError("");
  }, [quizzesDataError, studentError, teacherError]);

  const navigateToStudentRegister = () => {
    setShowStudentModal(false);
    history.push({ pathname: "/student-register" });
  };
  const navigateToStudentCheckScore = () => {
    setShowStudentModal(false);
    history.push({ pathname: "/studentCheckScore" });
  };

  const navigateToAdmin = () => {
    history.push("/admin");
  };

  const handleStart = (response) => {
    if (response.data.status === "OK") {
      setShowStudentModal(false);
      setShowStudentStartExamModal(true);
      setLoginError("");
    } else {
      setLoginError(response.data.message);
    }
  };
  const handleCreate = (response) => {
    if (response.data.status === "OK") {
      setServerError("");
      setShowTeacherModal(false);
      history.push({
        pathname: "/create-quiz",
        state: { quizData: response.data.data },
      });
    }
    setServerError(response.data.message);
  };

  if (teacherLoading || studentLoading || quizzesDataLoading) {
    return (
      <div style={style.content}>
        <CustomLoading />
      </div>
    );
  }

  return (
    <>
      <HeaderLayout />
      {teacherLoading ? null : (
        <div>
          <div id="section-container" className="row m-0">
            <div
              id="section-hrd-exam-text"
              className="col-lg-6 col-md-6 col-sm-12 font-face-en text-white text-center pb-5 bgPrimaryColor">
              <h1>HRD Exam</h1>
              <h5>Korea Software HRD Center</h5>
              {validToken ? (
                <div className="btnItem">
                  <button
                    className="btnItemOne"
                    style={style.buttonStyle}
                    onClick={() => setShowTeacherModal(true)}>
                    Create Quiz
                  </button>
                  <button
                    className="btnItemTwo"
                    style={style.buttonStyle}
                    onClick={navigateToAdmin}>
                    Manage
                  </button>
                </div>
              ) : (
                <div>
                  <div className="btnItem">
                    <button
                      type="button"
                      className="btnItemOne"
                      onClick={() => setShowStudentModal(true)}
                      style={style.buttonStyle}>
                      Start Quiz
                    </button>
                    <button
                      type="button"
                      className="btnItemTwo"
                      onClick={navigateToStudentRegister}
                      style={style.buttonStyle}>
                      Register
                    </button>
                  </div>
                  <div className="btnItem mt-3">
                    <button
                      type="button"
                      className="btnItemTwo"
                      // onClick={() => setShowCheckScoreModal(true)}
                      onClick={navigateToStudentCheckScore}
                      style={style.buttonStyle}>
                      Check Result
                    </button>
                  </div>
                </div>
              )}
            </div>
            <div
              id="section-hrd-exam-logo"
              className="col-lg-6 col-md-6 col-sm-12 bgPrimaryColor"
            />
          </div>
          <div className="row m-0 px-5 pt-5">
            <div className="col-lg-6 col-md-6 col-sm-12 m-auto">
              <Card className="cardColumnStyle">
                <Card.Header className="cardHeaderStyle">
                  <Image
                    width="170"
                    roundedCircle
                    className="m-auto"
                    alt="graduate-hat"
                    src={hatImage}
                  />
                </Card.Header>
                <Card.Body
                  className="font-face-kh"
                  style={{ lineHeight: "initial" }}>
                  <Card.Title className="title">ការធ្វើតេស្ត Online</Card.Title>
                  <Card.Title className="title">
                    បង្កើតឡើងសម្រាប់សិស្ស HRD
                  </Card.Title>
                </Card.Body>
              </Card>
            </div>
          </div>
          <div className="row m-0 p-5">
            <div className="col-lg-6 col-md-6 col-sm-12 mb-5">
              <Card className="cardStyle">
                <Card.Header className="cardHeaderStyle">
                  <Image
                    roundedCircle
                    width="170"
                    className="cardImgStyle"
                    alt="computer"
                    src={computer}
                  />
                </Card.Header>
                <Card.Body
                  className="textContainer font-face-kh"
                  style={{ lineHeight: "initial" }}>
                  <Card.Title className="title">ការប្រលង IT</Card.Title>
                  <Card.Text className="description">
                    បានផ្តល់នៅសំណួរជាច្រើនប្រភេទដែលមានភាព
                    ងាយស្រួលក្នុងការបង្កើតសំនួរសម្រាប់ការប្រលង
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 mb-5">
              <Card className="cardStyle">
                <Card.Header className="cardHeaderStyle">
                  <Image
                    roundedCircle
                    className="cardImgStyle"
                    alt="korean-flag"
                    src={koreanFlag}
                  />
                </Card.Header>
                <Card.Body
                  className="textContainer font-face-kh"
                  style={{ lineHeight: "initial" }}>
                  <Card.Title className="title">ការប្រលងជាភាសាកូរ៉េ</Card.Title>
                  <Card.Text className="description">
                    បានផ្តល់នៅសំណួរជាច្រើនប្រភេទដែលមានភាព
                    ងាយស្រួលក្នុងការបង្កើតសំនួរសម្រាប់ការប្រលង
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
          </div>
          <FooterLayout />
          <StudentLoginByCardModal
            showStudentModal={showStudentModal}
            handleCloseStudentModal={() => setShowStudentModal(false)}
            handleStart={handleStart}
            loginError={loginError}
          />
          <StudentStartExamModal
            showStudentStartExamModal={showStudentStartExamModal}
            handleCloseStudentStartExamModal={() =>
              setShowStudentStartExamModal(false)
            }
          />

          <TeacherCreateQuizModal
            showTeacherModal={showTeacherModal}
            handleCloseTeacherModal={() => setShowTeacherModal(false)}
            handleCreate={handleCreate}
            serverError={serverError}
          />
        </div>
      )}
    </>
  );
};

const style = {
  textBox: {
    width: "95%",
    borderRadius: "5px",
    margin: "auto",
  },
  buttonStyle: {
    outline: "none",
    borderRadius: "5px",
  },
  content: {
    position: "absolute",
    left: "50%",
    top: "50%",
    WebkitTransform: "translate(-50%, -50%)",
    transform: "translate(-50%, -50%)",
  },
  modalTitle: {
    fontFamily: "Roboto ",
    fontWeight: "bold",
    textAlign: "center",
  },
};

const mapStateToProps = (state) => {
  return {
    teacherData: {
      teacherLoading: state.teacherData.loading,
      validToken: state.teacherData.validToken,
      teacher: state.teacherData.teacher,
      teacherError: state.teacherData.error,
    },
    studentData: {
      studentLoading: state.studentData.loading,
      student: state.studentData.student,
      studentError: state.studentData.error,
    },
    quizzData: {
      quizzesDataLoading: state.quizzesData.loading,
      quizzesDataError: state.quizzesData.error,
      quizzesData: state.quizzesData.data,
      quizzesDataMessage: state.quizzesData.message,
    },
  };
};

export default connect(mapStateToProps, {})(HomePage);
