import React, { useEffect, useState } from "react";
import { Table, Button } from "react-bootstrap";

import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for view quiz.
 **********************************************************************/
import { connect } from "react-redux";
import { returnQuizToStudent } from "../../redux/actions/quizAction";
import { bindActionCreators } from "redux";
import { CustomLoading } from "../../components/utils/Utils";
import StudentCheckScore from "./modals/StudentCheckScore";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 15-08-2020
 * Description: constroll student
 * Modification History : all day
 *     Date:
 *  Developer: Lon dara
 *
 *  TODO: - View quiz
 *
 **********************************************************************/

/**************************************************************************
 * This method it used to view students response answer.
 **************************************************************************/

const ResponseWork = (props) => {
  const { returnQuizToStudent } = props;
  useEffect(() => {
    returnQuizToStudent();
  }, [returnQuizToStudent]);
  const [showCheckScoreModal, setShowCheckScoreModal] = useState(false);
  // const [quizId, setquizId] = useState(0);
  const [loginError, setLoginError] = useState("");
  const handleStart = (response) => {
    if (response.data.status === "OK") {
      setLoginError("");
    } else {
      setLoginError(response.data.message);
    }
  };
  if (props.message === "Network Error")
    return (
      <div
        style={{
          textAlign: "center",
        }}>
        <h3>Error connecting</h3>
        <Button onClick={() => window.location.reload()}>Reload</Button>
      </div>
    );
  if (props.quizzesLoading) return <CustomLoading />;
  return (
    <div style={{ marginTop: "-26px" }}>
      <div>
        <Table hover className="size_table">
          <thead className="t_head">
            <tr>
              <th
                style={{
                  textAlign: "center",
                  color: "white",
                  fontSize: "14px",
                  width: "70px",
                }}>
                No
              </th>
              <th className="font_color" style={{ width: "350px" }}>
                Title
              </th>
              <th className="font_color">Subject</th>
              <th className="font_color">Create Date</th>
              <th className="font_color">Assign Date</th>
              <th
                style={{
                  textAlign: "center",
                  color: "white",
                  fontSize: "14px",
                  width: "150px",
                }}>
                Action
              </th>
            </tr>
          </thead>
          {props.quizzesData
            ? props.quizzesData.map((data, index) => {
                return (
                  <tbody className="t_bg" key={index}>
                    <tr>
                      <td style={{ borderTop: "0px", textAlign: "center" }}>
                        {" "}
                        {index + 1}
                      </td>
                      <td>{data.title}</td>
                      <td>{data.subject}</td>
                      <td>{data.date}</td>
                      <td>{data.date}</td>
                      <td style={{ textAlign: "center" }}>
                        {/* <Link
                          to={`/admin/response/reactpdf?id=${data.quizId}&&status=${activeGen}`}> */}
                        <button
                          className="btn btn-fill btn-success style_btn"
                          onClick={() => {
                            setShowCheckScoreModal(true);
                            // setquizId(data.id);
                            localStorage.setItem('studentCheckQuizId', data.id)
                          }}>
                          <VisibilityOutlinedIcon className="size_svg" />
                        </button>
                        {/* </Link> */}
                      </td>
                    </tr>
                  </tbody>
                );
              })
            : null}
        </Table>
      </div>
      <StudentCheckScore
        showCheckScoreModal={showCheckScoreModal}
        handleCloseStudentModal={() => setShowCheckScoreModal(false)}
        handleStart={handleStart}
        loginError={loginError}
      />
    </div>
  );
};
const mapStateToProp = (state) => {
 
  return {
    quizzesData: state.quizzesData.data,
    quizzesLoading: state.quizzesData.loading,
    message: state.quizzesData.error,
  };
};
const mapDispatchToProps = (dp) => {
  return bindActionCreators(
    {
      returnQuizToStudent,
    },
    dp
  );
};

export default connect(mapStateToProp, mapDispatchToProps)(ResponseWork);
