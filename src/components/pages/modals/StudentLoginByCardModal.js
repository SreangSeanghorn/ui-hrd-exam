import React, { useState, useEffect } from "react";
import { Form, Button, Container } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import loginByCardID from "../../../redux/actions/studentActions";
import { getStudentQuizz } from "../../../redux/actions/studentquizzAction";
import validateStartQuizAction from "../../../redux/actions/validateStartQuizAction";
import Swal from "sweetalert2";

const StudentLoginByCardModal = ({
  showStudentModal,
  handleCloseStudentModal,
  handleStart,
  loginByCardID,
  loginError,
  getStudentQuizz,
  quizData,
  validateStartQuizAction,
  quizId,
}) => {
  const [cardID, setCardID] = useState("");
  const [cardIDError, setCardIDError] = useState("");

  const isValid = () => {
    let cardidError = "";
    if (!cardID) {
      cardidError = "Card ID cannot be empty!";
    }
    if (cardidError) {
      setCardIDError(cardidError);
      return false;
    }
    return true;
  };

  useEffect(() => {
    getStudentQuizz();
    if (loginError) {
      setCardIDError(loginError + "!");
    }
  }, [getStudentQuizz, loginError]);
  return (
    <Container>
      <Modal
        show={showStudentModal}
        onHide={() => {
          setCardIDError("");
          handleCloseStudentModal();
        }}
        style={{ textAlign: "center" }}
        centered
        backdrop="static"
        keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title className="mt-2">
            <div style={{ fontFamily: "Roboto ", marginLeft: "10px" }}>
              Student ID
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ borderRadius: "20px", marginTop: "-7px" }}>
          <Form.Group>
            <Form.Control
              value={cardID}
              type="text"
              placeholder="Enter Card ID"
              className="mb-3"
              style={style.textBox}
              name="cardid"
              onChange={(event) => {
                setCardID(event.target.value);
              }}
            />
            {cardIDError ? (
              <h6 className="pl-3 text-left invalid-feedback d-block">
                {cardIDError}
              </h6>
            ) : null}
            <Button
              variant="info"
              style={{
                fontFamily: "Roboto",
                width: "140px",
                marginBottom: "-7px",
              }}
              className="mt-2"
              onClick={(event) => {
                event.preventDefault();
                const valid = isValid();
                if (valid) {
                  setCardIDError("");
                  if (quizData.length < 1) {
                    Swal.fire("Unavailable", "Not yet have Quiz", "error").then(
                      (res) => {
                        window.location.href = "/";
                      }
                    );
                  }
                  loginByCardID(cardID, (response) => {
                    validateStartQuizAction(cardID, quizId, (msg) => {
                      if (msg === "Cannot take the exam twice") {
                        Swal.fire("Unavailable!", `${msg}`, "error").then(
                          (res) => {
                            window.location.href = "/";
                          }
                        );
                      } else {
                        handleStart(response);
                      }
                    });
                  });
                }
              }}>
              Start
            </Button>
          </Form.Group>
        </Modal.Body>
      </Modal>
    </Container>
  );
};

const style = {
  textBox: {
    width: "95%",
    borderRadius: "5px",
    margin: "auto",
  },
  buttonStyle: {
    outline: "none",
    borderRadius: "5px",
  },
  content: {
    position: "absolute",
    left: "50%",
    top: "50%",
    WebkitTransform: "translate(-50%, -50%)",
    transform: "translate(-50%, -50%)",
  },
  modalTitle: {
    fontFamily: "Roboto ",
    fontWeight: "bold",
    textAlign: "center",
  },
};

const mapStateToProps = (state) => {
  return {
    studentQuizzData: state.studentQuizzData.data,
    quizId: state.studentQuizzData.quizId,
    loading: state.studentQuizzData.loading,
    duration: state.studentQuizzData.duration,
    quizData: state.studentQuizzData.data,
  };
};
const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      loginByCardID,
      getStudentQuizz,
      validateStartQuizAction,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(StudentLoginByCardModal));
