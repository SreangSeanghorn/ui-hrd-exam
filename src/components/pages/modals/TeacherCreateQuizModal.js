import React, { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { insertQuiz } from '../../../redux/actions/quizAction';
import { useEffect } from 'react';

const TeacherCreateQuizModal = ({
  showTeacherModal,
  handleCloseTeacherModal,
  handleCreate,
  insertQuiz,
  serverError,
}) => {
  //--= title, subject, duration state
  const [title, setTitle] = useState('');
  const [subject, setSubject] = useState('');
  const [duration, setDuration] = useState('');
  const [titleError, setTitleError] = useState('');
  const [subjectError, setSubjectError] = useState('');
  const [durationError, setDurationError] = useState('');
  const [quizError, setQuizError] = useState('');

  useEffect(() => {
    serverError ? setQuizError(serverError) : setQuizError('');
  }, [serverError]);

  const validate = () => {
    let titleError = null;
    let subjectError = null;
    let durationError = null;
    setTitle(title.trim());
    setSubject(subject.trim());
    if (!title) {
      titleError = 'Title cannot be empty';
      setTitleError(titleError);
    } else {
      setTitleError('');
    }
    if (!subject) {
      subjectError = 'Subject cannot be empty';
      setSubjectError(subjectError);
    } else {
      setSubjectError('');
    }

    if (subject.trim() === '') {
      setSubjectError('Subject cannot be empty');
    }

    if (!duration) {
      durationError = 'Duration cannot be empty';
      setDurationError(durationError);
    } else {
      setDurationError('');
    }

    if (titleError || subjectError || durationError) {
      return false;
    }
    return true;
  };

  return (
    <Container>
      <Modal
        show={showTeacherModal}
        onHide={() => {
          handleCloseTeacherModal(false);
          setQuizError('');
        }}
        style={{ textAlign: 'center' }}
        centered
        backdrop='static'
        keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title className='mt-3'>
            <div style={style.modalTitle}>
              Create Exam
              {quizError ? <p className='text-danger'>{quizError}</p> : null}
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ borderRadius: '20px' }}>
          <Form.Group>
            <Form.Control
              value={title}
              type='text'
              placeholder='Title'
              className='mb-2'
              style={style.textBox}
              name='title'
              onChange={(event) => setTitle(event.target.value)}
            />
            {titleError ? (
              <h6 className='pl-3 text-left text-danger'>{titleError}</h6>
            ) : null}
            <Form.Control
              value={subject}
              type='text'
              placeholder='Subject'
              className='mb-2 mt-3'
              style={style.textBox}
              name='subject'
              onChange={(event) => setSubject(event.target.value)}
            />
            {subjectError ? (
              <h6 className='pl-3 text-left text-danger'>{subjectError}</h6>
            ) : null}
            <Form.Control
              value={duration}
              type='number'
              placeholder='Duration (mns)'
              className='mb-3 mt-3'
              style={style.textBox}
              name='duration'
              onChange={(event) => setDuration(event.target.value)}
            />
            {durationError ? (
              <h6 className='pl-3 text-left text-danger'>{durationError}</h6>
            ) : null}
            <Button
              variant='info'
              style={{ width: '140px' }}
              className='mt-3'
              onClick={(event) => {
                event.preventDefault();
                const date = new Date(Date.now());
                let parseDuration = duration;
                parseDuration = Number(duration);
                const valid = validate();
                if (valid) {
                  const newQuiz = {
                    title: title,
                    subject: subject,
                    date: date,
                    duration: parseDuration,
                  };
                  setTitleError('');
                  setSubjectError('');
                  setDurationError('');
                  insertQuiz(newQuiz, (response) => {
                    handleCreate(response);
                  });
                }
              }}>
              CREATE
            </Button>
          </Form.Group>
        </Modal.Body>
      </Modal>
    </Container>
  );
};

const style = {
  textBox: {
    width: '95%',
    borderRadius: '5px',
    margin: 'auto',
  },
  buttonStyle: {
    outline: 'none',
    borderRadius: '5px',
  },
  content: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    WebkitTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
  modalTitle: {
    fontFamily: 'Roboto ',
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: '13px',
  },
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      insertQuiz,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(
  null,
  mapDispatchToProps
)(withRouter(TeacherCreateQuizModal));
