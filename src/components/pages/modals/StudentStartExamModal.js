import React, { useEffect } from "react";
import { Form, Button, Container } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Auth from '../../auth/Auth'
const StudentStartExamModal = (props) => {
  const {
    showStudentStartExamModal,
    handleCloseStudentStartExamModal,
    student,
    history,
  } = props;

  // useEffect(() => {
   
  //   if (student) {
  //     if (student.status === "OK") {
  //       let studentId = student.data.studentId;
  //       console.log("===================>studentId:", studentId)
  //       localStorage.setItem("studentId", studentId);
  //     }
  //   }
  // }, [student]);
  const handleStartNow = (event) => {
    event.preventDefault();
    Auth.login(() => {
    history.push({
      pathname: "/student-doing-exam",
      state: { studentId: props.student.studentId},
    });
  })
  };
  return (
    <Container>
      <Modal
        show={showStudentStartExamModal}
        onHide={handleCloseStudentStartExamModal}
        style={{ textAlign: "center" }}
        centered
        backdrop="static"
        keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title style={{ marginLeft: "25px" }} className="mt-2">
            <div style={{ fontFamily: "Roboto " }}>Let's start the exam</div>
          </Modal.Title>
        </Modal.Header>

        <Modal.Body style={{ borderRadius: "20px" }}>
          <Form.Group>
            {" "}
            <h4 style={{ color: "red" }} className="pl-0">
              <strong>Rules*</strong>
            </h4>
            <ul style={{ textAlign: "left" }}>
              <li>Can't close or open another page.</li>
              <li>No Cheating.</li>
              <li>No Sharing.</li>
              <li>No Dicussion</li>
            </ul>
            <Button
              variant="info"
              style={{ fontFamily: "Roboto", width: "140px" }}
              className="mt-2"
              onClick={handleStartNow}>
              Start Exam
            </Button>
          </Form.Group>
        </Modal.Body>
      </Modal>
    </Container>
  );
};
const mapStateTOProps = (state) => {
  return {
    student: state.studentData.student,
  };
};
export default connect(mapStateTOProps)(withRouter(StudentStartExamModal));
