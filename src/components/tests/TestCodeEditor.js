import React, { useState } from "react";
import Editor from "react-simple-code-editor";
import { Button } from "react-bootstrap";
//--- prismjs
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-java";
import "prismjs/components/prism-javascript";
//--- prism-themes
import "prism-themes/themes/prism-atom-dark.css";

export const TestCodeEditor = () => {
  const [javascriptCode,setJavaScriptCode] = useState("");
  const [javaCode, setJavaCode] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(javascriptCode + "\n\n" + javaCode);
  };

  return (
    <div className="primaryBgColorOne text-center p-3 text-white robotoFont">
      <h1 className="font-weight-bold">Test Code Editor</h1>
      <form>
        <h3 className="font-weight-bold my-3">Section III. Coding</h3>
        <h3 className="font-weight-bold">1. Write React Code Example?</h3>
        <Editor
                    value={javascriptCode}
                    onValueChange={javascriptCode => setJavaScriptCode(javascriptCode)}
                    highlight={javascriptCode => highlight(javascriptCode, languages.javascript)}
                    padding={10}
                    tabSize={4}
                    preClassName="text-white"
                    style={{ background: "#1d1f21", color: "#fff" }}
                />

        <h3 className="font-weight-bold my-3">2. Write Java Code Example?</h3>
        <Editor
                    value={javaCode}
                    onValueChange={javaCode => setJavaCode(javaCode)}
                    highlight={javaCode => highlight(javaCode, languages.java)}
                    padding={10}
                    style={{ background: "#1d1f21", color: "#fff" }}
                />

        {/* <Editor
          value={javaCode}
          onValueChange={(javaCode) => setJavaCode({ javaCode })}
          highlight={(javaCode) => highlight(javaCode, languages.js)}
          padding={10}
        //   preClassName="#fff"
          style={{
            fontFamily: '"Fira code", "Fira Mono", monospace',
            fontSize: 12,
            backgroundColor: "#000",
          }}
        /> */}

        <Button
          type="submit"
          variant="dark"
          className="mt-3 font-weight-bold"
          onClick={handleSubmit}>
          Submit
        </Button>
      </form>
    </div>
  );
};

