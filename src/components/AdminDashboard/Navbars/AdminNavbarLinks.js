import React from "react";
import Grid from "@material-ui/core/Grid";
import logout from "../../../assets/img/logout.png";


export default function AdminNavbarLinks() {
  
  const onLogout = () => {
    localStorage.clear();
    window.location.href = '/';
  };
  
 
  return (
    <div style={{marginBottom: -15}}>
      <button style={styles.btn} onClick={onLogout}>
        <Grid container spacing={1}>
          <Grid item xs={10}>
            <p>Logout</p>
          </Grid>
          <Grid item xs={2}>
            <img src={logout} style={styles.img} alt= "logout"/>
          </Grid>
        </Grid>
      </button>
    </div>
  );
}
const styles = {
  btn: {
    border: "none",
    backgroundColor: "transparent",
    outline: "none"
  },
  img: {
    marginLeft: "-10px",
    height: "24px",
  },
};
