import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { Container } from 'react-bootstrap';

function FilltheGraps(props) {
  //--- define state
  const [fillTheGaps, setFillTheGaps] = useState('');
  const [point, setPoint] = useState(0);
  const [pointStateError, setPointStateError] = useState('');
  //--- destructure props
  const {
    onQuestionChange,
    sectionIndex,
    questionIndex,
    question: { fillTheGapsError, pointError },
  } = props;
  const questionPass = {
    sectionIndex,
    questionIndex,
  };

  const onPointChange = (event) => {
    let inputValue = event.target.value;
    if (inputValue === '') {
      setPointStateError('Point cannot be empty!');
      setPoint(inputValue);
    } else if (!inputValue.match(/^[0-9]+$/)) {
      setPointStateError('Invalid number!');
      setPoint(inputValue);
    } else {
      setPointStateError('');
      setPoint(inputValue);
    }
    onQuestionChange(questionPass, event);
  };

  return (
    <Container className='mt-3'>
      <TextField
        name='fillTheGaps'
        value={fillTheGaps}
        onChange={(event) => {
          setFillTheGaps(event.target.value);
          onQuestionChange(questionPass, event);
        }}
        style={{ width: '530px', marginTop: '11px' }}
        placeholder='Question 1)...... 2)......... ]'
        multiline
        fullWidth
      />
      {fillTheGapsError ? (
        <h6 className='text-danger bg-light'>{fillTheGapsError}</h6>
      ) : null}
      <TextField
        type='number'
        className='px-2 MuiInputBase-input-text'
        name='point'
        value={point}
        onChange={onPointChange}
        inputProps={{ style: styles.align }}
        style={styles.textField}
        multiline
        placeholder='points'
      />
      {pointError || pointStateError ? (
        <h6 className='text-danger'>{pointError || pointStateError}</h6>
      ) : null}
    </Container>
  );
}

const styles = {
  textField: {
    width: '113px',
    marginTop: '12px'
    
  },
  align: {
    textAlign: 'center',

  },
};

export default FilltheGraps;
