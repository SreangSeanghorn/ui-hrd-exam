import React from 'react';
import { components } from 'react-select';
import PowerInputIcon from '@material-ui/icons/PowerInput';
import BallotOutlinedIcon from '@material-ui/icons/BallotOutlined';
import NotesOutlinedIcon from '@material-ui/icons/NotesOutlined';
import SpellcheckOutlinedIcon from '@material-ui/icons/SpellcheckOutlined';
import CodeIcon from '@material-ui/icons/Code';

export const options = [
  {
    questionTypeId: 1,
    value: 'Multiple Choice',
    label: 'Multiple Choice',
    icon: <BallotOutlinedIcon />,
    multiQuestion: '',
    multiAnswerOne: '',
    checkOne: '',
    multiAnswerTwo: '',
    checkTwo: '',
    multiAnswerThree: '',
    checkThree: '',
    multiAnswerFour: '',
    checkFour: '',
    point: 0,
  },
  {
    questionTypeId: 2,
    value: 'TrueFalse',
    label: 'True/False',
    icon: <SpellcheckOutlinedIcon />,
    trueFalseValue: '',
    trueFalseQuestion: '',
    point: 0,
  },
  {
    questionTypeId: 3,
    value: 'Fill in the gaps',
    label: 'Fill in the gaps',
    icon: <PowerInputIcon />,
    fillTheGaps: '',
    point: 0,
  },
  {
    questionTypeId: 4,
    value: 'Question',
    label: 'Question',
    icon: <NotesOutlinedIcon />,
    question: '',
    point: 0,
  },
  {
    questionTypeId: 5,
    value: 'Code',
    label: 'Code',
    icon: <CodeIcon />,
    coding: '',
    image: null,
    point: 0,
  },
];

const { Option } = components;

export const IconOption = (props) => (
  <Option {...props}>
    <span>{props.data.icon} &nbsp;</span>
    {props.data.label}
  </Option>
);

export const CustomSelectValue = (props) => (
  <div>
    <span>{props.data.icon} &nbsp;</span>
    {props.data.label}
  </div>
);
