import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { Container } from 'react-bootstrap';

const Checkbox = (props) => <input type='checkbox' {...props} />;

function Multi(props) {
  //--- define state
  const [multiQuestion, setMultiQuestion] = useState(props.question.question);
  const [point, setPoint] = useState(0);
  const [pointStateError, setPointStateError] = useState('');
  //--- answer
  const [multiAnswerOne, setMultiAnswerOne] = useState('');
  const [multiAnswerTwo, setMultiAnswerTwo] = useState('');
  const [multiAnswerThree, setMultiAnswerThree] = useState('');
  const [multiAnswerFour, setMultiAnswerFour] = useState('');
  //--- check
  const [checkOne, setCheckOne] = useState(false);
  const [checkTwo, setCheckTwo] = useState(false);
  const [checkThree, setCheckThree] = useState(false);
  const [checkFour, setCheckFour] = useState(false);
  //--- destructure props
  const {
    sectionIndex,
    questionIndex,
    onQuestionChange,
    question: {
      multiQuestionError,
      multiAnswerOneError,
      multiAnswerTwoError,
      multiAnswerThreeError,
      multiAnswerFourError,
      pointError,
    },
  } = props;
  const questionPass = {
    sectionIndex,
    questionIndex,
  };

  const onPointChange = (event) => {
    let inputValue = event.target.value;
    if (inputValue === '') {
      setPointStateError('Point cannot be empty!');
      setPoint(inputValue);
    } else if (!inputValue.match(/^[0-9]+$/)) {
      setPointStateError('Invalid number!');
      setPoint(inputValue);
    } else {
      setPointStateError('');
      setPoint(inputValue);
    }
    onQuestionChange(questionPass, event);
  };

  return (
    <Container>
      <div className='mt-3 mb-3'>
        <TextField
          name='multiQuestion'
          value={multiQuestion}
          onChange={(event) => {
            setMultiQuestion(event.target.value);
            onQuestionChange(questionPass, event);
          }}
          style={{ width: '530px', marginTop: '11px' }}
          placeholder='Question...'
          multiline
          fullWidth
        />
        {multiQuestionError ? (
          <h6 className='text-danger'>{multiQuestionError}</h6>
        ) : null}
      </div>
      <div>
        <label>
          <Checkbox
            className='sizeBox'
            checked={checkOne}
            name='checkOne'
            onChange={(event) => {
              setCheckOne(event.target.checked);
              onQuestionChange(questionPass, event);
            }}
          />
          &nbsp;
          <TextField
            style={{ width: '509px' }}
            name='multiAnswerOne'
            value={multiAnswerOne}
            onChange={(event) => {
              setMultiAnswerOne(event.target.value);
              onQuestionChange(questionPass, event);
            }}
            placeholder='answer...'
          />
          {multiAnswerOneError ? (
            <h6 className='text-danger'>{multiAnswerOneError}</h6>
          ) : null}
        </label>
      </div>
      <div className='mt-3'>
        <label>
          <Checkbox
            className='sizeBox'
            checked={checkTwo}
            name='checkTwo'
            onChange={(event) => {
              setCheckTwo(event.target.checked);
              onQuestionChange(questionPass, event);
            }}
          />
          &nbsp;
          <TextField
            style={{ width: '509px' }}
            name='multiAnswerTwo'
            value={multiAnswerTwo}
            onChange={(event) => {
              setMultiAnswerTwo(event.target.value);
              onQuestionChange(questionPass, event);
            }}
            placeholder='answer...'
          />
          {multiAnswerTwoError ? (
            <h6 className='text-danger'>{multiAnswerTwoError}</h6>
          ) : null}
        </label>
      </div>
      <div className='mt-3'>
        <label>
          <Checkbox
            className='sizeBox'
            checked={checkThree}
            name='checkThree'
            onChange={(event) => {
              setCheckThree(event.target.checked);
              onQuestionChange(questionPass, event);
            }}
          />
          &nbsp;
          <TextField
            style={{ width: '509px' }}
            name='multiAnswerThree'
            value={multiAnswerThree}
            onChange={(event) => {
              setMultiAnswerThree(event.target.value);
              onQuestionChange(questionPass, event);
            }}
            placeholder='answer...'
          />
          {multiAnswerThreeError ? (
            <h6 className='text-danger'>{multiAnswerThreeError}</h6>
          ) : null}
        </label>
      </div>
      <div className='mt-3'>
        <label>
          <Checkbox
            className='sizeBox'
            checked={checkFour}
            name='checkFour'
            onChange={(event) => {
              setCheckFour(event.target.checked);
              onQuestionChange(questionPass, event);
            }}
          />
          &nbsp;
          <TextField
            style={{ width: '509px' }}
            name='multiAnswerFour'
            value={multiAnswerFour}
            onChange={(event) => {
              setMultiAnswerFour(event.target.value);
              onQuestionChange(questionPass, event);
            }}
            placeholder='answer...'
          />
          {multiAnswerFourError ? (
            <h6 className='text-danger'>{multiAnswerFourError}</h6>
          ) : null}
        </label>
      </div>
      <div className='mt-3'>
        <TextField
          type='number'
          className='px-2 MuiInputBase-input-text'
          name='point'
          value={point}
          onChange={onPointChange}
          inputProps={{ style: styles.align }}
          style={styles.textField}
          multiline
          placeholder='points'
        />
        {pointError || pointStateError ? (
          <h6 className='text-danger'>{pointError || pointStateError}</h6>
        ) : null}
      </div>
    </Container>
  );
}

const styles = {
  textField: {
    width: '113px',
  },
  align: {
    textAlign: 'center',
  },
};

export default Multi;
