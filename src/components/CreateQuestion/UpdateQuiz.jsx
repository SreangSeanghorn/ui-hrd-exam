import React, { Component, Fragment } from 'react';
import {
  Navbar,
  Nav,
  Button,
  Form,
  Row,
  Col,
  Card,
  Container,
} from 'react-bootstrap';
import {
  Link,
  //Redirect
} from 'react-router-dom';
import Select from 'react-select';
import Axios from 'axios';
import { BASE_URL } from '../../config/API';
import { CustomLoading } from '../utils/Utils';
import Section from './Section/Section';
import TrueFalse from './TrueFalse/TrueFalse';
import QuestionAndAnswer from './QuestionAnswer/QuestionAnswer';
import Multi from './Multi/Multi';
// import { arabicToRoman } from '../utils/Utils';
import { options, IconOption, CustomSelectValue } from './QuestionOptions';
import FilltheGraps from './FilltheGraps/FilltheGraps';
import Coding from './Coding/Coding';
import { IconButton } from '@material-ui/core';
//Icon
import logoImage from '../../assets/img/hrd_exam_logo.png';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import QueueOutlinedIcon from '@material-ui/icons/QueueOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
//import SpellcheckOutlinedIcon from '@material-ui/icons/SpellcheckOutlined';
//import NotesOutlinedIcon from '@material-ui/icons/NotesOutlined';
//import BallotOutlinedIcon from '@material-ui/icons/BallotOutlined';
//import PowerInputIcon from '@material-ui/icons/PowerInput';
//import CodeIcon from '@material-ui/icons/Code';

class UpdateQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quizId: this.props.match.params.id
        ? Number(this.props.match.params.id)
        : 0,
      quizData: {},
      sections: [],
      loading: false,
    };
  }

  componentDidMount() {
    //--- check token valid
    const token = localStorage.getItem('jwtToken');
    if (token) {
      //--- find questions
      this.getQuestionsByQuizId(this.state.quizId)
        .then((res) => {
          this.initSections(res);
        })
        .catch((error) => console.log(error));
    } else {
      this.props.history.push('/');
    }
  }

  render() {
    const { quizData, loading, sections } = this.state;
    // console.log('section-state: ', sections);
    if (loading || Object.keys(quizData).length === 0) {
      return <CustomLoading />;
    }
    return (
      <div className='container-fluid px-0 mb-5'>
        <Navbar expand='lg' style={style.navbar}>
          <Navbar.Brand href='#home' as={Link} to='/'>
            <img src={logoImage} className='w-50' alt='hrd-exam-logo' />
          </Navbar.Brand>
          <Nav style={style.navContainer}>
            <h4 style={style.navTitle}>
              Online {quizData ? quizData.title : null}
            </h4>
            <p style={style.navDesc}>
              Subject: {quizData ? quizData.subject : null} &nbsp;&nbsp; |
              &nbsp;&nbsp; Duration: {quizData ? quizData.duration : null}mn
            </p>
          </Nav>
          <Nav
            style={{
              fontSize: '16px',
              color: 'white',
              fontWeight: 'bold',
              paddingRight: '15px',
            }}>
            <p
              as={Link}
              to={`/create-quiz/previewData`}
              target='_blank'
              onClick={this.handlePreview}
              style={{ cursor: 'pointer', marginBottom: '-20px' }}>
              Preview
            </p>{' '}
            &nbsp;&nbsp;&nbsp;&nbsp; |
          </Nav>
          <Form inline className='pl-3'>
            <Button
              type='button'
              style={style.btnSave}
              className='btn-info'
              disabled={false}
              onClick={this.handleUpdate}>
              <DoneAllIcon className='fillIcon' />
              <span style={style.spanText}>Update</span>
            </Button>
          </Form>
        </Navbar>
        <Form className='mt-2'>
          {sections
            .sort((a, b) => a.instruction.localeCompare(b.instruction))
            .map((section, sectionIndex) => (
              <Fragment key={section.instructionId}>
                <Section
                  sectionIndex={sectionIndex}
                  section={this.state.sections[sectionIndex]}
                  handleChange={(event) =>
                    this.onSectionChange(event, sectionIndex)
                  }
                />
                {section.questions.map((question, questionIndex) => (
                  <Fragment key={question.questionId}>
                    <Card className='my-2 mb-3 mt-3 styleCard'>
                      <Card.Body className='styleBody'>
                        <Container>
                          <Row>
                            <Col
                              xs={3}
                              style={{ textAlign: 'left' }}
                              className='mt-3 ml-auto'>
                              <Select
                                value={question.options.find(
                                  (q) => q.value === question.value
                                )}
                                onChange={(event) =>
                                  this.handleOptionsChange(
                                    sectionIndex,
                                    questionIndex,
                                    event
                                  )
                                }
                                options={question.options}
                                components={{
                                  Option: IconOption,
                                  SingleValue: CustomSelectValue,
                                }}
                              />
                            </Col>
                            <Col xs={12}>
                              {this.renderComponent(
                                sectionIndex,
                                questionIndex,
                                question.questionType_id
                              )}
                            </Col>
                          </Row>
                        </Container>
                      </Card.Body>
                      <Card.Footer className='cardFooter styleFooter'>
                        <IconButton
                          className='tooltip-icon'
                          onClick={(event) =>
                            this.handleAddQuestion(
                              sectionIndex,
                              questionIndex,
                              event
                            )
                          }>
                          <AddCircleOutlineIcon className='fillIcon' />
                          <span className='tooltiptext'>Add question</span>
                        </IconButton>
                        <IconButton
                          className='tooltip-icon'
                          onClick={(event) =>
                            this.handleAddSection(event, sectionIndex)
                          }>
                          <QueueOutlinedIcon className='fillIcon' />
                          <span className='tooltiptext'>Add title</span>
                        </IconButton>
                        <IconButton
                          className='tooltip-icon'
                          disabled={
                            sections.length === 1 &&
                            section.questions.length === 1
                              ? true
                              : false
                          }
                          onClick={(event) =>
                            this.handleDelete(
                              event,
                              sectionIndex,
                              question.questionId
                            )
                          }>
                          <DeleteOutlineOutlinedIcon
                            className={
                              sections.length === 1 &&
                              section.questions.length === 1
                                ? ''
                                : 'fillIcon'
                            }
                          />
                          <span className='tooltiptext'>Delete question</span>
                        </IconButton>
                      </Card.Footer>
                    </Card>
                  </Fragment>
                ))}
              </Fragment>
            ))}
        </Form>
      </div>
    );
  }

  getQuestionsByQuizId = async (quizId) => {
    const questions = await Axios.get(`${BASE_URL}/quizzes/details/${quizId}`)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.status === 'OK') {
            if (res.data.data.length === 0) {
              console.log(res.data.data);
              window.location.href = '/';
            } else {
              return res;
            }
          } else {
            window.location.href = '/';
          }
        }
        console.log('error: ', res.status);
      })
      .catch((error) => {
        console.log('get-questions-by-quizID-error: ', error);
        return null;
      });
    return questions;
  };

  initSections = (res) => {
    let sections = [];
    const data = res.data.data;
    const quizData = {
      date: data.quizData,
      duration: data.duration,
      generation_id: data.generation_id,
      quizId: data.quizId,
      subject: data.subject,
      teacher_id: data.teacher_id,
      title: data.title,
    };
    data.responses.map((d) => {
      const section = {
        instructionId: d.instructionId,
        instruction: d.instruction,
        title: d.title,
        questions: d.response,
      };
      section.questions.map((question) => {
        if (question.questionType_id === 1) {
          question.value = 'Multiple Choice';
        } else if (question.questionType_id === 2) {
          question.value = 'TrueFalse';
        } else if (question.questionType_id === 3) {
          question.value = 'Fill in the gaps';
        } else if (question.questionType_id === 4) {
          question.value = 'Question';
        } else {
          question.value = 'Code';
        }
        question.options = options;
        return question;
      });
      sections.push(section);
      return d;
    });
    //--- update UI
    this.setState({
      loading: false,
      quizData: quizData,
      sections: sections,
    });
  };

  handleUpdate = (event) => {
    event.preventDefault();
    console.log(event);
  };

  onSectionChange = (event, sectionIndex) => {
    const { value } = event.target;
    const section = this.state.sections[sectionIndex];
    section.title = value;
    section.titleError = '';
    const updateSections = [...this.state.sections];
    updateSections.map((s) =>
      s.instructionId === section.instructionId ? (s = section) : s
    );
    this.setState({ sections: updateSections });
  };

  onQuestionChange = (question, event) => {
    const { sectionIndex, questionIndex } = question;
    const section = this.state.sections[sectionIndex];
    const currentQuestion = section.questions[questionIndex];
    if (event.target) {
      const { name, value, checked } = event.target;
      if (currentQuestion.questionType_id === 1) {
        if (name === 'multiQuestion') {
          currentQuestion.question.multiQuestion = value;
          currentQuestion.question.multiQuestionError = '';
        } else if (name === 'checkOne') {
          currentQuestion.question.checkOne = checked;
        } else if (name === 'multiAnswerOne') {
          currentQuestion.question.multiAnswerOne = value;
          currentQuestion.question.multiAnswerOneError = '';
        } else if (name === 'checkTwo') {
          currentQuestion.question.checkTwo = checked;
        } else if (name === 'multiAnswerTwo') {
          currentQuestion.question.multiAnswerTwo = value;
          currentQuestion.question.multiAnswerTwoError = '';
        } else if (name === 'checkThree') {
          currentQuestion.question.checkThree = checked;
        } else if (name === 'multiAnswerThree') {
          currentQuestion.question.multiAnswerThree = value;
          currentQuestion.question.multiAnswerThreeError = '';
        } else if (name === 'checkFour') {
          currentQuestion.question.checkFour = checked;
        } else if (name === 'multiAnswerFour') {
          currentQuestion.question.multiAnswerFour = value;
          currentQuestion.question.multiAnswerFourError = '';
        } else {
          currentQuestion.question.point = Number(value);
          currentQuestion.question.pointError = '';
        }
      } else if (currentQuestion.questionType_id === 2) {
        if (name === 'trueFalse') {
          currentQuestion.answer = value.toUpperCase();
          currentQuestion.trueFalseValueError = '';
        } else if (name === 'trueFalseQuestion') {
          currentQuestion.question = value;
          currentQuestion.trueFalseQuestionError = '';
        } else {
          currentQuestion.point = Number(value);
          currentQuestion.trueFalsePointError = '';
        }
      } else if (currentQuestion.questionType_id === 3) {
        if (name === 'fillTheGaps') {
          currentQuestion.question.fillTheGapsError = '';
          currentQuestion.question.fillTheGaps = value;
        } else {
          currentQuestion.question.pointError = '';
          currentQuestion.question.point = Number(value);
        }
      } else if (currentQuestion.questionType_id === 4) {
        if (name === 'question') {
          currentQuestion.question.questionError = '';
          currentQuestion.question.question = value;
        } else if (name === 'answer') {
          currentQuestion.question.answerError = '';
          currentQuestion.question.answer = value;
        } else {
          currentQuestion.question.pointError = '';
          currentQuestion.question.point = Number(value);
        }
      } else {
        currentQuestion.point = value;
        currentQuestion.pointError = '';
      }
    } else {
      if (currentQuestion.question.questionTypeId === 5) {
        //option code
        currentQuestion.question.coding = event;
        currentQuestion.question.codingError = '';
      }
    }
  };

  handleOptionsChange = (sectionIndex, questionIndex, event) => {
    //--- find current section
    let currentSection = this.state.sections[sectionIndex];
    //--- find current question
    let currentQuestion = this.state.sections[sectionIndex].questions[
      questionIndex
    ];
    //--- update option change
    console.log('op-ch: ', currentQuestion);
    console.log('event: ', event);
    currentQuestion = event;
    //--- copy sections that have
    const updateSections = [...this.state.sections];
    updateSections.map((section) => {
      if (section.instructionId === currentSection.instructionId) {
        section.questions.map((question) => {
          if (question.questionId === currentQuestion.questionId) {
            question = currentQuestion;
          }
          return question;
        });
      }
      return section;
    });
    //--- update UI
    this.setState({
      sections: updateSections,
    });
  };

  renderComponent = (sectionIndex, questionIndex, questionTypeId) => {
    let displayComponent = null;
    let currentQuestion = this.state.sections[sectionIndex].questions[
      questionIndex
    ];
    const ShowMultipleChoiceComponent = (
      <Multi
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowTrueFalseComponent = (
      <TrueFalse
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowFillInTheGapsComponent = (
      <FilltheGraps
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowQuestionAndAnswerComponent = (
      <QuestionAndAnswer
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowCodingComponent = (
      <Coding
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    switch (questionTypeId) {
      case 1:
        displayComponent = ShowMultipleChoiceComponent;
        break;
      case 2:
        displayComponent = ShowTrueFalseComponent;
        break;
      case 3:
        displayComponent = ShowFillInTheGapsComponent;
        break;
      case 4:
        displayComponent = ShowQuestionAndAnswerComponent;
        break;
      case 5:
        displayComponent = ShowCodingComponent;
        break;
      default:
        displayComponent = null;
        break;
    }
    //--- show selected component
    return displayComponent;
  };
}

const style = {
  navbar: {
    backgroundColor: '#41AAA8',
    marginBottom: '15px',
  },
  navContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    justifyContent: 'flex-end',
    height: '100px',
    marginRight: 'auto',
  },
  navTitle: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: '19px',
  },
  navDesc: {
    color: 'white',
    fontSize: '16px',
  },
  btnSave: {
    backgroundColor: 'white',
    height: '38px',
    border: 0,
  },
  spanText: {
    fontWeight: 'bold',
    marginLeft: '5px',
    color: 'black',
    fontSize: '15px',
  },
};

export default UpdateQuiz;
