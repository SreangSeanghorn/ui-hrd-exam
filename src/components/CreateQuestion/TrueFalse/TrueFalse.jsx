import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';

function TrueFalse(props) {
  //--- props
  const {
    onQuestionChange,
    sectionIndex,
    questionIndex,
    question: {
      answer,
      question,
      qPoint,
      trueFalseValueError,
      trueFalseQuestionError,
      trueFalsePointError,
    },
  } = props;
  const initQuestion = {
    sectionIndex,
    questionIndex,
  };
  //--- state
  const [trueFalseValue, setTrueFalseValue] = useState(answer);
  const [trueFalseQuestion, setTrueFalseQuestion] = useState(question);
  const [trueFalseValueErrorState, setTrueFalseValueErrorState] = useState('');
  const [point, setPoint] = useState(qPoint);
  const [pointStateError, setPointStateError] = useState('');

  const onTrueFalseValueChange = (event) => {
    let inputValue = event.target.value.toUpperCase();
    if (!inputValue.match(/T|F/)) {
      setTrueFalseValueErrorState(
        'Please input Letter T for True or F for False'
      );
      setTrueFalseValue('');
      onQuestionChange(initQuestion, event);
    } else {
      setTrueFalseValueErrorState('');
      setTrueFalseValue(inputValue);
      onQuestionChange(initQuestion, event);
    }
  };

  const onTrueFalsePointChange = (event) => {
    let inputValue = event.target.value;
    if (inputValue === '') {
      setPointStateError('Point cannot be empty!');
      setPoint(inputValue);
    } else if (!inputValue.match(/^[0-9]+$/)) {
      setPointStateError('Invalid number!');
      setPoint(inputValue);
    } else {
      setPointStateError('');
      setPoint(inputValue);
    }
    onQuestionChange(initQuestion, event);
  };

  return (
    <div className='mt-4'>
      <TextField
        className='px-2 MuiInputBase-input-text'
        name='trueFalse'
        value={trueFalseValue}
        onChange={onTrueFalseValueChange}
        multiline
        style={styles.textField}
        inputProps={{ maxLength: 1, style: styles.align }}
        placeholder='True/False'
      />
      {trueFalseValueErrorState || trueFalseValueError ? (
        <h6 className='text-danger'>
          {trueFalseValueErrorState || trueFalseValueError}
        </h6>
      ) : null}
      <TextField
        className='widthtField'
        name='trueFalseQuestion'
        value={trueFalseQuestion}
        onChange={(event) => {
          setTrueFalseQuestion(event.target.value);
          onQuestionChange(initQuestion, event);
        }}
        multiline
        placeholder='fill the question...'
      />
      {trueFalseQuestionError ? (
        <h6 className='text-danger'>{trueFalseQuestionError}</h6>
      ) : null}
      <TextField
        type='number'
        className='px-2 MuiInputBase-input-text'
        name='point'
        value={point}
        onChange={onTrueFalsePointChange}
        inputProps={{ style: styles.align }}
        style={styles.textField}
        multiline
        placeholder='points'
      />
      {trueFalsePointError || pointStateError ? (
        <h6 className='text-danger'>
          {trueFalsePointError || pointStateError}
        </h6>
      ) : null}
    </div>
  );
}
const styles = {
  textField: {
    width: '113px',
  },
  align: {
    textAlign: 'center',
  },
};
export default TrueFalse;
