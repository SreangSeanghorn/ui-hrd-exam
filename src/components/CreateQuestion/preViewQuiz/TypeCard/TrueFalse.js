import React, { Component } from "react";
import { Card } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class TrueFalse extends Component {
  render() {
    return (
      <div>
        <br />
        <Card style={{ width: "100%" }}>
          <Card.Body className="textAnswer">
            <TextField
              // disabled
              className="MuiInputBase-input-text"
              value={this.props.anwser}
            />
            {this.props.index + 1 + ". "}
            {this.props.question}
          </Card.Body>
        </Card>
      </div>
    );
  }
}
