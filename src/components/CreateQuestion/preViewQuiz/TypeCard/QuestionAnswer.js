import React, { Component } from "react";
import { Card } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class QuestionAnswer extends Component {
  render() {
    return (
      <div>
        <br />
        <Card style={{ width: "100%" }}>
          <Card.Body className="textAnswer">{(this.props.index +1) + ". " + this.props.question}</Card.Body>
          <Card.Body>
            <TextField
              disabled
              id="standard-full-width"
              multiline
              fullWidth
              placeholder="Answer"
              name="answer1"
              className="px-2 textAnswer"
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
