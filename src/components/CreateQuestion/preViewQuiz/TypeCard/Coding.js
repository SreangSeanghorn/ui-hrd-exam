import React, { useState } from "react";
import { Card } from "react-bootstrap";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-java";
import "prismjs/components/prism-javascript";
//--- prism-themes
import "prism-themes/themes/prism-atom-dark.css";
import parse from 'html-react-parser';

export default function Coding({ question, questionIndex, index }) {
  const [javascriptCode, setJavaScriptCode] = useState("");
  const content = question;
  var dataSplit = content.split(/\n/);
  return (
    <div>
      <form>
        <br />
        <Card style={{ width: "100%" }}>
          <Card.Body className="textAnswer">
            {index + 1}
            <span>. </span>
            {dataSplit.map((data) => parse(data, { trim: true }))}
          </Card.Body>
        </Card>
        <br />
        <Card>
          <Editor
            disabled
            value={javascriptCode}
            onValueChange={(javascriptCode) =>
              setJavaScriptCode(javascriptCode)
            }
            highlight={(javascriptCode) =>
              highlight(javascriptCode, languages.javascript)
            }
            padding={10}
            tabSize={4}
            preClassName="#fff"
            style={{
              width: "100%",
              minHeight: "250px",
              fontFamily: '"Consolas", monospace',
              fontSize: 18,
            }}
          />
        </Card>
      </form>
    </div>
  );
}
