import React, { Component } from "react";
import { Card } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class FillinTheGaps extends Component {
  render() {
    return (
      <div>
        <br />
        <Card style={{ width: "100%" }}>
          <Card.Body className="textAnswer">
            {(this.props.index + 1)+". "}
            {this.props.question}
          </Card.Body>
          <Card.Body>
            <TextField
              id="standard-basic"
              fullWidth
              placeholder="Answer "
              className="text-input-fill"
              disabled
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
