import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
// import "../../../../assets/css/TabSection.css";
export default class MultiChoise extends Component {
  render() {
    return (
      <div>
        <Card style={{ width: "100%", marginTop: "14px"}} >
          <Card.Body className="bodyAnswer textAnswer">
            {(this.props.index +1) + ". "+this.props.question}
          </Card.Body>

          <Card.Body>
            <Form.Check
            // disabled
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer "
              checked={this.props.isCorrect1 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-1`}
              label={this.props.anw1}
              custom
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              checked={this.props.isCorrect2 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-2`}
              label={this.props.anw2}
              custom
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              checked={this.props.isCorrect3 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-3`}
              label={this.props.anw3}
              custom
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              checked={this.props.isCorrect4 === "true" ? true : false}
              readOnly
              // id={`custom-inline-checkbox-4`}
              label={this.props.anw4}
              custom
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
