import React, { useState, useEffect } from 'react';
import { Tab, Form, Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { getViewQuiz } from '../../../../redux/actions/classworkAction';
import { bindActionCreators } from 'redux';
import logoImage from '../../../../assets/img/hrd_exam_logo.png';
import { Button, Col, Nav, Row } from 'react-bootstrap';
import '../../../../assets/css/TabSection.css';
import { withRouter, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { CustomLoading } from '../../../../components/utils/Utils';
//import { highlight, languages } from "prismjs/components/prism-core";
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-javascript';
//--- prism-themes
import 'prism-themes/themes/prism-atom-dark.css';

//import type Card
import TrueFalse from '../TypeCard/TrueFalse';
import MultiChoise from '../TypeCard/MultiChoise';
import FillinTheGaps from '../TypeCard/FillinTheGaps';
import QuestionAnswer from '../TypeCard/QuestionAnswer';
import Coding from '../TypeCard/Coding';



const queryString = require('query-string');
function Tabars(props) {
  const [questions, setQuestion] = useState([]);
  const {
    //handeChangeAnswer,
    getViewQuiz,
    viewQuizData,
  } = props;
  const [previewData, setpreviewData] = useState()
  let getID = queryString.parse(props.location.search);

  useEffect(() => {
  //  console.log("dataPreview: ",localStorage.getItem('dataPreview')); 
   let data = JSON.parse(localStorage.getItem('dataPreview'))
   setpreviewData(data.responses)
   console.log("Hello Data",data.responses);
    getViewQuiz(getID.id);
  }, [getID.id, getViewQuiz]);

  useEffect(() => {
    if (viewQuizData) {
      setQuestion(viewQuizData);
    }
  }, [viewQuizData]);

console.log("previewData:", previewData);
if(previewData===undefined || previewData.length < 0){
  return (
    <div
      style={{
        textAlign: 'center',
      }}>
      <h3>No Data</h3>
      {/* <Button onClick={() => window.location.href="/admin/classwork"}>Go Quiz</Button> */}
    </div>
  );
}

  if (previewData && previewData.length > 0) {
    return (
      <>
        <div>
          <Tab.Container
            defaultActiveKey={
              previewData.find(
                (question) => question.instruction === 'Section I'
              ).instructionId
            }
            id='tab-container'
            className='m-0'>
            <Row className=' m-0 nav_content-fixed' id='item'>
              <Col lg={2} className='fixed_nav' id='exam-logo'>
                <a href='#toHome' >
                  <img
                    src={logoImage}
                    className='d-inline-block py-3 '
                    style={{ width: '137px' }}
                    alt='hrd-exam-logo'
                  />
                </a>
              </Col>
              <Col lg={8} className='nav_content'>
                <Row style={{ marginLeft: '12%' }}>
                  <Nav variant='pills'>
                    <Col lg={12} className='pt-3'>
                      <Form inline>
                        {previewData
                          .sort((a, b) =>
                            a.instruction.localeCompare(b.instruction)
                          )
                          .map((question) => {
                            return (
                              <Nav.Item key={question.instructionId}>
                                <Nav.Link
                                  eventKey={question.instructionId}
                                  className='nav-link text-nav  ml-3'>
                                  {question.instruction}
                                </Nav.Link>
                              </Nav.Item>
                            );
                          })}
                      </Form>
                    </Col>
                  </Nav>
                </Row>
                {/* <Row>
                  <Col lg={12}></Col>
                </Row> */}
              </Col>
              <Col lg={2} className='fixed_nav py-3'>
                <Form inline>
                  <Link
                    target='_blank'
                    to={`/admin/classwork/reactpdf?id=${getID.id}`}>
                    {/* <Button className='btn-page-submit' variant='light'>
                      <strong>Print</strong>
                    </Button> */}
                  </Link>
                </Form>
              </Col>
            </Row>

            <Row className=' m-0 body-content'>
              <Col lg={12}>
                <Row>
                  <Col lg={2}></Col>
                  <Col lg={8}>
                    <br />
                    <br />
                    <Tab.Content>
                      {previewData
                        .sort((a, b) =>
                          a.instruction.localeCompare(b.instruction)
                        )
                        .map((question) => {
                          return (
                            <Tab.Pane
                              eventKey={question.instructionId}
                              key={question.instructionId}>
                              <Card>
                                <Card.Body className='font-weight-bold textQuestionTitle'>
                                  {question.instruction}
                                </Card.Body>
                                <Card.Body className='pl-5  textQuestion'>
                                  {question.title}
                                </Card.Body>
                              </Card>
                              {question.response.map((value, index) => {
                                switch (value.questionType_id) {
                                  case 2:
                                    return (
                                      <TrueFalse
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                      />
                                    );
                                  case 1:
                                    return (
                                      <MultiChoise
                                        key={value.questionId}
                                        question={value.question}
                                        index={index}
                                        anw1={value.answer.option1.answer}
                                        isCorrect1={
                                          value.answer.option1.isCorrect
                                        }
                                        anw2={value.answer.option2.answer}
                                        isCorrect2={
                                          value.answer.option2.isCorrect
                                        }
                                        anw3={value.answer.option3.answer}
                                        isCorrect3={
                                          value.answer.option3.isCorrect
                                        }
                                        anw4={value.answer.option4.answer}
                                        isCorrect4={
                                          value.answer.option4.isCorrect
                                        }
                                      />
                                    );
                                  case 3:
                                    return (
                                      <FillinTheGaps
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                      />
                                    );
                                  case 4:
                                    return (
                                      <QuestionAnswer
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                      />
                                    );
                                  case 5:
                                    return (
                                      <Coding
                                        key={value.questionId}
                                        question={value.question}
                                        anwser={value.answer}
                                        index={index}
                                      />
                                    );
                                  default:
                                    return null;
                                }
                              })}
                            </Tab.Pane>
                          );
                        })}
                    </Tab.Content>
                  </Col>
                  <Col lg={2}></Col>
                </Row>
              </Col>
            </Row>
          </Tab.Container>
        </div>
      </>
    );
  }

  return (
    <div style={style.content}>
      <CustomLoading />
    </div>
  );
}

const style = {
  border_text: {
    borderTop: '0',
    borderButtom: '0',
    borderLeft: '0',
    borderRight: '0',
    borderRadius: '0',
    padding: '0',
  },
  content: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    WebkitTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
};
const mapStateToProps = (state) => {
  return {
    viewQuizData: state.viewQuizData.data,
    laoding: state.viewQuizData.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      getViewQuiz,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Tabars));
// export default Tabars
