import React, { Component, Fragment } from 'react';
import {
  Navbar,
  Nav,
  Button,
  Form,
  Row,
  Col,
  Card,
  Container,
} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import Select from 'react-select';
//style
import './CreateQuiz.css';
//Icon
import logoImage from '../../assets/img/hrd_exam_logo.png';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import QueueOutlinedIcon from '@material-ui/icons/QueueOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import SpellcheckOutlinedIcon from '@material-ui/icons/SpellcheckOutlined';
import NotesOutlinedIcon from '@material-ui/icons/NotesOutlined';
import BallotOutlinedIcon from '@material-ui/icons/BallotOutlined';
import PowerInputIcon from '@material-ui/icons/PowerInput';
import CodeIcon from '@material-ui/icons/Code';
import { IconButton } from '@material-ui/core';
import Section from './Section/Section';
import TrueFalse from './TrueFalse/TrueFalse';
import QuestionAndAnswer from './QuestionAnswer/QuestionAnswer';
import Multi from './Multi/Multi';
import { arabicToRoman } from '../utils/Utils';
import { options, IconOption, CustomSelectValue } from './QuestionOptions';
import Axios from 'axios';
import FilltheGraps from './FilltheGraps/FilltheGraps';
import Coding from './Coding/Coding';
import { BASE_URL } from '../../config/API';
import { CustomLoading } from '../utils/Utils';
import Swal from 'sweetalert2';

const initialQuestion = {
  questionId: 'question-1',
  question: {
    questionTypeId: 2,
    value: 'TrueFalse',
    label: 'True/False',
    icon: <SpellcheckOutlinedIcon />,
    trueFalseValue: '',
    trueFalseQuestion: '',
    point: 0,
  },
  options: options,
};

const headers = {
  headers: {
    Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
  },
};

class CreateQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quizId: this.props.location.state
        ? this.props.location.state.quizData.quizId
        : null,
      sections: [
        {
          instructionId: 'section-1',
          instruction: 'Section I',
          title: '',
          titleError: '',
          instructionError: '',
          questions: [initialQuestion],
        },
      ],
      loading: false,
    };
  }

  render() {
    if (this.state.quizId === null) {
      return <Redirect to='/' />;
    } else if (this.state.loading) {
      return <CustomLoading />;
    } else {
      const { sections } = this.state;
      const { quizData } = this.props.location.state;
      console.log(sections);
      return (
        <div className='container-fluid px-0 mb-5'>
          <Navbar expand='lg' style={style.navbar}>
            <Navbar.Brand href='#home' as={Link} to='/'>
              <img src={logoImage} className='w-50' alt='hrd-exam-logo' />
            </Navbar.Brand>
            <Nav style={style.navContainer}>
              <h4 style={style.navTitle}>
                Online {quizData ? quizData.title : null}
              </h4>
              <p style={style.navDesc}>
                Subject: {quizData ? quizData.subject : null} &nbsp;&nbsp; |
                &nbsp;&nbsp; Duration: {quizData ? quizData.duration : null}mn
              </p>
            </Nav>
            <Nav
              style={{
                fontSize: '16px',
                color: 'white',
                fontWeight: 'bold',
                paddingRight: '15px',
              }}>
              <p
                as={Link}
                to={`/create-quiz/previewData`}
                target='_blank'
                onClick={this.handlePreview}
                style={{ cursor: 'pointer', marginBottom: '-20px' }}>
                Preview
              </p>{' '}
              &nbsp;&nbsp;&nbsp;&nbsp; |
            </Nav>
            <Form inline className='pl-3'>
              <Button
                type='button'
                style={style.btnSave}
                className='btn-info'
                disabled={false}
                onClick={this.handleSave}>
                <DoneAllIcon className='fillIcon' />
                <span style={style.spanText}>Save</span>
              </Button>
            </Form>
          </Navbar>
          <Form className='mt-2'>
            {sections
              .sort((a, b) => a.instruction.localeCompare(b.instruction))
              .map((section, sectionIndex) => (
                <Fragment key={section.instructionId}>
                  <Section
                    sectionIndex={sectionIndex}
                    section={this.state.sections[sectionIndex]}
                    handleChange={(event) =>
                      this.onSectionChange(event, sectionIndex)
                    }
                  />
                  {section.questions.map((question, questionIndex) => (
                    <Fragment key={question.questionId}>
                      <Card className='my-2 mb-3 mt-3 styleCard'>
                        <Card.Body className='styleBody'>
                          <Container>
                            <Row>
                              <Col
                                xs={3}
                                style={{ textAlign: 'left' }}
                                className='mt-3 ml-auto'>
                                <Select
                                  value={question.options.filter(
                                    (q) => q.value === question.question.value
                                  )}
                                  onChange={(event) =>
                                    this.handleOptionsChange(
                                      sectionIndex,
                                      questionIndex,
                                      event
                                    )
                                  }
                                  options={question.options}
                                  components={{
                                    Option: IconOption,
                                    SingleValue: CustomSelectValue,
                                  }}
                                />
                              </Col>
                              <Col xs={12}>
                                {this.renderComponent(
                                  sectionIndex,
                                  questionIndex,
                                  question.question.questionTypeId
                                )}
                              </Col>
                            </Row>
                          </Container>
                        </Card.Body>
                        <Card.Footer className='cardFooter styleFooter'>
                          <IconButton
                            className='tooltip-icon'
                            onClick={(event) =>
                              this.handleAddQuestion(
                                sectionIndex,
                                questionIndex,
                                event
                              )
                            }>
                            <AddCircleOutlineIcon className='fillIcon' />
                            <span className='tooltiptext'>Add question</span>
                          </IconButton>
                          <IconButton
                            className='tooltip-icon'
                            onClick={(event) =>
                              this.handleAddSection(event, sectionIndex)
                            }>
                            <QueueOutlinedIcon className='fillIcon' />
                            <span className='tooltiptext'>Add title</span>
                          </IconButton>
                          <IconButton
                            className='tooltip-icon'
                            disabled={
                              sections.length === 1 &&
                              section.questions.length === 1
                                ? true
                                : false
                            }
                            onClick={(event) =>
                              this.handleDelete(
                                event,
                                sectionIndex,
                                question.questionId
                              )
                            }>
                            <DeleteOutlineOutlinedIcon
                              className={
                                sections.length === 1 &&
                                section.questions.length === 1
                                  ? ''
                                  : 'fillIcon'
                              }
                            />
                            <span className='tooltiptext'>Delete question</span>
                          </IconButton>
                        </Card.Footer>
                      </Card>
                    </Fragment>
                  ))}
                </Fragment>
              ))}
          </Form>
        </div>
      );
    }
  }

  onSectionChange = (event, sectionIndex) => {
    const { value } = event.target;
    const section = this.state.sections[sectionIndex];
    section.title = value;
    section.titleError = '';
    const updateSections = [...this.state.sections];
    updateSections.map((s) =>
      s.instructionId === section.instructionId ? (s = section) : s
    );
    this.setState({ sections: updateSections });
  };

  onQuestionChange = (question, event) => {
    const { sectionIndex, questionIndex } = question;
    const section = this.state.sections[sectionIndex];
    const currentQuestion = section.questions[questionIndex];
    if (event.target) {
      const { name, value, checked } = event.target;
      if (currentQuestion.question.questionTypeId === 1) {
        if (name === 'multiQuestion') {
          currentQuestion.question.multiQuestion = value;
          currentQuestion.question.multiQuestionError = '';
        } else if (name === 'checkOne') {
          currentQuestion.question.checkOne = checked;
        } else if (name === 'multiAnswerOne') {
          currentQuestion.question.multiAnswerOne = value;
          currentQuestion.question.multiAnswerOneError = '';
        } else if (name === 'checkTwo') {
          currentQuestion.question.checkTwo = checked;
        } else if (name === 'multiAnswerTwo') {
          currentQuestion.question.multiAnswerTwo = value;
          currentQuestion.question.multiAnswerTwoError = '';
        } else if (name === 'checkThree') {
          currentQuestion.question.checkThree = checked;
        } else if (name === 'multiAnswerThree') {
          currentQuestion.question.multiAnswerThree = value;
          currentQuestion.question.multiAnswerThreeError = '';
        } else if (name === 'checkFour') {
          currentQuestion.question.checkFour = checked;
        } else if (name === 'multiAnswerFour') {
          currentQuestion.question.multiAnswerFour = value;
          currentQuestion.question.multiAnswerFourError = '';
        } else {
          currentQuestion.question.point = Number(value);
          currentQuestion.question.pointError = '';
        }
      } else if (currentQuestion.question.questionTypeId === 2) {
        if (name === 'trueFalse') {
          currentQuestion.question.trueFalseValue = value.toUpperCase();
          currentQuestion.question.trueFalseValueError = '';
        } else if (name === 'trueFalseQuestion') {
          currentQuestion.question.trueFalseQuestion = value;
          currentQuestion.question.trueFalseQuestionError = '';
        } else {
          currentQuestion.question.point = Number(value);
          currentQuestion.question.trueFalsePointError = '';
        }
      } else if (currentQuestion.question.questionTypeId === 3) {
        if (name === 'fillTheGaps') {
          currentQuestion.question.fillTheGapsError = '';
          currentQuestion.question.fillTheGaps = value;
        } else {
          currentQuestion.question.pointError = '';
          currentQuestion.question.point = Number(value);
        }
      } else if (currentQuestion.question.questionTypeId === 4) {
        if (name === 'question') {
          currentQuestion.question.questionError = '';
          currentQuestion.question.question = value;
        } else if (name === 'answer') {
          currentQuestion.question.answerError = '';
          currentQuestion.question.answer = value;
        } else {
          currentQuestion.question.pointError = '';
          currentQuestion.question.point = Number(value);
        }
      } else {
        currentQuestion.question.point = value;
        currentQuestion.question.pointError = '';
      }
    } else {
      if (currentQuestion.question.questionTypeId === 5) {
        //option code
        currentQuestion.question.coding = event;
        currentQuestion.question.codingError = '';
      }
    }
  };

  handleOptionsChange = (sectionIndex, questionIndex, event) => {
    //--- find current section
    let currentSection = this.state.sections[sectionIndex];
    //--- find current question
    let currentQuestion = this.state.sections[sectionIndex].questions[
      questionIndex
    ];
    //--- update option change
    currentQuestion.question = event;
    //--- copy sections that have
    const updateSections = [...this.state.sections];
    updateSections.map((section) => {
      if (section.instructionId === currentSection.instructionId) {
        section.questions.map((question) => {
          if (question.questionId === currentQuestion.questionId) {
            question = currentQuestion;
          }
          return question;
        });
      }
      return section;
    });
    //--- update UI
    this.setState({
      sections: updateSections,
    });
  };

  renderComponent = (sectionIndex, questionIndex, questionTypeId) => {
    let displayComponent = null;
    let currentQuestion = this.state.sections[sectionIndex].questions[
      questionIndex
    ];
    const ShowMultipleChoiceComponent = (
      <Multi
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowTrueFalseComponent = (
      <TrueFalse
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowFillInTheGapsComponent = (
      <FilltheGraps
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowQuestionAndAnswerComponent = (
      <QuestionAndAnswer
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    const ShowCodingComponent = (
      <Coding
        sectionIndex={sectionIndex}
        questionIndex={questionIndex}
        question={currentQuestion.question}
        onQuestionChange={this.onQuestionChange}
      />
    );
    switch (questionTypeId) {
      case 1:
        displayComponent = ShowMultipleChoiceComponent;
        break;
      case 2:
        displayComponent = ShowTrueFalseComponent;
        break;
      case 3:
        displayComponent = ShowFillInTheGapsComponent;
        break;
      case 4:
        displayComponent = ShowQuestionAndAnswerComponent;
        break;
      case 5:
        displayComponent = ShowCodingComponent;
        break;
      default:
        displayComponent = null;
        break;
    }
    //--- show selected component
    return displayComponent;
  };

  validateSection = (currentSection) => {
    const title = currentSection.title;
    let titleError = null;
    if (!title) {
      titleError = 'Instruction cannot be empty!';
    }
    if (titleError) {
      currentSection.titleError = titleError;
    }
    if (titleError) {
      return false;
    }
    return true;
  };

  newSection = () => {
    let generateID = Math.random().toString(36).substring(7);
    const initialNewQuestion = {
      questionId: generateID,
      question: {
        questionTypeId: 2,
        value: 'TrueFalse',
        label: 'True/False',
        icon: <SpellcheckOutlinedIcon />,
        trueFalseValue: '',
        trueFalseQuestion: '',
        trueFalseValueError: '',
        trueFalseQuestionError: '',
      },
      options: options,
    };
    const newSection = {
      instructionId: generateID,
      title: '',
      instruction: '',
      titleError: '',
      instructionError: '',
      questions: [initialNewQuestion],
    };
    return newSection;
  };

  validateQuestion = (currentQuestion) => {
    if (currentQuestion.question.questionTypeId === 1) {
      let multiQuestion = '';
      multiQuestion = currentQuestion.question.multiQuestion;
      const multiAnswerOne = currentQuestion.question.multiAnswerOne;
      const multiAnswerTwo = currentQuestion.question.multiAnswerTwo;
      const multiAnswerThree = currentQuestion.question.multiAnswerThree;
      const multiAnswerFour = currentQuestion.question.multiAnswerFour;
      const point = currentQuestion.question.point;
      let multiQuestionError = '';
      let multiAnswerOneError = '';
      let multiAnswerTwoError = '';
      let multiAnswerThreeError = '';
      let multiAnswerFourError = '';
      let pointError = '';
      if (!multiQuestion) {
        multiQuestionError = 'Question cannot be empty!';
      }
      if (multiQuestion.trim() === '') {
        this.multiQuestion.trim();
        multiQuestionError = 'Question cannot be empty!';
      }
      if (!multiAnswerOne) {
        multiAnswerOneError = 'Answer cannot be empty!';
      }
      if (!multiAnswerTwo) {
        multiAnswerTwoError = 'Answer cannot be empty!';
      }
      if (!multiAnswerThree) {
        multiAnswerThreeError = 'Answer cannot be empty!';
      }
      if (!multiAnswerFour) {
        multiAnswerFourError = 'Answer cannot be empty!';
      }
      if (point === 0) {
        pointError = 'Point should not zero';
      } else if (!point) {
        pointError = 'Point cannot be empty!';
      }
      if (multiQuestionError) {
        currentQuestion.question.multiQuestionError = multiQuestionError;
      }
      if (multiAnswerOneError) {
        currentQuestion.question.multiAnswerOneError = multiAnswerOneError;
      }
      if (multiAnswerTwoError) {
        currentQuestion.question.multiAnswerTwoError = multiAnswerTwoError;
      }
      if (multiAnswerThreeError) {
        currentQuestion.question.multiAnswerThreeError = multiAnswerThreeError;
      }
      if (multiAnswerFourError) {
        currentQuestion.question.multiAnswerFourError = multiAnswerFourError;
      }
      if (pointError) {
        currentQuestion.question.pointError = pointError;
      }
      if (
        multiQuestionError ||
        multiAnswerOneError ||
        multiAnswerTwoError ||
        multiAnswerOneError ||
        multiAnswerTwoError ||
        pointError
      ) {
        return false;
      }
      return true;
    } else if (currentQuestion.question.questionTypeId === 2) {
      const trueFalseValue = currentQuestion.question.trueFalseValue;
      const trueFalseQuestion = currentQuestion.question.trueFalseQuestion;
      const point = currentQuestion.question.point;
      let trueFalseValueError = '';
      let trueFalseQuestionError = '';
      let trueFalsePointError = '';
      if (!trueFalseValue) {
        trueFalseValueError = 'Value cannot be empty!';
      }
      if (!trueFalseQuestion) {
        trueFalseQuestionError = 'Question cannot be empty!';
      }
      if (point === 0) {
        trueFalsePointError = 'Point should not zero';
      } else if (!point) {
        trueFalsePointError = 'Point cannot be empty!';
      }
      if (trueFalseValueError) {
        currentQuestion.question.trueFalseValueError = trueFalseValueError;
      }
      if (trueFalseQuestionError) {
        currentQuestion.question.trueFalseQuestionError = trueFalseQuestionError;
      }
      if (trueFalsePointError) {
        currentQuestion.question.trueFalsePointError = trueFalsePointError;
      }
      if (
        trueFalseValueError ||
        trueFalseQuestionError ||
        trueFalsePointError
      ) {
        return false;
      }
      return true;
    } else if (currentQuestion.question.questionTypeId === 3) {
      const fillTheGaps = currentQuestion.question.fillTheGaps;
      const point = currentQuestion.question.point;
      let fillTheGapsError = '';
      let pointError = '';
      if (!fillTheGaps) {
        fillTheGapsError = 'FillTheGaps cannot be empty!';
      }
      if (point === 0) {
        pointError = 'Point should not zero';
      } else if (!point) {
        pointError = 'Point cannot be empty!';
      }
      if (fillTheGapsError) {
        currentQuestion.question.fillTheGapsError = fillTheGapsError;
      }
      if (pointError) {
        currentQuestion.question.pointError = pointError;
      }
      if (fillTheGapsError || pointError) {
        return false;
      }
      return true;
    } else if (currentQuestion.question.questionTypeId === 4) {
      const question = currentQuestion.question.question;
      const point = currentQuestion.question.point;
      let questionError = '';
      let pointError = '';
      if (!question) {
        questionError = 'Question cannot be empty!';
      }
      if (point === 0) {
        pointError = 'Point should not zero';
      } else if (!point) {
        pointError = 'Point cannot be empty!';
      }
      if (questionError) {
        currentQuestion.question.questionError = questionError;
      }
      if (pointError) {
        currentQuestion.question.pointError = pointError;
      }
      if (questionError || pointError) {
        return false;
      }
      return true;
    } else {
      const coding = currentQuestion.question.coding;
      const point = currentQuestion.question.point;
      let codingError = '';
      let pointError = '';
      if (!coding) {
        codingError = 'Coding cannot be empty!';
      }
      if (point === 0) {
        pointError = 'Point should not zero';
      } else if (!point) {
        pointError = 'Point cannot be empty!';
      }
      if (codingError) {
        currentQuestion.question.codingError = codingError;
      }
      if (pointError) {
        currentQuestion.question.pointError = pointError;
      }
      if (codingError || pointError) {
        return false;
      }
      return true;
    }
  };

  newTrueFalseQuestion = () => {
    let generateID = Math.random().toString(36).substring(7);
    const newTrueFalseQuestion = {
      questionId: generateID,
      question: {
        questionTypeId: 2,
        value: 'TrueFalse',
        label: 'True/False',
        icon: <SpellcheckOutlinedIcon />,
        trueFalseValue: '',
        trueFalseQuestion: '',
        point: 0,
      },
      options: options,
    };
    return newTrueFalseQuestion;
  };

  newQuestion = () => {
    let generateID = Math.random().toString(36).substring(7);
    const newQuestion = {
      questionId: generateID,
      question: {
        questionTypeId: 4,
        value: 'Question',
        label: 'Question',
        icon: <NotesOutlinedIcon />,
        question: '',
        point: 0,
      },
      options: options,
    };
    return newQuestion;
  };

  newMultiQuestion = () => {
    let generateID = Math.random().toString(36).substring(7);
    const newMultiQuestion = {
      questionId: generateID,
      question: {
        questionTypeId: 1,
        value: 'Multiple Choice',
        label: 'Multiple Choice',
        icon: <BallotOutlinedIcon />,
        multiQuestion: '',
        multiAnswerOne: '',
        checkOne: '',
        multiAnswerTwo: '',
        checkTwo: '',
        multiAnswerThree: '',
        checkThree: '',
        multiAnswerFour: '',
        checkFour: '',
        point: 0,
      },
      options: options,
    };
    return newMultiQuestion;
  };

  newFillTheGapsQuestion = () => {
    let generateID = Math.random().toString(36).substring(7);
    const newFillTheGaps = {
      questionId: generateID,
      question: {
        questionTypeId: 3,
        value: 'Fill in the gaps',
        label: 'Fill in the gaps',
        icon: <PowerInputIcon />,
        fillTheGaps: '',
        point: 0,
      },
      options: options,
    };
    return newFillTheGaps;
  };

  newCodeQuestion = () => {
    let generateID = Math.random().toString(36).substring(7);
    const newCodeQuestion = {
      questionId: generateID,
      question: {
        questionTypeId: 5,
        value: 'Code',
        label: 'Code',
        icon: <CodeIcon />,
        coding: '',
        image: null,
        point: 0,
      },
      options: options,
    };
    return newCodeQuestion;
  };

  handleAddQuestion = (sectionIndex, questionIndex, event) => {
    event.preventDefault();
    //--- find current section
    let currentSection = this.state.sections[sectionIndex];
    //--- find current question
    let currentQuestion = currentSection.questions[questionIndex];
    //--- validate
    const valid = this.validateQuestion(currentQuestion);
    console.log(currentQuestion);
    if (!valid) {
      //--- copy old sections
      const updateSections = [...this.state.sections];
      updateSections.map((section) => {
        if (section.instructionId === currentSection.instructionId) {
          section.questions.map((question) =>
            question.questionId === currentQuestion.questionId
              ? (question = currentQuestion)
              : question
          );
        }
        return section;
      });
      // update UI
      this.setState({
        sections: updateSections,
      });
    }
    if (valid) {
      console.log(currentQuestion);
      let newQuestion = {};
      if (currentQuestion.question.questionTypeId === 1) {
        newQuestion = this.newMultiQuestion();
      } else if (currentQuestion.question.questionTypeId === 2) {
        newQuestion = this.newTrueFalseQuestion();
      } else if (currentQuestion.question.questionTypeId === 3) {
        newQuestion = this.newFillTheGapsQuestion();
      } else if (currentQuestion.question.questionTypeId === 4) {
        newQuestion = this.newQuestion();
      } else {
        newQuestion = this.newCodeQuestion();
      }
      //--- start using splice method
      let index = 0;
      let questions = currentSection.questions;
      for (let i = 0; i < questions.length; i++) {
        if (questions[i].questionId === currentQuestion.questionId) {
          index = i + 1;
          break;
        }
      }
      //--- copy old current section with lots of questions
      const copyQuestions = [...questions];
      //--- quesitons splice into new question
      copyQuestions.splice(index, 0, newQuestion);
      //--- copy old sections
      const updateSections = [...this.state.sections];
      updateSections.map((section) =>
        section.instructionId === currentSection.instructionId
          ? (section.questions = copyQuestions)
          : section
      );
      //we update UI
      this.setState({
        sections: updateSections,
      });
    }
  };

  handleAddSection = (event, sectionIndex) => {
    event.preventDefault();
    let currentSection = this.state.sections[sectionIndex];
    const valid = this.validateSection(currentSection);
    if (!valid) {
      const updateSections = this.state.sections.map((section) => {
        if (section.instructionId === currentSection.instructionId) {
          section = currentSection;
        }
        return section;
      });
      this.setState({
        sections: updateSections,
      });
    }
    if (valid) {
      // increasement++;
      let newSection = this.newSection();
      //--- start using splice method
      let index = 0;
      for (let i = 0; i < this.state.sections.length; i++) {
        if (
          this.state.sections[i].instructionId === currentSection.instructionId
        ) {
          index = i + 1;
          break;
        }
      }
      //--- splice section
      const newSections = [...this.state.sections];

      newSections.splice(index, 0, newSection);
      newSections.map(
        (section, index) =>
          (newSections[index].instruction = `Section ${arabicToRoman(
            index + 1
          )}`)
      );
      this.setState({
        sections: newSections,
      });
    }
  };

  handleDelete = (event, sectionIndex, questionId) => {
    event.preventDefault();
    //--- find current section
    let currentSection = this.state.sections[sectionIndex];
    //--- check sections length
    let sectionsLength = 0;
    sectionsLength = this.state.sections.length;
    //--- check sections length
    if (sectionsLength === 1) {
      this.state.sections.map((section) => {
        if (section.instructionId === currentSection.instructionId) {
          let updateQuestions = section.questions.filter(
            (question) => question.questionId !== questionId
          );
          this.setState({
            sections: [
              {
                instructionId: currentSection.instructionId,
                title: currentSection.title,
                instruction: currentSection.instruction,
                titleError: currentSection.titleError,
                instructionError: currentSection.instructionError,
                questions: updateQuestions,
              },
            ],
          });
        }
        return section;
      });
    } else {
      //--- sections-length > 1, current-section and question only 1
      if (sectionsLength > 1 && currentSection.questions.length === 1) {
        // i--;
        const updateSections = this.state.sections.filter(
          (section) => section.instructionId !== currentSection.instructionId
        );

        updateSections.map(
          (section, index) =>
            (updateSections[index].instruction = `Section ${arabicToRoman(
              index + 1
            )}`)
        );

        //--- update UI
        this.setState({
          sections: updateSections,
        });
      }
      //--- sections-length > 1, current-section and questions
      else {
        let updateCurrentSection = null;
        let updateQuestions = null;
        updateQuestions = currentSection.questions.filter(
          (question) => question.questionId !== questionId
        );
        updateCurrentSection = {
          instructionId: currentSection.instructionId,
          title: currentSection.title,
          instruction: currentSection.instruction,
          titleError: currentSection.titleError,
          instructionError: currentSection.instructionError,
          questions: updateQuestions,
        };
        const updateSections = this.state.sections.map((section) => {
          if (section.instructionId === updateCurrentSection.instructionId) {
            section = updateCurrentSection;
          }
          return section;
        });
        //--- update UI
        this.setState({
          sections: updateSections,
        });
      }
    }
  };

  handlePreview = (event) => {
    event.preventDefault();
    const { sections } = this.state;
    let sectionValid = false;
    let questionValid = false;
    sections.map((section) => {
      let validSection = this.validateSection(section);

      if (!validSection) {
        const updateSections = [...sections];
        updateSections.map((s) =>
          s.instructionId === section.instructionId ? (s = section) : s
        );
        this.setState({ ...this.state, sections: updateSections });
        console.log('section-not-valid: ', section);
      } else {
        console.log('section-valid: ', validSection);
        sectionValid = true;
      }

      section.questions.map((question) => {
        let validQuestion = this.validateQuestion(question);

        if (!validQuestion) {
          console.log('========> question-not-valid: ', question);
          const updateSections = [...sections];
          const updateSection = section.questions.map((q) =>
            q.questionId === question.questionId ? (q = question) : q
          );
          updateSections.map((s) =>
            s.instructionId === section.instructionId ? (s = updateSection) : s
          );
          this.setState({ ...this.state, secions: updateSections });
        } else {
          console.log('question-valid: ', validQuestion);
          questionValid = true;
        }
        return question;
      });
      return section;
    });

    // it it all valid
    // debug purpose
    console.log(
      `${sectionValid} ${sectionValid && questionValid} ${questionValid}`
    );
    if (sectionValid && questionValid) {
      console.log('==>this.state.sections:', this.state.sections);

      //--- request body for preview
      const { quizData } = this.props.location.state;
      const responses = [];
      for (let i = 0; i < this.state.sections.length; i++) {
        const responseDataObject = {
          instructionId: this.state.sections[i].instructionId,
          instruction: this.state.sections[i].instruction,
          title: this.state.sections[i].title,
        };
        const response = [];
        for (let j = 0; j < this.state.sections[i].questions.length; j++) {
          const questionDataObject = {};
          const questionTypeId = this.state.sections[i].questions[j].question
            .questionTypeId;
          const question = this.state.sections[i].questions[j];
          if (questionTypeId === 1) {
            questionDataObject.questionId = question.questionId;
            questionDataObject.question = question.question.multiQuestion;
            questionDataObject.questionType_id = questionTypeId;
            questionDataObject.image = null;
            questionDataObject.questionType = question.question.label;
            questionDataObject.answer = {
              option1: {
                answer: question.question.multiAnswerOne,
                isCorrect: question.question.checkOne,
              },
              option2: {
                answer: question.question.multiAnswerTwo,
                isCorrect: question.question.checkTwo,
              },
              option3: {
                answer: question.question.multiAnswerThree,
                isCorrect: question.question.checkThree,
              },
              option4: {
                answer: question.question.multiAnswerFour,
                isCorrect: question.question.checkFour,
              },
            };
            questionDataObject.point = 0;
            questionDataObject.qPoint = question.question.point;
          } else if (questionTypeId === 2) {
            questionDataObject.questionId = question.questionId;
            questionDataObject.question = question.question.trueFalseQuestion;
            questionDataObject.questionType_id = questionTypeId;
            questionDataObject.image = null;
            questionDataObject.questionType = question.question.label;
            questionDataObject.answer = null;
            questionDataObject.point = 0;
            questionDataObject.qPoint = question.question.point;
          } else if (questionTypeId === 3) {
            questionDataObject.questionId = question.questionId;
            questionDataObject.question = question.question.fillTheGaps;
            questionDataObject.questionType_id = questionTypeId;
            questionDataObject.image = null;
            questionDataObject.questionType = question.question.label;
            questionDataObject.answer = null;
            questionDataObject.point = 0;
            questionDataObject.qPoint = question.question.point;
          } else if (questionTypeId === 4) {
            questionDataObject.questionId = question.questionId;
            questionDataObject.question = question.question.question;
            questionDataObject.questionType_id = questionTypeId;
            questionDataObject.image = null;
            questionDataObject.questionType = question.question.label;
            questionDataObject.answer = null;
            questionDataObject.point = 0;
            questionDataObject.qPoint = question.question.point;
          } else {
            questionDataObject.questionId = question.questionId;
            questionDataObject.question = question.question.coding;
            questionDataObject.questionType_id = questionTypeId;
            questionDataObject.image = null;
            questionDataObject.questionType = question.question.label;
            questionDataObject.answer = null;
            questionDataObject.point = 0;
            questionDataObject.qPoint = 20;
          }
          response.push(questionDataObject);
        }
        responseDataObject.response = [...response];
        responses.push(responseDataObject);
      }
      const data = {
        quizId: this.state.quizId,
        title: quizData.title,
        subject: quizData.subject,
        date: Date.now(),
        duration: quizData.duration,
        status: false,
        generation_id: 0,
        teacher_id: 0,
        responses: [...responses],
      };
      // console.log("data Json : ", data)
      localStorage.setItem('dataPreview', JSON.stringify(data));
      // console.log(this.props.history)
      window.open('/create-quiz/previewData', '_blank');
    }
  };

  handleSave = async (event) => {
    event.preventDefault();
    const { quizId, sections } = this.state;
    let sectionValid = false;
    let questionValid = false;
    sections.map((section) => {
      let validSection = this.validateSection(section);

      if (!validSection) {
        const updateSections = [...sections];
        updateSections.map((s) =>
          s.instructionId === section.instructionId ? (s = section) : s
        );
        this.setState({ ...this.state, sections: updateSections });
        console.log('section-not-valid: ', section);
      } else {
        console.log('section-valid: ', validSection);
        sectionValid = true;
      }

      section.questions.map((question) => {
        let validQuestion = this.validateQuestion(question);

        if (!validQuestion) {
          console.log('========> question-not-valid: ', question);
          const updateSections = [...sections];
          const updateSection = section.questions.map((q) =>
            q.questionId === question.questionId ? (q = question) : q
          );
          updateSections.map((s) =>
            s.instructionId === section.instructionId ? (s = updateSection) : s
          );
          this.setState({ ...this.state, secions: updateSections });
        } else {
          console.log('question-valid: ', validQuestion);
          questionValid = true;
        }
        return question;
      });
      return section;
    });

    // it it all valid
    // debug purpose
    console.log(
      `${sectionValid} ${sectionValid && questionValid} ${questionValid}`
    );
    if (sectionValid && questionValid) {
      console.log(sections);
      const allQuestions = [];
      this.setState({ loading: true });
      for (let i = 0; i < sections.length; i++) {
        try {
          const sectionData = await Promise.race([
            this.postSection(sections, i, quizId),
          ]);
          console.log('post-section-success: ', sectionData);
          for (let j = 0; j < sections[i].questions.length; j++) {
            const questions = sections[i].questions;
            const question = questions[j].question;
            const questionTypeId = question.questionTypeId;
            if (questionTypeId === 1) {
              let dataMultiQuestion = {};
              await Promise.all([
                this.postMultipleChoiceAnswer(questions, j)
                  .then(async (res) => {
                    console.log('post-multi-answer-success: ', res);
                    dataMultiQuestion = await this.postMultipleChoiceQuestion(
                      questions,
                      j,
                      sectionData.id,
                      res.data.answerId
                    )
                      .then((res) => {
                        console.log('post-multi-q-success: ', res);
                        return res;
                      })
                      .catch((error) => {
                        console.log('post-multi-q-fail: ', error);
                        return null;
                      });
                    return res;
                  })
                  .catch((error) => {
                    console.log('post-multi-answer-fail: ', error);
                    return null;
                  }),
              ]);
              allQuestions.push(dataMultiQuestion);
            } else if (questionTypeId === 2) {
              let dataTrueFalseQuestion = {};
              await Promise.all([
                this.postTrueFalseAnswer(questions, j)
                  .then(async (res) => {
                    console.log('post-true-false-answer-success: ', res);
                    dataTrueFalseQuestion = await this.postTrueFalseQuestion(
                      sections[i].questions,
                      j,
                      sectionData.id,
                      res.data.answerId
                    )
                      .then((res) => {
                        console.log('post-true-false-q-success: ', res);
                        return res;
                      })
                      .catch((error) => {
                        console.log('post-true-false-q-fail: ', error);
                        return null;
                      });
                    return res;
                  })
                  .catch((error) => {
                    console.log('post-true-false-answer-fail: ', error);
                    return null;
                  }),
              ]);
              allQuestions.push(dataTrueFalseQuestion);
            } else if (questionTypeId === 3) {
              const dataFillTheGaps = await this.postFillTheGapsQuestion(
                questions,
                j,
                sectionData.id
              )
                .then((res) => {
                  console.log('post-fill-success: ', res);
                  return res;
                })
                .catch((error) => {
                  console.log('post-fill-fail: ', error);
                  return null;
                });
              allQuestions.push(dataFillTheGaps);
            } else if (questionTypeId === 4) {
              const dataQuestion = await this.postQuestion(
                questions,
                j,
                sectionData.id
              )
                .then((res) => {
                  console.log('post-q-success: ', res);
                  return res;
                })
                .catch((error) => {
                  console.log('post-q-fail: ', error);
                  return null;
                });
              allQuestions.push(dataQuestion);
            } else {
              const dataCodeQuestion = await this.postCodeQuestion(
                questions,
                j,
                sectionData.id
              )
                .then((res) => {
                  console.log('post-code-success: ', res);
                  return res;
                })
                .catch((error) => {
                  console.log('post-code-fail: ', error);
                  return null;
                });
              allQuestions.push(dataCodeQuestion);
            }
          }
        } catch (error) {
          console.log('post-section-fail: ', error);
          return null;
        }
      }
      if (allQuestions.length !== 0) {
        // alert(allQuestions[0].message);
        Swal.fire('Successfully', allQuestions[0].message, 'success').then(
          () => (window.location.href = '/admin/classwork')
        );
      }
    }
  };

  postSection = async (sections, i, quizId) => {
    try {
      const response = await Axios.post(`${BASE_URL}/instructions`, {
        instruction: sections[i].instruction,
        title: sections[i].title,
        quizId: quizId,
      });
      console.log('Section', i + 1, response.data.data.id);
      return response.data.data;
    } catch (error) {
      console.log(error);
    }
  };

  //--- Post Answer Start
  // questionTypeId = 1
  postMultipleChoiceAnswer = async (questions, j) => {
    let multiRequest = {
      answer: {
        option1: {
          answer: questions[j].question.multiAnswerOne,
          isCorrect: questions[j].question.checkOne
            ? questions[j].question.checkOne + ''
            : 'false',
        },
        option2: {
          answer: questions[j].question.multiAnswerTwo,
          isCorrect: questions[j].question.checkTwo
            ? questions[j].question.checkTwo + ''
            : 'false',
        },
        option3: {
          answer: questions[j].question.multiAnswerThree,
          isCorrect: questions[j].question.checkThree
            ? questions[j].question.checkThree + ''
            : 'false',
        },
        option4: {
          answer: questions[j].question.multiAnswerFour,
          isCorrect: questions[j].question.checkFour
            ? questions[j].question.checkFour + ''
            : 'false',
        },
      },
    };
    console.log(multiRequest);
    try {
      const response = await Axios.post(
        `${BASE_URL}/answers/multiple`,
        multiRequest,
        headers
      );
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  // questionTypeId = 2
  postTrueFalseAnswer = async (questions, j) => {
    try {
      const response = await Axios.post(
        `${BASE_URL}/answers`,
        {
          answer: {
            answer: questions[j].question.trueFalseValue,
          },
        },
        headers
      );
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  // quesitonTypeId = 4 skip
  //--- Post Answer End

  //--- Post Question Start
  // questionTypeId = 1
  postMultipleChoiceQuestion = async (
    questions,
    j,
    instructionId,
    answerId
  ) => {
    try {
      const response = await Axios.post(`${BASE_URL}/questions`, {
        question: questions[j].question.multiQuestion,
        image: null,
        questionTypeId: questions[j].question.questionTypeId,
        instructionId, //need
        answerId, //need
        point: questions[j].question.point,
      });
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  // questionTypeId = 2
  postTrueFalseQuestion = async (questions, j, instructionId, answerId) => {
    try {
      const response = await Axios.post(`${BASE_URL}/questions`, {
        question: questions[j].question.trueFalseQuestion,
        image: null,
        questionTypeId: questions[j].question.questionTypeId,
        instructionId, //need
        answerId, //need
        point: questions[j].question.point,
      });
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  // questionTypeId = 3
  postFillTheGapsQuestion = async (questions, j, instructionId) => {
    const questionTypeId = questions[j].question.questionTypeId;
    try {
      const response = await Axios.post(`${BASE_URL}/questions`, {
        question: questions[j].question.fillTheGaps,
        image: null,
        questionTypeId,
        instructionId, //need
        answerId: 0, //need
        point: questions[j].question.point,
      });
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  // questionTypeId = 4
  postQuestion = async (questions, j, instructionId) => {
    const questionTypeId = questions[j].question.questionTypeId;
    try {
      const response = await Axios.post(`${BASE_URL}/questions`, {
        question: questions[j].question.question,
        image: null,
        questionTypeId,
        instructionId, //need
        answerId: 0, //need
        point: questions[j].question.point,
      });
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  // questionTypeId = 5
  postCodeQuestion = async (questions, j, instructionId) => {
    console.log('post-code-q-request-body: ', questions[j].question);
    const questionTypeId = questions[j].question.questionTypeId;
    try {
      const response = await Axios.post(`${BASE_URL}/questions`, {
        question: questions[j].question.coding,
        image: null,
        questionTypeId,
        instructionId, //need
        answerId: 0, //need
        point: questions[j].question.point,
      });
      return response.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };
  //--- Post Question End
}

const style = {
  navbar: {
    backgroundColor: '#41AAA8',
    marginBottom: '15px',
  },
  navContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    justifyContent: 'flex-end',
    height: '100px',
    marginRight: 'auto',
  },
  navTitle: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: '19px',
  },
  navDesc: {
    color: 'white',
    fontSize: '16px',
  },
  btnSave: {
    backgroundColor: 'white',
    height: '38px',
    border: 0,
  },
  spanText: {
    fontWeight: 'bold',
    marginLeft: '5px',
    color: 'black',
    fontSize: '15px',
  },
};

export default CreateQuiz;
