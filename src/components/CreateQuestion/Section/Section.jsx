import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { Container, Card, Form } from 'react-bootstrap';

/**
 * Original Author: Svay Kong
 * Created Date: 14-08-2020
 * Development Group: HRD_EXAM
 * Description: This class is described how teacher create section
 */
function Section(props) {
  //--- define state
  const [title, setTitle] = useState(props.section.title);
  //--- destructure props
  const {
    section: { titleError, instruction },
    sectionIndex,
    handleChange,
  } = props;

  return (
    <Card className='my-2 styleCard'>
      <Card.Body className='styleBodySession'>
        <Form.Group>
          <Container>
            <TextField
              name='instruction'
              style={{ width: '530px', marginTop: '20px' }}
              value={instruction}
              inputProps={{
                readOnly: Boolean(true),
                disabled: Boolean(true),
              }}
              placeholder='Section'
            />
            <TextField
              name='title'
              value={title}
              style={{ width: '530px' }}
              placeholder='Instruction'
              className='mt-4'
              onChange={(event) => {
                setTitle(event.target.value);
                handleChange(event, sectionIndex);
              }}
            />
            {titleError ? <h6 className='text-danger'>{titleError}</h6> : null}
          </Container>
        </Form.Group>
      </Card.Body>
    </Card>
  );
}

export default Section;
