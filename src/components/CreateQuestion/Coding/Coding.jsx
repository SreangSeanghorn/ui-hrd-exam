import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import sanitizeHtml from 'sanitize-html';
//import SyntaxHighlighter from 'react-syntax-highlighter';
//import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import axios from 'axios';
//import parse from 'html-react-parser';
import { BASE_URL } from '../../../config/API';
import { TextField } from '@material-ui/core';

export default class Coding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      dataHtml: '',
      dataArr: [],
      point: 0,
      pointStateError: '',
      coding: '',
    };
  }

  render() {
    // let regex = /^<pre>/;
    const { editorState } = this.state;
    const { pointError, codingError } = this.props.question;
    console.log('dataHTML: ', this.state.dataHtml);
    console.log('dataArr: ', this.state.dataArr);
    console.log(this.props);
    return (
      <div className='w-100 m-auto'>
        <TextField
          type='number'
          className='px-2 MuiInputBase-input-text'
          name='point'
          value={this.state.point}
          onChange={this.onPointChange}
          inputProps={{ style: styles.align }}
          style={styles.textField}
          multiline
          placeholder='points'
        />
        {pointError || this.state.pointStateError ? (
          <h6 className='text-danger'>
            {pointError || this.state.pointStateError}
          </h6>
        ) : null}
        <div>
          <Editor
            editorState={editorState}
            wrapperClassName='demo-wrapper'
            editorClassName='demo-editor'
            toolbar={{
              inline: { inDropdown: true },
              list: { inDropdown: true },
              textAlign: { inDropdown: true },
              link: { inDropdown: true },
              history: { inDropdown: true },
              image: {
                uploadCallback: this.uploadImageCallBack,
                previewImage: true,
                alt: { present: true, mandatory: false },
                inputAccept: 'image/jpeg,image/jpg,image/png',
                defaultSize: {
                  height: 'auto',
                  width: '100%',
                },
              },
            }}
            onEditorStateChange={this.onEditorStateChange}
          />
          {codingError ? <h6 className='text-danger'>{codingError}</h6> : null}
        </div>
      </div>
    );
  }

  onPointChange = (event) => {
    const question = {
      sectionIndex: this.props.sectionIndex,
      questionIndex: this.props.questionIndex,
    };
    let inputValue = event.target.value;
    if (inputValue === '') {
      this.setState({
        pointStateError: 'Point cannot be empty!',
        point: inputValue,
      });
    } else if (!inputValue.match(/^[0-9]+$/)) {
      this.setState({
        pointStateError: 'Invalid number!',
        point: inputValue,
      });
    } else {
      this.setState({
        pointStateError: '',
        point: inputValue,
      });
    }
    this.props.onQuestionChange(question, event);
  };

  onEditorStateChange = (editorState) => {
    const question = {
      sectionIndex: this.props.sectionIndex,
      questionIndex: this.props.questionIndex,
    };
    this.setState(
      {
        editorState,
        dataHtml: draftToHtml(convertToRaw(editorState.getCurrentContent())),
      },
      () => {
        this.splitContent();
      }
    );
    this.props.onQuestionChange(question, this.state.dataHtml);
  };

  async uploadImageCallBack(file) {
    const form = new FormData();
    form.append('file', file);
    // form.set('file', file);
    return await axios
      .post(`${BASE_URL}/image/upload`, form, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
        },
      })
      .then((response) => {
        console.log(response);
        return {
          data: {
            link: response.data.data,
          },
        };
      })
      .catch((error) => {
        console.log('post-img-error: ', error);
        return null;
      });
  }

  splitContent = () => {
    const content = this.state.dataHtml;
    var dataSlipt = content.split(/\n/);
    this.setState({
      dataArr: [...dataSlipt],
    });
  };

  cleanHtmlString = (html) => {
    var clean = sanitizeHtml(html, {
      allowedTags: ['br', 'div'],
    });
    var cleanHtml = clean
      .replace(/<br \/>/g, '\n')
      .replace(/&lt;/g, '<')
      .replace(/&gt;/g, '>')
      .replace(/&amp;/g, '&');

    return cleanHtml;
  };
}

const styles = {
  textField: {
    width: '113px',
  },
  align: {
    textAlign: 'center',
  },
};
