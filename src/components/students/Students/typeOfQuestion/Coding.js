import React, { useState, Fragment } from 'react';
import { Card, Form } from 'react-bootstrap';
import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-javascript';
//--- prism-themes
import 'prism-themes/themes/prism-atom-dark.css';
import parse from 'html-react-parser';

export default function Section5({
  sectionIndex,
  questionTypeId,
  // key,
  question,
  index,
  onGetAnswer,
  point,
}) {
  const [javascriptCode, setJavaScriptCode] = useState('');
  const anwserQuestion = {
    questionIndex: index,
    sectionIndex: sectionIndex,
    questionTypeId: questionTypeId,
  };

  const content = question;
  var dataSplit = content.split(/\n/);
  
  return (
    <div>
      <form>
        <Card style={{ width: '100%' }} className='cardTop'>
          <Form.Control
            disabled
            className='boxCard'
            type='text'
            value={point}
          />
          <Card.Body className='textAnswer'>
            {dataSplit.map((data) => parse(data, { trim: true }))}
          </Card.Body>
        </Card>
        <br />
        <Card style={{ backgroundColor: '#ececec2e' }}>
          <Card.Body>
            <Editor
              name='code'
              value={javascriptCode}
              onValueChange={(javascriptCode) =>
                setJavaScriptCode(javascriptCode)
              }
              highlight={(javascriptCode) =>
                highlight(javascriptCode, languages.javascript)
              }
              padding={10}
              tabSize={4}
              preClassName='#fff'
              style={{
                width: '100%',
                minHeight: '250px',
                fontFamily: '"Consolas", monospace',
                fontSize: 18,
              }}
              onBlur={(e) => onGetAnswer(anwserQuestion, e)}
            />
          </Card.Body>
        </Card>
      </form>
    </div>
  );
}
