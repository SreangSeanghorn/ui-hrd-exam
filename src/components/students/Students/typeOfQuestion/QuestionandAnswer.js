import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class Section4 extends Component {
  render() {
    const {
      sectionIndex,
      questionTypeId,
      // key,
      question,
      index,
      point,
      onGetAnswer,
    } = this.props;
    const anwserQuestion = {
      questionIndex: index,
      sectionIndex: sectionIndex,
      questionTypeId: questionTypeId,
    };
    return (
      <div>
        <Card style={{ width: "100%" }} className="cardTop">
        <Form.Control
            disabled
            className="boxCard"
            type="text"
            value={point}
          />
          <Card.Body className="textAnswer">
            {index + 1}
            <span>. </span>
            {question}
          </Card.Body>
          <Card.Body>
            <TextField
              id="standard-full-width"
              multiline
              fullWidth
              placeholder="Answer"
              name="answer"
              className="px-2 textAnswer"
              onBlur={(e) => onGetAnswer(anwserQuestion, e)}
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
