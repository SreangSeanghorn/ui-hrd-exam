import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class Section3 extends Component {
  render() {
    const {
      sectionIndex,
      questionTypeId,
      // key,
      question,
      index,
      onGetAnswer,
      point
    } = this.props;
    const anwserQuestion = {
      questionIndex: index,
      sectionIndex: sectionIndex,
      questionTypeId: questionTypeId,
    };
    return (
      <div>
        <Card style={{ width: "100%" }} className="cardTop">
        <Form.Control
            disabled
            className="boxCard"
            type="text"
            value={point}
          />
          <Card.Body className="textAnswer">
          {index + 1}{". "+question}
          </Card.Body>
          <Card.Body>
            <TextField
              id="standard-basic"
              fullWidth
              multiline
              placeholder="Answer"
              name="answer"
              className="px-2"
              onBlur={(e) => onGetAnswer(anwserQuestion, e)}
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
