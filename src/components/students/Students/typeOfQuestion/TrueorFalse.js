import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
export default class Section1 extends Component {
  constructor(props) {
    super(props);
    this.state = { anwser: "" };
    // this.getAnswer = this.getAnswer.bind(this);
  }
  getAnswer = (e) => {
    let data = e.target.value
    this.setState({
      anwser: data,
    });
    // console.log(e.target.value)
  };
  render() {
    console.log(this.state.anwser)
    const correct = {
      borderColor: "#47a8ab",
    };
    const {
      sectionIndex,
      questionTypeId,
      qPoint,
      question,
      index,
      onGetAnswer,
    } = this.props;
    const anwserQuestion = {
      questionIndex: index,
      sectionIndex: sectionIndex,
      questionTypeId: questionTypeId,
    };
    return (
      <div>
        <Card style={correct} className="cardTop">
          <Form.Control
            disabled
            className="boxCard"
            type="text"
            value={qPoint}
          />
          <Card.Body className="textAnswer">
            <TextField
              className="px-2 MuiInputBase-input-text"
              name="truFalse"
              // onChang={(e) => this.getAnswer(e)}
              onBlur={(e) => {
                onGetAnswer(anwserQuestion, e);
                this.getAnswer(e)
              }}
            />
            {index + 1}
            {". " + question}
          </Card.Body>
        </Card>
      </div>
    );
  }
}
