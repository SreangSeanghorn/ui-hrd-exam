import React, { Component } from "react";
import { Card, Form } from "react-bootstrap";
// import "../../../../assets/css/TabSection.css";
export default class Section2 extends Component {
  render() {
    const {
      sectionIndex,
      questionTypeId,
      // key,
      question,
      index,
      onGetAnswer,
      point,
    } = this.props;
    const anwserQuestion = {
      questionIndex: index,
      sectionIndex: sectionIndex,
      questionTypeId: questionTypeId,
    };
    return (
      <div>
        <Card style={{ width: "100%" }} className="cardTop">
          <Form.Control
            disabled
            className="boxCard"
            type="text"
            value={point}
          />
          <Card.Body className="bodyAnswer textAnswer">
            {index + 1}
            <span>. </span>
            {question}
          </Card.Body>

          <Card.Body>
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer "
              id={`custom-inline-checkbox-${index}`}
              label={this.props.anw1}
              custom
              name="answer_1"
              // onChange={(e) => console.log("1")}
              onChange={(e) => onGetAnswer(anwserQuestion, e)}
            />
            <Form.Check
              // checked={}
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              id={`custom-inline-checkbox-${index+100}`}
              label={this.props.anw2}
              custom
              name="answer_2"
              // onChange={(e) => console.log("2")}
              onChange={(e) => onGetAnswer(anwserQuestion, e)}
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              id={`custom-inline-checkbox-${index+2000}`}
              label={this.props.anw3}
              name="answer_3"
              custom
              // onChange={(e) => console.log("3")}
              onChange={(e) => onGetAnswer(anwserQuestion, e)}
            />
            <Form.Check
              type="checkbox"
              className="my-1 mr-sm-2 pl-5 textAnswer"
              id={`custom-inline-checkbox-${index+3000}`}
              label={this.props.anw4}
              name="answer_4"
              custom
              // onChange={(e) => console.log("4")}
              onChange={(e) => onGetAnswer(anwserQuestion, e)}
            />
          </Card.Body>
        </Card>
      </div>
    );
  }
}
