import React from 'react'
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import ReactDOM from "react-dom";
import { Form,Button, Container } from 'react-bootstrap';
// import ClearIcon from '@material-ui/icons/Clear';

 function ModalCard() {
    const [isOpen, setIsOpen] = React.useState(false);

    const showModal = () => {
      setIsOpen(true);
    };
  
    const hideModal = () => {
      setIsOpen(false);
    };
    const style = {
      textBox : {
        width : '60%',
        borderRadius : '12px',
        margin : 'auto'
      }
    }
    return (
        <>
      <button onClick={showModal}>Display Modal</button>
      <Container style= {{fontFamily : 'Roboto '}}>
        <Modal 
        show={isOpen} 
        onHide={hideModal} 
        style= {{textAlign : "center"}} 
        centered
        backdrop="static"
        keyboard={false}
       
        >
          <Modal.Header closeButton >
            <Modal.Title >
                <div style= {{fontFamily : 'Roboto '}}>
                Student ID
              </div>
                
            </Modal.Title>
          </Modal.Header>

          <Modal.Body style= {{borderRadius : "20px"}}>
            <Form.Group>
            <Form.Control type="text" placeholder="Card ID" className= "mb-4 mt-4" style = {style.textBox}/>
            <p >Cookies must be enable in your browser</p>
            <Button variant="info" style = {{fontFamily : 'Roboto', width : '140px'}} className = "mt-3">START</Button>
            </Form.Group>
          </Modal.Body>
        </Modal>
      </Container>
    </>
    );
}
const rootElement = document.getElementById("root");
ReactDOM.render(<ModalCard/>, rootElement);
export default ModalCard;