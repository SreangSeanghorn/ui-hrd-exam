import React, { Component } from 'react';
import Tabars from './Tabars';
import { postResult } from '../../../redux/actions/postResultAction';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Swal from 'sweetalert2';
class Student extends Component {
  constructor() {
    super();
    this.state = this.submitHandler.bind(this);
  }
  submitHandler = (result) => {
    postResult(result, (msg) => {
      if (msg !== 'Cannot submitted the result twice.') {
        Swal.fire('Successfully.', `${msg}`, 'success').then((res) => {
          window.location.href = '/';
        });
      } else {
        Swal.fire('Submit faild.', `${msg}`, 'warning').then((res) => {
          window.location.href = '/';
        });
      }
    });
  };

  render() {
    return (
      <div>
        <Tabars onSubmitHandler={this.submitHandler} />
      </div>
    );
  }
}

const mapDispatchToProps = (dp) => {
  const boundActionCreators = bindActionCreators(
    {
      postResult,
    },
    dp
  );
  return boundActionCreators;
};

export default connect(null, mapDispatchToProps)(Student);
