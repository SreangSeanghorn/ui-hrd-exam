import React, { useState, useEffect } from 'react';
import { Tab, Form, Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { getStudentQuizz } from '../../../redux/actions/studentquizzAction';
import { bindActionCreators } from 'redux';
import logoImage from '../../../assets/img/hrd_exam_logo.png';
import { Button, Col, Nav, Row } from 'react-bootstrap';
import '../../../assets/css/TabSection.css';
import { withRouter } from 'react-router-dom';
// import Modal from "react-bootstrap/Modal";
import 'bootstrap/dist/css/bootstrap.min.css';
import { CustomLoading } from '../../utils/Utils';
import Swal from 'sweetalert2';
//import type Card
import TrueorFalse from '../Students/typeOfQuestion/TrueorFalse';
import MultipleChoice from '../Students/typeOfQuestion/MultipleChoice';
import FillintheGap from '../Students/typeOfQuestion/FillintheGap';
import QuestionandAnswer from '../Students/typeOfQuestion/QuestionandAnswer';
import Coding from '../Students/typeOfQuestion/Coding';
function Tabars(props) {
  // const [isOpen, setIsOpen] = useState(false);
  const [questions, setQuestion] = useState([]);

  const {
    // handeChangeAnswer,
    getStudentQuizz,
    studentQuizzData,
    onSubmitHandler,
    duration,
  } = props;
  const [seconds, setSeconds] = useState(60);
  const [minutes, setMinutes] = useState({ duration: 0, seconds: 0 });
  const timer = null ;
  const timeSecond = false;

  // const [counter, setCounter] = useState(90);

  useEffect(() => {
    getStudentQuizz();
  }, [getStudentQuizz]);

  useEffect(() => {
    if (studentQuizzData) {
      setQuestion(studentQuizzData);
    }
  }, [studentQuizzData]);
  useEffect(() => {
    if (duration !== undefined) {
      setMinutes(duration-1);
    }
  }, [setMinutes, duration]);
  useEffect(() => {
    const timeSecond =
      seconds > 0 &&
      setInterval(() => {
        setSeconds(seconds - 1);
        // setSeconds(seconds)
      }, 1000);
      if (seconds===0 && minutes===0){
        const student_id = props.location.state.studentId
    let filterResponses = [...questions];
    let jsonContainer = { filterResponses };
    let answer = {
      jsonContainer,
      quiz_id: parseInt(props.quizId),
      score: 0,
      student_id: parseInt(student_id),
    };
       Swal.fire('Time Over', 'Result has been submit', 'warning').then(res =>{
      onSubmitHandler(answer);
    });
    }
      else{
           return () => {
      clearInterval(timeSecond);
    };
      }
   
  }, [seconds, minutes]);

  useEffect(() => {
    const timer =
      seconds === 0 && minutes > 0 &&
      setInterval(() => {
        setMinutes(minutes - 1);
        setSeconds(60);
      }, 1000);

    return () => {
      clearInterval(timer);
    };
    // }
  }, [minutes, seconds]);

      
  const getAnswer = (question, e) => {
    const { value, checked, name } = e.target;
    const { questionIndex, sectionIndex, questionTypeId } = question;
    const section = questions[sectionIndex];
    const currentQuestion = section.response[questionIndex];
    if (questionTypeId !== 1) {
      currentQuestion.answer = value;
    } else {
      if (name === 'answer_1') {
        currentQuestion.answer.option1.isCorrect = '' + checked;
      } else if (name === 'answer_2') {
        currentQuestion.answer.option2.isCorrect = '' + checked;
      } else if (name === 'answer_3') {
        currentQuestion.answer.option3.isCorrect = '' + checked;
      } else if (name === 'answer_4') {
        currentQuestion.answer.option4.isCorrect = '' + checked;
      }
    }

    if (questionTypeId === 1) {
      const updateQuestions = [...questions];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      setQuestion(updateQuestions);
    }
    if (questionTypeId === 2) {
      const updateQuestions = [...questions];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      setQuestion(updateQuestions);
    }
    if (questionTypeId === 4) {
      const updateQuestions = [...questions];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      setQuestion(updateQuestions);
    }
    if (questionTypeId === 3) {
      // currentQuestion.point = 0;
      const updateQuestions = [...questions];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      setQuestion(updateQuestions);
    }
    if (questionTypeId === 5) {
      // currentQuestion.point = 0;
      const updateQuestions = [...questions];
      updateQuestions.map((q) => {
        if (q.instructionId === section.instructionId) {
          q.response.map((res) =>
            res.questionId === currentQuestion.questionId
              ? (res = currentQuestion)
              : res
          );
        }
        return q;
      });
      setQuestion(updateQuestions);
    }
  };


  const handleSubmitResult = () => {
    const student_id = props.location.state.studentId
    let filterResponses = [...questions];
    let jsonContainer = { filterResponses };
    let answer = {
      jsonContainer,
      quiz_id: parseInt(props.quizId),
      score: 0,
      student_id: parseInt(student_id),
    };
    Swal.fire({
      title: 'Are you sure to submit?',
      text: 'You sure to submit your result.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Submit',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.value) {
        onSubmitHandler(answer);
      }
    });
  };

  // prevent browser

  var browserPrefixes = ["moz", "ms", "o", "webkit"],
    isVisible = true; // internal flag, defaults to true

  // get the correct attribute name
  function getHiddenPropertyName(prefix) {
    return prefix ? prefix + "Hidden" : "hidden";
  }
  // get the correct event name
  function getVisibilityEvent(prefix) {
    return (prefix ? prefix : "") + "visibilitychange";
  }

  // get current browser vendor prefix
  function getBrowserPrefix() {
    for (var i = 0; i < browserPrefixes.length; i++) {
      if (getHiddenPropertyName(browserPrefixes[i]) in document) {
        // return vendor prefix
        return browserPrefixes[i];
      }
    }
    // no vendor prefix needed
    return null;
  }

  // bind and handle events
  var browserPrefix = getBrowserPrefix(),
    hiddenPropertyName = getHiddenPropertyName(browserPrefix),
    visibilityEventName = getVisibilityEvent(browserPrefix);

  function onVisible() {
    const student_id = props.location.state.studentId
    let filterResponses = [...questions];
    let jsonContainer = { filterResponses };
    let answer = {
      jsonContainer,
      quiz_id: parseInt(props.quizId),
      score: 0,
      student_id: parseInt(student_id),
    };
    // prevent double execution
    if (isVisible) {
      return;
    }
    // change flag value (visible)
    isVisible = true;
    Swal.fire('Cheating', 'Result has been submit', 'warning').then(res =>{
      onSubmitHandler(answer);
    })
  }

  function onHidden() {
    const student_id = localStorage.getItem("studentId");
    let filterResponses = [...questions];
    let jsonContainer = { filterResponses };
    let answer = {
      jsonContainer,
      quiz_id: parseInt(props.quizId),
      score: 0,
      student_id: parseInt(student_id),
    };
    // prevent double execution
    if (!isVisible) {
      return;
    }
    // change flag value (hidden)
    isVisible = false;
    Swal.fire('Cheating', 'Result has been submit', 'warning').then(res =>{
      onSubmitHandler(answer);
    })
  }

  function handleVisibilityChange(forcedFlag) {
    // forcedFlag is a boolean when this event handler is triggered by a
    // focus or blur eventotherwise it's an Event object
    if (typeof forcedFlag === "boolean") {
      if (forcedFlag) {
        return onVisible();
      }

      return onHidden();
    }

    if (document[hiddenPropertyName]) {
      return onHidden();
    }

    return onVisible();
  }
  document.addEventListener(visibilityEventName, handleVisibilityChange, false);

  // extra event listeners for better behaviour
  document.addEventListener(
    "focus",
    function () {
      handleVisibilityChange(true);
    },
    false
  );

  document.addEventListener(
    "blur",
    function () {
      handleVisibilityChange(false);
    },
    false
  );

  window.addEventListener(
    "focus",
    function () {
      handleVisibilityChange(true);
    },
    false
  );

  window.addEventListener(
    "blur",
    function () {
      handleVisibilityChange(false);
    },
    false
  );

  if(questions ===null){
    return (
      <div
      style={{
        textAlign: 'center',
      }}
      >
      <h3>Don't have quiz yet.</h3>
      <Button onClick={() => window.location.href="/"}>Reload</Button>
    </div>
    )
  }
  if (questions && questions.length > 0) {
    return (
      <>
        <div>
          <Tab.Container
            defaultActiveKey={
              questions.find((question) => question.instruction === 'Section I')
                .instructionId
            }
            id='tab-container'
            className='m-0'>
            <Row className=' m-0 nav_content-fixed' id='item'>
              <Col lg={2} className='fixed_nav' id='exam-logo'>
                <img
                  src={logoImage}
                  className='w-50 d-inline-block align-top py-3 '
                  alt='hrd-exam-logo'
                />
              </Col>
              <Col lg={8} className='nav_content'>
                <Row>
                  <Nav variant='pills'>
                    <Col lg={12} className='pt-3 pl-5'>
                      <Form inline>
                        {questions
                          .sort((a, b) =>
                            a.instruction.localeCompare(b.instruction)
                          )
                          .map((question) => {
                            return (
                              <Nav.Item key={question.instructionId}>
                                <Nav.Link
                                  eventKey={question.instructionId}
                                  className='nav-link text-nav  ml-3'>
                                  {question.instruction}
                                </Nav.Link>
                              </Nav.Item>
                            );
                          })}
                      </Form>
                    </Col>
                  </Nav>
                </Row>
              </Col>
              <Col lg={2} className='fixed_nav py-3'>
                <span className='text-nav'>
                  {minutes}: {seconds}
                </span>
                <Button
                  style={{ marginLeft: '10%' }}
                  className='btn-page-submit'
                  variant='light'
                  onClick={() => {
                    handleSubmitResult();
                  }}>
                  Submit
                </Button>
              </Col>
            </Row>
            <Row className=' m-0 body-content'>
              <Col lg={12}>
                <Row>
                  <Col lg={2}></Col>
                  <Col lg={8} className='m-5'>
                    <Tab.Content>
                      {questions
                        .sort((a, b) =>
                          a.instruction.localeCompare(b.instruction)
                        )
                        .map((question, sectionIndex) => {
                          return (
                            <Tab.Pane
                              eventKey={question.instructionId}
                              key={question.instructionId}>
                              <Card>
                                <Card.Body className='font-weight-bold textQuestionTitle'>
                                  {question.instruction}
                                </Card.Body>
                                <Card.Body className='pl-5  textQuestion'>
                                  {question.title}
                                </Card.Body>
                              </Card>
                              {question.response.map((value, index) => {
                                switch (value.questionType_id) {
                                  case 2:
                                    return (
                                      <TrueorFalse
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        questionId={value.questionId}
                                        question={value.question}
                                        index={index}
                                        qPoint={value.qPoint}
                                        point={value.point}
                                        onGetAnswer={getAnswer}
                                      />
                                    );
                                  case 1:
                                    return (
                                      <MultipleChoice
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        questionId={value.questionId}
                                        question={value.question}
                                        index={index}
                                        point={value.qPoint}
                                        onGetAnswer={getAnswer}
                                        anw1={value.answer.option1.answer}
                                        anw2={value.answer.option2.answer}
                                        anw3={value.answer.option3.answer}
                                        anw4={value.answer.option4.answer}
                                      />
                                    );
                                  case 3:
                                    return (
                                      <FillintheGap
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        questionId={value.questionId}
                                        question={value.question}
                                        index={index}
                                        point={value.qPoint}
                                        onGetAnswer={getAnswer}
                                      />
                                    );
                                  case 4:
                                    return (
                                      <QuestionandAnswer
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        questionId={value.questionId}
                                        question={value.question}
                                        index={index}
                                        point={value.qPoint}
                                        onGetAnswer={getAnswer}
                                      />
                                    );
                                  case 5:
                                    return (
                                      <Coding
                                        sectionIndex={sectionIndex}
                                        questionTypeId={value.questionType_id}
                                        key={value.questionId}
                                        questionId={value.questionId}
                                        question={value.question}
                                        index={index}
                                        point={value.qPoint}
                                        onGetAnswer={getAnswer}
                                      />
                                    );
                                  default:
                                    return null;
                                }
                              })}
                            </Tab.Pane>
                          );
                        })}
                    </Tab.Content>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Tab.Container>
        </div>
      </>
    );
  } else if (questions.length === 0) {
    return (
      <div style={style.content}>
        <CustomLoading />
      </div>
    );
  }
  return (
    <div style={style.content}>
      <CustomLoading />
    </div>
  );
}

const style = {
  border_text: {
    borderTop: '0',
    borderButtom: '0',
    borderLeft: '0',
    borderRight: '0',
    borderRadius: '0',
    padding: '0',
  },
  content: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    WebkitTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
};

const mapStateToProps = (state) => {
  return {
    studentQuizzData: state.studentQuizzData.data,
    quizId: state.studentQuizzData.quizId,
    loading: state.studentQuizzData.loading,
    duration: state.studentQuizzData.duration,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      getStudentQuizz,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Tabars));
