import React from 'react';
import { Navbar, Nav, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import '../../assets/css/HeaderLayout.css';
import logo from '../../assets/img/hrd_exam_logo.png';

const HeaderLayout = ({ loading, validToken, error }) => {
  const onLogout = () => {
    localStorage.removeItem('jwtToken');
    window.location.href = '/';
  };

  if (error) {
    return (
      <div className='text-center mt-3'>
        <h3 className='text-danger'> Error: {error}</h3>
      </div>
    );
  }

  if (loading) {
    return (
      <div style={style.content}>
        <CustomLoading />
      </div>
    );
  }

  return (
    <div>
      <Navbar expand='lg' className='bgPrimaryColor'>
        <Navbar.Brand href='#home' as={Link} to='/'>
          <Image
            src={logo}
            className='w-75 d-inline-block align-top'
            alt='hrd-exam-logo'
          />
        </Navbar.Brand>
        <Nav id='navbar-login' className='ml-auto'>
          {validToken ? (
            <LogoutNavItem handleLogout={onLogout} />
          ) : (
            <LoginNavItem />
          )}
        </Nav>
      </Navbar>
    </div>
  );
};

const style = {
  content: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    WebkitTransform: 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
  },
};

const LoginNavItem = () => {
  return (
    <Nav.Link
      href='#link'
      as={Link}
      to='/teacher-login'
      className='navLinkStyle font-face-en'>
      Login{' '}
      <svg
        xmlns='http://www.w3.org/2000/svg'
        width='28.626'
        height='29.02'
        viewBox='0 0 28.626 29.02'>
        <path
          id='log-in-a7786081a2d8459a25b5a44e5aeaa887'
          d='M115.864,124.615l-7.146-6.954a.651.651,0,0,0-1.107.446v4.182H95.9c-1.068,0-1.935,1.174-1.935,2.622s.866,2.622,1.935,2.622h11.713v4.482a.651.651,0,0,0,1.107.446l7.146-6.954A.619.619,0,0,0,115.864,124.615Zm-11.055,4.054h-4.26v5.957a4.209,4.209,0,0,0,4.26,4.146H118.33a4.209,4.209,0,0,0,4.26-4.146V113.9a4.209,4.209,0,0,0-4.26-4.146H104.809a4.209,4.209,0,0,0-4.26,4.146v7.255h4.26V113.9h13.515l.006,20.729H104.809ZM118.33,113.9v0Z'
          transform='translate(-93.964 -109.752)'
          fill='#fff'
        />
      </svg>
    </Nav.Link>
  );
};

const LogoutNavItem = (props) => {
  return (
    <Nav.Link
      href='#link'
      as={Link}
      to='/'
      onClick={() => props.handleLogout()}
      className='navLinkStyle font-face-en'>
      Logout{' '}
      <svg
        xmlns='http://www.w3.org/2000/svg'
        width='28.965'
        height='29.024'
        viewBox='0 0 28.965 29.024'>
        <path
          id='Path_208'
          data-name='Path 208'
          d='M23.919,23.768V19.414H12.655v-5.8H23.919V9.256l8.046,7.256-8.046,7.256M20.7,2a3.073,3.073,0,0,1,3.218,2.9V7.8H20.7V4.9H6.218V28.121H20.7v-2.9h3.218v2.9a3.073,3.073,0,0,1-3.218,2.9H6.218A3.073,3.073,0,0,1,3,28.121V4.9A3.073,3.073,0,0,1,6.218,2Z'
          transform='translate(-3 -2)'
          fill='#fff'
        />
      </svg>
    </Nav.Link>
  );
};

const CustomLoading = () => {
  return (
    <Loader
      className='text-center'
      type='ThreeDots'
      color='#00BFFF'
      height={100}
      width={100}
      timeout={5000}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.teacherData.loading,
    validToken: state.teacherData.validToken,
    error: state.teacherData.error,
  };
};

export default connect(mapStateToProps)(HeaderLayout);
