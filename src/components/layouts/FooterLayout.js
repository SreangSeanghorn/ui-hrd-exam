import React from 'react'
import footerBg from '../../assets/img/footer.png';

export const FooterLayout = () => {

    const backgroundStyle = { 
        backgroundImage: `url(${footerBg})`,
        width: "100%", 
        paddingBottom: "38.59%", 
        backgroundSize: "cover", 
        backgroundPosition: "center", 
        backgroundRepeat: "no-repeat",
        margin: "0",
    };

    return (
        <div style={backgroundStyle}>
        
        </div>
    )
}

