const {
  STUDENTQUIZZ_SUCCESS,
  STUDENTQUIZZ_FAILED,
  STUDENTQUIZZ_REQUEST,
} = require("../../actions/actionTypes");

const defaultState = {
  loading: false,
  data: [],
};
const getStudentQuizz = (state = defaultState, action) => {
  switch (action.type) {
    case STUDENTQUIZZ_REQUEST:
      return {
        ...state,
      };
    case STUDENTQUIZZ_SUCCESS:
      return {
        ...state,
        data: action.payload,
        quizId: action.quizId,
        duration:action.duration,
        loading: false,
        type: action.type,
      };
    case STUDENTQUIZZ_FAILED:
      return {
        ...state,
        // success: action.success,
        loading: false,
        type: action.type,
      };
    default:
      return state;
  }
};
export default getStudentQuizz;
