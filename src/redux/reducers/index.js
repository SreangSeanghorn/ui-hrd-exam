import { combineReducers } from 'redux';
import teacherReducer from './teacherReducer/teacherReducer';
import getQuestionReducers from './getQuestionReducers/getQuestionReducer';
import studentReducer from './studentReducer/studentReducer';

//Author: Nheng Vanchhay
import classworkReducers from './classworkReducers/classworkReducers';
import viewQuizReducers from './classworkReducers/viewQuizReducers';
import studentDataReducers from './studentDataReducer/studentDataReducer';
import generationReducers from './generationReducers/generationReducers';
import quizReducer from './crudQuizReducers/quizReducer'
import getResultReducers from './crudResultReducer/getResultReducer'
import getResultStudentReducer from './crudResultReducer/getResultStudentReducer'

import getQuestionByQiuzId from './crudQuestionReducers/getByQuizIdReducers'
import questionTypeReducer from './questionTypeReducer/questionTypeReducer'
import classNameReducer from './studentDataReducer/classNameReducer'
import createTeacherReducer from './createTeacherReducer/createTeacherReducer'
import classNameResponseReducer from './classNameResponseReducer/classNameResponseReducer'

//Import from Seyma
import postResult from './postResultReducers/postResultReducers';
import getStudentQuizzReducer from './studentQuizzReducers/studentQuizzReducers';

const reducers = {

    //Author: Nheng Vanchhay
    teacherData: teacherReducer,
    questionData: getQuestionReducers,
    studentData: studentReducer,
    classworkData: classworkReducers,
    studentDataAdmin: studentDataReducers,
    viewQuizData: viewQuizReducers,
    classNameData : classNameReducer,
    getResultStudentData: getResultStudentReducer,
    classNameResponseData: classNameResponseReducer,

    //dara
    generationData: generationReducers,

    quizzesData: quizReducer,

    questionTypeData: questionTypeReducer, 
    resultData: getResultReducers, 
    getQQId: getQuestionByQiuzId,

   //Code Seyma
    studentQuizzData:getStudentQuizzReducer,
    createTeacher : createTeacherReducer,
    postResult:postResult

};

export const rootReducer = combineReducers(reducers);