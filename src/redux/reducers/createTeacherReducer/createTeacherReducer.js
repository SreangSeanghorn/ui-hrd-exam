
const {
  TEACHER_FETCH,
  TEACHER_SUCCESS,
  TEACHER_FAILED,
  TEACHER_POST,
  TEACHER_DELETE,
  TEACHER_UPDATE,
  TEACHER_CHANGE,
} = require("../../actions/actionTypes");

const defaultState = {
  loading: false,
  message : '',
  data: [],
  error: "",
  status:false,
};

const getCreateTeacher = (state = defaultState, action) => {
  switch (action.type) {
    case TEACHER_FETCH:
      return {
        ...state,
        loading: true,
      };

    case TEACHER_POST:
      const users = state.data.concat(action.payload);
      return {
        ...state,
        users,
      };

        case TEACHER_SUCCESS: 
        return {
            ...state,
            data: action.data,
            loading: false,
            type: action.type,
            error: "",
        };
        case TEACHER_FAILED: 
        return {
            ...state,
            loading: true,
            type: action.type,
            error: action.error
        }
    case TEACHER_DELETE:
      return {
        status:true,
        data: state.data.filter((stu) => stu.id !== action.payLoad),
        message: action.message,
      };
    case TEACHER_UPDATE:
    case TEACHER_CHANGE:
    default:
      return state;
  }
};

export default getCreateTeacher;
