import { GET_QUESTION } from '../../actions/actionTypes';

const initialState = {
    data: null,
}
const getQuestionReducers = (state = initialState, action) => {
    switch(action.type) {       
        case GET_QUESTION:{
            return { 
                ...state,
                data: action.payLoad, 
            }
        }

        default: 
            return state
    }
}

export default getQuestionReducers;