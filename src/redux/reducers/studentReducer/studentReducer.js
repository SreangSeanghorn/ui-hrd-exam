import {
    STUDENT_REQUEST,
    STUDENT_LOGIN_CARD_ID_SUCCESS,
    STUDENT_REGISTER_SUCCESS,
    STUDENT_ERROR,
  } from '../../actions/actionTypes';
  
  const initialState = {
    loading: false,
    student: '',
    error: '',
  }
  
  const studentReducer = (state = initialState, action) => {
    switch (action.type) {
      case STUDENT_REQUEST:
        return {
          ...state,
          loading: true,
        }
  
      case STUDENT_LOGIN_CARD_ID_SUCCESS:
        console.log("action.payload:", action.payload)
        return {
          loading: false,
          student: action.payload,
          error: '',
        }
  
      case STUDENT_REGISTER_SUCCESS:
        return {
          ...state,
          loading: false,
          student: '',
          error: '',
        }

      case STUDENT_ERROR:
        console.log("STUDENT_ERROR")
        return {
          ...state,
          loading: false,
          student: '',
          error: action.payload,
        };
  
      default:
        return state;
    }
  };
  
  export default studentReducer;