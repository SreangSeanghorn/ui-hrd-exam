import {
  QUESTION_TYPE_REQUEST,
  QUESTION_TYPE_SUCCESS,
  QUESTION_TYPE_ERROR,
} from '../../actions/actionTypes';

const initialState = {
  loading: false,
  error: '',
  questionTypes: null,
};

const questionTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case QUESTION_TYPE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case QUESTION_TYPE_SUCCESS:
      return {
        loading: false,
        error: '',
        questionTypes: action.payload,
      };
    case QUESTION_TYPE_ERROR:
      return {
        loading: false,
        error: action.payload,
        questionTypes: null,
      };
    default:
      return state;
  }
};

export default questionTypeReducer;
