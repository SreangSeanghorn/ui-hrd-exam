const {
  STUDENT_REQUEST,
  SUTUDENT_SUCCESS,
  SUTUDENT_FAILED,
  SUTUDENT_DELETE,
  SUTUDENT_GEN_SUCCESS,
  // STUDENT_CLASS,
} = require("../../actions/actionTypes");

const defaultState = {
  loading: false,
  success: false,
  data: [],
  status: "",
};
const getStudents = (state = defaultState, action) => {
  switch (action.type) {
    case STUDENT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case SUTUDENT_SUCCESS:
      return {
        data: action.data.data,
        loading: false,
        // id : action.payload
      };
    case SUTUDENT_FAILED:
      return {
        ...state,
        success: action.success,
        loading: false,
        error: action.payload,
      };
    case SUTUDENT_DELETE:
      return {
        ...state,
        data: state.data.filter((stu) => stu.studentId !== action.payLoad),
        status: action.status,
      };
    case SUTUDENT_GEN_SUCCESS:
      return {
        data: action.payLoad,
        loading: false,
      };
    default:
      return state;
  }
};
export default getStudents;
