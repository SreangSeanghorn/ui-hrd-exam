const { STUDENT_CLASS } = require("../../actions/actionTypes");

const defaultState = {
    loading: false,
    data: [],
    error: "",
  };

  const getClassName = (state = defaultState, action) => {
      switch  (action.type) {
        case STUDENT_CLASS:
            return {
              ...state,
              data: action.data.data,
            //   loading: false,
            };
          default:
            return state;
      }
  }

  export default getClassName