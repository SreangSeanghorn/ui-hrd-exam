import { POST_RESULT_SUCCESS, POST_RESULT_FAILED,POST_RESULT_REQUEST } from "../../actions/actionTypes";

const defaultState = {
  loading: false,
  result: [],
  status: "",
};

const postResult = (state = defaultState, action) => {
  switch (action.type) {
    case POST_RESULT_REQUEST:
        return {
          ...state,
          loading: true,
        }
    case POST_RESULT_SUCCESS:
      return {
        ...state,
        loading: false,
        result: action.result,
        type: action.type,
        error: "",
      };
    case POST_RESULT_FAILED:
      return {
        ...state,
        type: action.type,
        loading: true,
      };

    default:
      return state;
  }
};
export default postResult;
