import {GET_QUESTION  } from '../../actions/actionTypes';

const initialState = {
    data:[],
    error: "",
}

const getQuestion = (state = initialState, action) => {
    switch(action.type) {

        case GET_QUESTION:
            return { 
                ...state, 
                data:action.data
            }
        
        default: 
            return state
    }
}

export default getQuestion;