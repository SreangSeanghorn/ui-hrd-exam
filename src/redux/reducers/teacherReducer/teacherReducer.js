import {
  TEACHER_REQUEST,
  TEACHER_LOGIN_SUCCESS,
  TEACHER_ERROR,
} from '../../actions/actionTypes';

const initialState = {
  loading: false,
  validToken: false,
  teacher: {},
  token: null,
  error: '',
};

const booleanActionPayload = (payload) => {
  if (payload) {
    return true;
  } else {
    return false;
  }
};

const teacherReducer = (state = initialState, action) => {
  switch (action.type) {
    case TEACHER_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case TEACHER_LOGIN_SUCCESS:
      return {
        loading: false,
        validToken: booleanActionPayload(action.payload),
        teacher: action.payload,
        error: '',
      };

    case TEACHER_ERROR:
      return {
        loading: false,
        validToken: booleanActionPayload(action.payload),
        teacher: {},
        error: action.payload,
      };

    default:
      return state;
  }
};

export default teacherReducer;
