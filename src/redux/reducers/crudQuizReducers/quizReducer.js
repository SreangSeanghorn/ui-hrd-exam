const {
  QUIZ_SUCCESS,
  QUIZ_FALSE,
  QUIZ_REQUEST,
  QUIZ_INSERT_SUCCESS,
  QUIZ_RETURN_SUCCESS,
} = require('../../actions/actionTypes');

const defaultState = {
  loading: false,
  error: '',
  data: [],
  quiz: {},
  paging: null,
};

const quizReducer = (state = defaultState, action) => {
  switch (action.type) {
    case QUIZ_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case QUIZ_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        data: action.data.data,
        paging: action.paging,
      };
    case QUIZ_INSERT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        quiz: action.payload,
      };
    case QUIZ_RETURN_SUCCESS: 
    return {
      ...state,
      loading: false,
      error: '',
      data: action.payLoad,
    };
    case QUIZ_FALSE:
      return {
        data: [],
        quiz: {},
        paging: null,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default quizReducer;
