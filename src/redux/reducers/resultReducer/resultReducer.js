import {
  POST_RESULT
  } from '../../actions/actionTypes';
  
  const initialState = {
    data: null,
  }
  
  const resultReducer = (state = initialState, action) => {
    switch (action.type) {
      case POST_RESULT:
        return {
          ...state,
          result: action.payLoad,
        }
      default:
        return state;
    }
  };
  
  export default resultReducer;