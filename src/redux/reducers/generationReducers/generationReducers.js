import {
  GENERATION_REQUEST,
  GENERATION_SUCCESS,
  GENERATION_FAILED,
  GENERATION_DELETE,
  GENERATION_ID_STATUS,
} from "../../actions/actionTypes";

const defaultState = {
  loading: false,
  genId: "",
  activeGen: "", 
  data: [],
  error: "",
};
const getGeneration = (state = defaultState, action) => {
  switch (action.type) {
    case GENERATION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GENERATION_SUCCESS:
      return {
        ...state,
        data: action.data.data,
        loading: false,
        error: "",
      };
    case GENERATION_ID_STATUS:
      // console.log(" action.activeGen", action);
      return { 
        ...state, 
        genId: action.payLoad ,
        activeGen : action.status
      };
    case GENERATION_FAILED:
      return {
        ...state,
        error: action.payload,
      };
    case GENERATION_DELETE:
      return {
        ...state,
        data: state.data.filter((cls) => cls.id !== action.payLoad),
        message: action.message,
      };
    default:
      return state;
  }
};
export default getGeneration;
