const {
  CLASSWORK_SUCCESS,
  CLASSWORK_REQUEST,
  CLASSWORK_FAILED,
  CLASSWORK_DELETE,
  CLASSWORK_GEN_SUCCESS,
} = require("../../actions/actionTypes");

const defaultState = {
  loading: false,
  data: [],
  error: "",
  // paping:{}
};
const Classwork = (state = defaultState, action) => {
  switch (action.type) {
    case CLASSWORK_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case CLASSWORK_SUCCESS:
      return {
        ...state,
        data: action.data.data,
        paging: action.paging,
        loading: false,
        error: "",
      };
    case CLASSWORK_GEN_SUCCESS:
      return {
        ...state,
        data: action.payLoad,
        loading: false,
        error: "",
      };
    case CLASSWORK_FAILED:
      return {
        ...state,
        error: action.payload,
      };
    case CLASSWORK_DELETE:
      return {
        ...state,
        data: state.data.filter((cls) => cls.id !== action.payLoad),
        message: action.message,
      };
    default:
      return state;
  }
};
export default Classwork;
