const {
  VIEW_QUIZ_REQUEST,
  VIEW_QUIZ_SUCCESS,
  VIEW_QUIZ_FAILED,
} = require("../../actions/actionTypes");

const defaultState = {
  data: [],
  loading: false,
  error: "",
};
const ViewQuiz = (state = defaultState, action) => {
  switch (action.type) {
    case VIEW_QUIZ_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case VIEW_QUIZ_SUCCESS:

      return {
        ...state,
        data: action.payLoad,
        loading: false,
      };
    case VIEW_QUIZ_FAILED:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
export default ViewQuiz;
