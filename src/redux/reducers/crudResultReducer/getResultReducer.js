const { RESULT_REQUEST,RESULT_FALSE,RESULT_SUCCESS } = require("../../actions/actionTypes");


/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: 
 **********************************************************************/


const defaultState = {
    loading: false,
    error: '',
    data: []
};


const getResultByClass = (state = defaultState, action) => {
    switch (action.type) {
        case RESULT_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case RESULT_SUCCESS: 
        return {
            data: action.data.data,
            className : action.className,
            loading: false,
            message: action.message,
            error : ''
        };
        case RESULT_FALSE: 
        return {
            ...state,
            error: action.payload
        }
        default:
            return state
    }
}
export default getResultByClass