
/**********************************************************************
 * Original Author: Nheng Vanchhay
 * Created Date: 21-08-2020
 * Development Group: HRD_EXAM
 * Description: 
 **********************************************************************/

const {
    RESULT_STUDENT_REQUEST,
    RESULT_STUDENT_SUCCESS,
    RESULT_STUDENT_FAILED,
  } = require("../../actions/actionTypes");
  
  const defaultState = {
    data: [],
    loading: false,
    error: '',
  };
  const getResultStudentReducer = (state = defaultState, action) => {
    switch (action.type) {
      case RESULT_STUDENT_REQUEST:
      return {
        ...state,
        loading: true,
      };
        case RESULT_STUDENT_SUCCESS:  
                return {
                    data: action.payLoad,
                    loading: false,
                    score: action.score
                };
      case RESULT_STUDENT_FAILED:
        return {
          ...state,
          loading: false,
        };
        default:
        return state;
    }
  };
  export default getResultStudentReducer;
  