
const {  CLASS_NAME_SUCCESS } = require("../../actions/actionTypes");

const defaultState = {
    loading: false,
    data: [],
    error: "",
  };

  const getClassName = (state = defaultState, action) => {
      switch  (action.type) {
        case CLASS_NAME_SUCCESS:
            return {
              ...state,
              data: action.payLoad,
            //   loading: false,
            };
          default:
            return state;
      }
  }

  export default getClassName