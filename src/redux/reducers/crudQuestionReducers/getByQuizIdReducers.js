const { QUIESTION_BY_QUIZID_REQUEST,QUIESTION_BY_QUIZID_SUCCESS,QUIESTION_BY_QUIZID_FAILED } = require("../../actions/actionTypes");


// create by Lon dara

const defaultState = {
    loading: false,
    error: '',
    data: [],
};

const getQuestionByQuizId = (state = defaultState, action) => {
    switch (action.type) {
        case QUIESTION_BY_QUIZID_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case QUIESTION_BY_QUIZID_SUCCESS: 
        return {
            data: action.data,
            loading: false,
            error : action.message
        };
        case QUIESTION_BY_QUIZID_FAILED: 
        return {
            ...state,
            error: action.payload
        }
        default:
            return state
    }
}
export default getQuestionByQuizId