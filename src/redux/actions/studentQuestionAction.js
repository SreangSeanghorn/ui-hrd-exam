import {POST_RESULT} from './actionTypes';
import Axios from 'axios';
import { BASE_URL } from '../../config/API';

export const getResult = ()=> {
    return (dp)=>{
        Axios.post(`${BASE_URL}/results`).then(res=>{
            dp({
                type: POST_RESULT,
                payLoad: res.data
            })
        }).catch(error => console.log('error: ', error))
    }
}
