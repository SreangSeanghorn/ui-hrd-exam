import { BASE_URL } from "../../config/API";
import Axios from "axios";

//get only Class Name
export const validateStartQuiz = (cardId, quizId, callback) => async () => {
  await Axios.get(`${BASE_URL}/results/validate/${quizId}?card_id=${cardId}`)
    .then((res) => {
      console.log(res);
      callback(res.data.message);
    })
    .catch((error) => console.log(error));
};

export default validateStartQuiz;
