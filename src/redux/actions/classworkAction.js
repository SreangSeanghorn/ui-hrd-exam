import Axios from "axios";
import { BASE_URL } from "../../config/API";
import {
  CLASSWORK_REQUEST,
  CLASSWORK_FAILED,
  CLASSWORK_SUCCESS,
  CLASSWORK_DELETE,
  VIEW_QUIZ_REQUEST,
  VIEW_QUIZ_SUCCESS,
  VIEW_QUIZ_FAILED,
  CLASSWORK_GEN_SUCCESS,
} from './actionTypes';

const getClasswork_GEN = (genID) => {
  return async (dp) => {
    try {
      const result = await Axios.get(
        `${BASE_URL}/quizzes?generationId=${genID}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
          },
        }
      );
   
      dp({
        type: CLASSWORK_GEN_SUCCESS,
        payLoad: result.data.data,
      });
    } catch (error) {
      dp({
        type: CLASSWORK_FAILED,
        payload: error.message,
      });
    }
  };
};

export const getViewQuiz = (id, callback) => {
  return async (dp) => {
    dp({
      type: VIEW_QUIZ_REQUEST,
    });
    try {
      const result = await Axios.get(`${BASE_URL}/quizzes/details/${id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
        },
      });
      console.log('result: ', result);
      callback(result.data.status);
      dp({
        type: VIEW_QUIZ_SUCCESS,
        payLoad: result.data.data.responses,
      });
    } catch (error) {
      console.log(error);
      dp({
        type: VIEW_QUIZ_FAILED,
        payLoad: error.message,
      });
    }
  };
};

export const fetchClassworkOne = (id, callback) => {
  Axios.get(`${BASE_URL}/quizzes/${id}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  })
    .then((res) => {
      callback(res.data.data);
    })
    .catch((error) => console.log(error));
};

export const deleteClasswork = (id) => {
  return (dp) => {
    Axios.delete(`${BASE_URL}/quizzes/${id}`)
      .then((res) => {
        console.log('res:', res);
        dp({
          type: CLASSWORK_DELETE,
          payLoad: id,
          status: res.status,
        });
      })
      .catch((error) => console.log(error));
  };
};

export const updateClassWorkStatus = (isActive, id, callback) => {
  Axios.put(`${BASE_URL}/quizzes?id=${id}`, isActive, {
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  }).then((res) => {
    callback(res.data.message);
  });
};

export const updateClassWork = (classwork, id, callback) => {
  Axios.put(`${BASE_URL}/quizzes/${id}`, classwork, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  })
    .then((res) => {
      callback(res.data.message);
    })
    .catch((error) => console.log(error));
};

export const getClassworks = (number, generationId) => {
  return async (dp) => {
    dp({
      type: CLASSWORK_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/quizzes/details/page?generationId=${generationId}&page=${number}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
          },
        }
      );
      dp({
        type: CLASSWORK_SUCCESS,
        data: result.data,
        paging: result.data.paging,
      });
    } catch (error) {
      console.log(error);
      dp({
        type: CLASSWORK_FAILED,
        payload: error.message,
      });
    }
  };
};
export const returnScoreToStudent = (isSend, id, callback) => {
  Axios.put(`${BASE_URL}/quizzes/sent/${id}`, isSend, {
    headers: {
      'Content-Type': 'application/json',
      accept: 'application/json',
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  }).then((res) => {
    callback(res.data.message);
  });
};

export default getClasswork_GEN;
