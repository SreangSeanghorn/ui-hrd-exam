import Axios from 'axios';
import {
  TEACHER_FETCH,
  TEACHER_SUCCESS,
  TEACHER_FAILED,
  TEACHER_DELETE,
} from './actionTypes';
import { BASE_URL } from '../../config/API';

const getCreateTeacher=()=>{
    return async (dp) => {
        dp({
          type: TEACHER_FETCH,
          loading: true,
          success: false,
        });
        try {
          const result = await Axios.get(`${BASE_URL}/teachers`,{headers: {
            Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
          },});   
          dp({
            type: TEACHER_SUCCESS,
            loading: false,
            success: true,
            data: result.data.data,
          });
        } catch (error) {
          dp({
            type: TEACHER_FAILED,
            loading: false,
            success: false,
            error: error.message
          });
        }
      };
}

//Action Creator for creating posts
export const addTeacher = (teacher, callback) => {
  Axios.post(`${BASE_URL}/teachers`, teacher, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  })
    .then((res) => {
      callback(res.data);
    })
    .catch((err) => {});
};

export const fetchCreateTeacher = (id, callback) => {
  Axios.get(`${BASE_URL}/teachers/${id}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  }).then((res) => {
    callback(res.data.data);
  });
};

export const deleteCreateTeacher = (id) => {
  return (dp) => {
    Axios.delete(`${BASE_URL}/teachers/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
      },
    }).then((res) => {
      dp({
        type: TEACHER_DELETE,
        payLoad: id,
        message: res.message,
      });
    });
  };
};

export const updateTeacher = (teacher, id, callback) => {
  teacher.status = true;
  Axios.put(`${BASE_URL}/${id}`, teacher, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  }).then((res) => {
    callback(res.data.status);
  });
};

export const changePassword = (teacher, callback) => {
  console.log(teacher);
  Axios.put(`${BASE_URL}/teachers/updatepassword`, teacher, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
    },
  }).then((res) => {
    callback(res.data.status);
  });
};

export default getCreateTeacher;
