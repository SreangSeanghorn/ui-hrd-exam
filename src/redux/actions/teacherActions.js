import {
  TEACHER_REQUEST,
  TEACHER_LOGIN_SUCCESS,
  TEACHER_ERROR,
} from './actionTypes';
import Axios from 'axios';
import { BASE_URL } from '../../config/API';
import setJWTToken from '../securityUtils/setJWTToken';
import jwt_decode from 'jwt-decode';

export const login = (teacher, callback) => async (dispatch) => {
  try {
    //--- teacher make a request to server so set loading to true
    dispatch({
      type: TEACHER_REQUEST,
    });
    //--- waiting a response
    const response = await Axios.post(`${BASE_URL}/authenticate`, teacher);
    let decodedToken = null;
    if (response.status === 200) {
      //--- callback
      callback(response);
      if (response.data.token) {
        //--- extract token from server
        const token = response.data.token;
        //--- store token in the local storage
        localStorage.setItem('jwtToken', token);
        //--- set our token in header ***
        setJWTToken(token);
        //--- decode token on ReactJS
        decodedToken = jwt_decode(token);
      }
    }
    dispatch({
      type: TEACHER_LOGIN_SUCCESS,
      payload: decodedToken,
    });
  } catch (error) {
    //--- dispatch type error for error
    dispatch({
      type: TEACHER_ERROR,
      payload: error.message,
    });
  }
};
