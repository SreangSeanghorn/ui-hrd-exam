const {
  GENERATION_REQUEST,
  GENERATION_SUCCESS,
  GENERATION_FAILED,
  GENERATION_DELETE,
  GENERATION_ID_STATUS,
} = require("./actionTypes");
const { default: Axios } = require("axios");
const { BASE_URL } = require("../../config/API");

const getGeneration = () => {
  return async (dp) => {
    dp({
      type: GENERATION_REQUEST,
    });
    try {
      const result = await Axios.get(`${BASE_URL}/generations`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
        },
      });
      console.log("Gen",result)
      dp({
        type: GENERATION_SUCCESS,
        data: result.data,
      });
    } catch (error) {
      dp({
        type: GENERATION_FAILED,
        payload: error.message,
      });
    }
  };
};

export const setGenId = (id, activeGen)=>{
  return (dp)=>{
    dp({
        type: GENERATION_ID_STATUS,
        payLoad: id,
        status : activeGen
    })
  }
}

export const postGeneration = (generation, callBack) => {
  return async (dp) => {
    Axios.post(`${BASE_URL}/generations`, generation, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
      },
    })
      .then((res) => {
        callBack(res.data.message);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const deleteGeneration = (id) => {
  return (dp) => {
    Axios.delete(`${BASE_URL}/generations/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
      },
    }).then((res) => {
      dp({
        type: GENERATION_DELETE,
        payLoad: id,
        message: res.message,
      });
    });
  };
};

export const updateGenerationStatus = (isActive, id, callback) => {
  return (dp) => {
    Axios.put(`${BASE_URL}/generations/status/${id}`, isActive, {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
        Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
      },
    }).then((res) => {
      callback(res.data.message);
    }).catch(error => console.log(error))
  };
};
export default getGeneration;
