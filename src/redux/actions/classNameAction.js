import {   CLASS_NAME_SUCCESS, CLASS_NAME_FAILED } from "./actionTypes";
import Axios from "axios";
import { BASE_URL } from "../../config/API";

//get only Class Name
export const getClassNameResponse = (quizId) => {
    return async (dp) => {
    //   dp({
    //     type: CLASS_NAME_REQUEST,
    //   });
      try {
        const result = await Axios.get(`${BASE_URL}/results/classname/${quizId}`)
        dp({
          type: CLASS_NAME_SUCCESS,
          payLoad: result.data.data,
        });
      } catch (error) {
          console.log(error)
        dp({
          type: CLASS_NAME_FAILED,
          payload: error.message,
        });
      }
    };
  };
  export default getClassNameResponse
