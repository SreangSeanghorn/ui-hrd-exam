import Axios from 'axios';
import { BASE_URL } from '../../config/API';
import {
  QUIZ_SUCCESS,
  QUIZ_FALSE,
  QUIZ_REQUEST,
  QUIZ_INSERT_SUCCESS,
  QUIZ_RETURN_SUCCESS,
} from './actionTypes';

// create by lon dara for get add quiz

export const getQuiz = (number, generationId) => {

  return async (dp) => {
    dp({
      type: QUIZ_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/results/quizzes?generationId=${generationId}&&page=${number}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
          },
        }
      );
      dp({
        type: QUIZ_SUCCESS,
        data: result.data,
        paging: result.data.paging,
        message: result.data.message,
      });
    } catch (error) {
      dp({
        type: QUIZ_FALSE,
        payload: error.message,
      });
    }
  };
};

export const returnQuizToStudent = () => {

  return async (dp) => {
    dp({
      type: QUIZ_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/quizzes/sentQuiz`
      );
      dp({
        type: QUIZ_RETURN_SUCCESS,
        payLoad: result.data.data,
        message: result.data.message,
      });
    } catch (error) {
      dp({
        type: QUIZ_FALSE,
        payload: error.message,
      });
    }
  };
};

export const insertQuiz = (newQuiz, callback) => async (dp) => {
  try {
    dp({
      type: QUIZ_REQUEST,
    });
    let quiz = {};
    const response = await Axios.post(`${BASE_URL}/quizzes`, newQuiz);
    if (response.status === 200) {
      callback(response);
      if (response.data.status === 'OK') {
        quiz = response.data.data;
        dp({
          type: QUIZ_INSERT_SUCCESS,
          payload: quiz,
        });
      } else if (response.data.status === 500) {
        dp({
          type: QUIZ_FALSE,
          payload: 'Error database status 500',
        });
      } else {
        dp({
          type: QUIZ_FALSE,
          payload: response.data.message,
        });
      }
    }
  } catch (error) {
    console.log(error.message);
    dp({
      type: QUIZ_FALSE,
      payload: error.message,
    });
  }
};
