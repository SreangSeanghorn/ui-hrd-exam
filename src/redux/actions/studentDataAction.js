const {
  STUDENT_REQUEST,
  SUTUDENT_FAILED,
  SUTUDENT_SUCCESS,
  SUTUDENT_DELETE,
  STUDENT_CLASS,
  SUTUDENT_GEN_SUCCESS,
} = require("./actionTypes");
const { default: Axios } = require("axios");
const { BASE_URL } = require("../../config/API");

const getStudent = (cls = 'KPS', genId = 0) => {
  return async (dp) => {
    dp({
      type: STUDENT_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/students/class?classname=${cls}&generationId=${genId}`
      );
      dp({
        type: SUTUDENT_SUCCESS,
        data: result.data,
      });
    } catch (error) {
      dp({
        type: SUTUDENT_FAILED,
        loading: false,
        success: false,
      });
    }
  };
};
export const fetchStudentOne = (id, callback) => {
  Axios.get(`${BASE_URL}/students/${id}`).then((res) => {
    callback(res.data.data);
  });
};
export const updateStudent = (student, id, callback) => {
  Axios.put(`${BASE_URL}/students/${id}`, student).then((res) => {
    callback(res.data);
  });
};
export const deleteStudent = (id) => {
  return (dp) => {
    Axios.delete(`${BASE_URL}/students/${id}`).then((res) => {
      dp({
        type: SUTUDENT_DELETE,
        payLoad: id,
        status: res.status,
      });
    });
  };
};
export const addStudent = (student, callback) => {
  Axios.post(`${BASE_URL}/students`, student).then((res) => {
    callback(res.data.message);
  });
};

//get only Class Name
export const getClasswork = (genId = 0) => {
  return async (dp) => {
    dp({
      type: STUDENT_REQUEST,
    });
    try {
      const result = await Axios.get(`${BASE_URL}/students/getClassName?generationId=${genId}`);
      dp({
        type: STUDENT_CLASS,
        data: result.data,
      });
    } catch (error) {
      dp({
        type: SUTUDENT_FAILED,
        payload: error.message,
      });
    }
  };
};
 export const getAllStudentGen = (genID) => {
   console.log("==> genID:", genID)
  return async (dp) => {
    dp({
      type: STUDENT_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/students?generationId=${genID}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('jwtToken')}`,
          },
        }
      );
      console.log("result Stu", result)
      dp({
        type: SUTUDENT_GEN_SUCCESS,
        payLoad: result.data.data,
      });
    } catch (error) {
      console.log(error);
      dp({
        type: SUTUDENT_FAILED,
        payload: error.message,
      });
    }
  };
};

export default getStudent;
