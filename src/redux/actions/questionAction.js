import {GET_QUESTION} from './actionTypes';
import Axios from 'axios';
import { BASE_URL} from '../../config/API';
export const getQuestion = ()=> {
    return (dp)=>{
        Axios.get(`${BASE_URL}/quizzes`).then(res=>{
            dp({
                type: GET_QUESTION,
                payLoad: res.data
            })
        }).catch(error => console.log('error: ', error))
    }
}