import {
  QUESTION_TYPE_REQUEST,
  QUESTION_TYPE_SUCCESS,
  QUESTION_TYPE_ERROR,
} from '../actions/actionTypes';
import Axios from 'axios';
import { BASE_URL } from '../../config/API';

let token = localStorage.getItem('jwtToken');

export const getQuestionType = (callback) => async (dispatch) => {
  try {
    dispatch({
      type: QUESTION_TYPE_REQUEST,
      loading: true,
    });
    const response = await Axios.get(`${BASE_URL}/questiontypes`, {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
    let questionTypes = null;
    if (response.status === 200) {
      callback(response);
      questionTypes = response.data.data;
    }
    dispatch({
      type: QUESTION_TYPE_SUCCESS,
      payload: questionTypes,
    });
  } catch (error) {
    dispatch({
      type: QUESTION_TYPE_ERROR,
      payload: error.message,
    });
  }
};


