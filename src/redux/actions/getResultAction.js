import Axios from "axios";
import {
  BASE_URL
} from "../../config/API";
import { RESULT_REQUEST, RESULT_FALSE, RESULT_SUCCESS, RESULT_STUDENT_REQUEST, RESULT_STUDENT_FAILED, RESULT_STUDENT_SUCCESS,RESULT_ONE_STUDENT_SUCCESS } from "./actionTypes";

/**********************************************************************
 * Original Author: Lon dara
 * Created Date: 13-08-2020
 * Development Group: HRD_EXAM
 * Description: This component for view quiz.
 **********************************************************************/

const getResultByClass = (QuizId, Class) => {
  return async (dp) => {
    dp({
      type: RESULT_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/results/${QuizId}?class_name=${Class}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      const ClassName = await Axios.get(
        `${BASE_URL}/students/getClassName`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      const arrClass = ClassName.data.data.map((value) => ({ value }));
      dp({
        type: RESULT_SUCCESS,
        data: result.data,
        className: arrClass,
        message: result.data.message,
      });
    } catch (error) {
      dp({
        type: RESULT_FALSE,
        payload: error.message,
      });
    }
  };
}; 

export const checkQuestionStudent = (question, student_Id, quiz_id, callBack) => {
  Axios.put(`${BASE_URL}/results/${student_Id}?quiz_id=${quiz_id}`, question
  ).then((res) => {
    callBack(res.data.message);
  }).catch(error => console.log(error))
} 

export const getResultStudent = (quizId, studentId) => {
  return async (dp) => {
    dp({
      type: RESULT_STUDENT_REQUEST,
    });
    try {
      const result = await Axios.get(`${BASE_URL}/results/checking/{student_id}?quiz_id=${quizId}&student_id=${studentId}`

      ); 
      dp({
        type: RESULT_STUDENT_SUCCESS,
        payLoad: result.data.data.resultList[0].result,
        score: result.data.data.resultList[0].score
      });
    } catch (error) {
      console.log("error",error)
      dp({
        type: RESULT_STUDENT_FAILED,
        payLoad: error.message,
      });
    }
  };
};

export const getResultOneStudent = (quizId, studentId,callBack) => {
  Axios.get(`${BASE_URL}/results/${studentId}/{quiz_id}?quiz_id=${quizId}`
  ).then((res) => {
    callBack(res.data.data.score);
    console.log(res.data.data.score)
  }).catch(error => console.log(error))
} 

export default getResultByClass;
