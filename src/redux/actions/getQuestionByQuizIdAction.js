import Axios from "axios";
import { BASE_URL } from "../../config/API";
import {
  QUIESTION_BY_QUIZID_REQUEST,
  QUIESTION_BY_QUIZID_SUCCESS,
  QUIESTION_BY_QUIZID_FAILED,
} from './actionTypes';

// create by lon dara

const getQuestionByQuizId = (Id) => {
  return async (dp) => {
    dp({
      type: QUIESTION_BY_QUIZID_REQUEST,
    });
    try {
      const result = await Axios.get(
        `${BASE_URL}/quizzes/details/${Id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      
      dp({
        type: QUIESTION_BY_QUIZID_SUCCESS,
        data: result.data.data,
        message: result.data.message,
      });
    } catch (error) {
      dp({
        type: QUIESTION_BY_QUIZID_FAILED,
        payload: error.message,
      });
    }
  };
};

export default getQuestionByQuizId;
