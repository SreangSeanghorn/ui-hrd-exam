import {POST_RESULT} from './actionTypes';
import Axios from 'axios';
import { BASE_URL } from '../../config/API';
export const postResult = ()=> {
    return (dp)=>{
        Axios.get(`${BASE_URL}/results`).then(res=>{
            dp({
                type: POST_RESULT,
                payLoad: res.payLoad
            })
        }).catch(error => console.log('error: ', error))
    }
}
