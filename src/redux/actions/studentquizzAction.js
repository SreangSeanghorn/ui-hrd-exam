import Axios from "axios";
import { BASE_URL } from "../../config/API";
import {
    STUDENTQUIZZ_SUCCESS,
    STUDENTQUIZZ_FAILED,
    CLASSWORK_REQUEST,
} from "./actionTypes";

export const getStudentQuizz = () => {
  return (dp) => {
    dp({
      type: CLASSWORK_REQUEST,
    });
    Axios.get(`${BASE_URL}/quizzes/status`)
    .then((res) => {
      let data = [];
      if (res.data.status === "OK") {
        data = res.data.data[0].responses;
      }
      dp({
        type: STUDENTQUIZZ_SUCCESS,
        payload: data,
        quizId: res.data.data[0].quizId,
        duration:res.data.data[0].duration
      });
    })
    .catch((error) => {
        dp({
            type:STUDENTQUIZZ_FAILED,
            payload:error.message,
        })
    });
  };
};
