import {
  STUDENT_REQUEST,
  STUDENT_LOGIN_CARD_ID_SUCCESS,
  STUDENT_REGISTER_SUCCESS,
  STUDENT_ERROR,
} from "./actionTypes";
import Axios from "axios";
import { BASE_URL } from "../../config/API";

export const loginByCardID = (cardID, callback) => async (dispatch) => {
  try {
    //--- student make a request to server so set loading to true
    dispatch({
      type: STUDENT_REQUEST,
    });
    //--- waiting a response
    const response = await Axios.get(
      `${BASE_URL}/students/login?CardId=${cardID}`
    );
    if (response.status === 200) {
      callback(response);
      dispatch({
        type: STUDENT_LOGIN_CARD_ID_SUCCESS,
        payload: response.data.data,
      });
    }
  } catch (error) {
    console.log("login-eeror", error);
    dispatch({
      type: STUDENT_ERROR,
      payload: error.message,
    });
  }
};

export const register = (newStudent, callback) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: STUDENT_REQUEST,
      });
      // await response
      const response = await Axios.post(
        `${BASE_URL}/students/login`,
        newStudent
      );
      let data = null;
      if (response.status === 200) {
        callback(response);
        data = response.data;
      }
      dispatch({
        type: STUDENT_REGISTER_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: STUDENT_ERROR,
        payload: error.message,
      });
    }
  };
};

export default loginByCardID;
