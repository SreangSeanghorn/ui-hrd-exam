import Axios from 'axios';
import { BASE_URL } from '../../config/API';

export const postResult = (result, callback) => {
  Axios.post(`${BASE_URL}/results`, result, {})
    .then((res) => {
      callback(res.data.message);
    })
    .catch((error) => console.log('error: ', error));
};
