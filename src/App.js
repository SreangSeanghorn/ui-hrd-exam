import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setJWTToken from './redux/securityUtils/setJWTToken';
import store from './redux/store/store';
import { TEACHER_LOGIN_SUCCESS } from './redux/actions/actionTypes';
import ProtectedRoute from './components/auth/ProtectedRoute';

//---Pages
import HomePage from './components/pages/HomePage';
import { NotFoundPage } from './components/pages/NotFoundPage';
import StudentRegisterPage from './components/auth/StudentRegisterPage';
import TeacherLoginPage from './components/auth/TeacherLoginPage';
import Student from './components/students/Tabars/Student';
import Admin from './layouts/Admin';
import CreateQuiz from './components/CreateQuestion/CreateQuiz';
import ViewQuiz from './views/ClassWork/Quiz/Tabars/Tabars';
import MianPdf from './views/RectPdf/MianPdf.js';
import CheckResultStudentTabars from './views/Response/Tabars/CheckResultStudentTabars';
import ProtectedStudentRoute from './components/auth/ProtectedStudentRoute';
import PreviewData from './components/CreateQuestion/preViewQuiz/Tabars/Tabars';
import UpdateQuiz from './components/CreateQuestion/UpdateQuiz';
import ReturnScoreStudent from './components/pages/ReturnScoreStudent'
import StudentCheckResultTabars from './components/pages/ReturnScoreToStudentTabars/StudentCheckResultTabars'

//---style
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/material-dashboard-react.css?v=1.9.0';
import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './assets/css/App.css';

const jwtToken = localStorage.getItem('jwtToken');
if (jwtToken) {
  setJWTToken(jwtToken);
  const decoded_jwtToken = jwt_decode(jwtToken);
  store.dispatch({
    type: TEACHER_LOGIN_SUCCESS,
    payload: decoded_jwtToken,
  });

  const currentTime = Date.now() / 1000;
  if (decoded_jwtToken.exp < currentTime) {
    localStorage.clear();
    window.location.href = '/';
  }
}

function App() {
  return (
    <div className='container-fluid px-0'>
      <Router>
        <Switch>
          <Route exact path='/' component={HomePage} />

          <Route path='/teacher-login' component={TeacherLoginPage} />

          <Route path='/student-register' component={StudentRegisterPage} />

          <Route path='/studentCheckScore' component={ReturnScoreStudent} />

          <ProtectedStudentRoute
            path='/student-doing-exam'
            component={Student}
          />
          <Route path='/student-check-result' component={StudentCheckResultTabars} />

          <Route path="/admin/classwork/viewquiz" component={ViewQuiz} />
          
          <Route path="/create-quiz/previewData" component={PreviewData} />

          <Route path='/admin/classwork/reactpdf' component={MianPdf} />

          <Route
            path='/admin/response/student/checkresult'
            component={CheckResultStudentTabars}
          />

          <Route path='/create-quiz' component={CreateQuiz} />

          <Route path='/update-quiz/:id' component={UpdateQuiz} />

          <ProtectedRoute path='/admin' component={Admin} />

          <Route path='*' component={NotFoundPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
